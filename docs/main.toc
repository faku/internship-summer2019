\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {english}{}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Introduction}{1}{section.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Analysis}{2}{section.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}TVM}{2}{subsection.2.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2}nGraph}{3}{subsection.2.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3}hls4ml}{3}{subsection.2.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4}HeteroCL}{3}{subsection.2.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.5}Conclusion}{6}{subsection.2.5}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Installation}{7}{section.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Development}{8}{section.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Deployment}{12}{section.5}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1}Adapt Vivado HLS code}{12}{subsection.5.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2}Create IP}{12}{subsection.5.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3}Deploying on FPGA}{16}{subsection.5.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.4}Running inference}{17}{subsection.5.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6}SDR Neural Network}{18}{section.6}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1}Network architecture}{18}{subsection.6.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.2}Dataset}{19}{subsection.6.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3}Development}{19}{subsection.6.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.3.1}Deployment}{22}{subsubsection.6.3.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{References}{23}{section*.20}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{Acronyms}{24}{section*.21}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{Appendices}{25}{section*.23}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A}TVM: Deploy ResNet model from MXNet on VTA}{25}{Appendix.a.A}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {B}hls4ml: Keras/Tensorflow model to HLS example}{29}{Appendix.a.B}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {C}HeteroCL: LeNet inference}{32}{Appendix.a.C}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {D}Development process: Produced Vivado HLS code}{34}{Appendix.a.D}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {E}REDS model: Vivado HLS code}{37}{Appendix.a.E}% 
