\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{bachelor}[v1.0 Lucas Elisei LaTeX class]

% Load 'report' class.
\LoadClass[10pt,a4paper]{article}

% Required packages.
\RequirePackage{geometry}
%\RequirePackage{lmodern}
\RequirePackage{tikz}

% Layout.
\geometry{left=1.0in,right=1.0in,top=1.0in,bottom=1.0in}

% Default font.
\renewcommand{\familydefault}{\sfdefault}

\newcommand*{\subtitle}[1]{\gdef\@subtitle{#1}}

\newcommand*{\supervisor}[1]{\gdef\@supervisor{#1}}

% Redefine \maketitle
\renewcommand{\maketitle}{
    \newpage
    \null

%    \begin{tikzpicture}[remember picture,overlay]
%        \node[anchor=north east,inner sep=1cm] at (current page.north east)
%        {\includegraphics[height=3cm]{figures/hesso_logo.png}};
%    \end{tikzpicture}

    \begin{tikzpicture}[remember picture,overlay]
        \node[anchor=north west,inner sep=1cm] at (current page.north west)
        {\includegraphics[height=3cm]{figures/reds_logo.png}};
    \end{tikzpicture}

    \vfill

    \begin{center}
        {\fontsize{30}{40}\selectfont \@title}
        \vskip 20pt
        {\Large \@subtitle}
        \vskip 100pt
        {\Large
            \begin{tabular}{ll}
                Student: & \@author \\
                \\
                Supervisor: & \@supervisor
            \end{tabular}
        }
        \vskip 60pt
        {\large \@date}
    \end{center}

    \vfill
}

% Disable paragraph indentation.
\setlength\parindent{0pt}
