\section{Analysis}

Four frameworks studied in the \textit{\acrlong{ml} \acrshort{fpga} Accelerator} project have been chosen to be examined
in more detail during this task. These frameworks are:

\begin{itemize}
\item   TVM/VTA
\item   nGraph
\item   hls4ml
\item   HeteroCL
\end{itemize}

These frameworks will be compared based on a list of specific constraints:

\begin{enumerate}[label=\textbf{\arabic*.}]
\item   \textbf{Open-Source License}: since this work will be used in the framework for the \acrshort{sdr} Makerspace
        project, all the work done within this task must be oriented towards an open-source development. This implies a
        study of the entire framework toolchain and the related licenses.
\item   \textbf{Portability}: the code produced by a framework must be platform-agnostic. This ideally implies a
        implementation of the generated \acrfull{hdl} code on different \acrshort{fpga} models and families (Xilinx and
        Intel). Nevertheless, supporting Xilinx platforms (especially the Zinq family) is key requirement in view of the
        implementation on an \acrshort{sdr} platform already own by the REDS institute.
\item   \textbf{Interface compatibility}: in order to guarantee good flexibility of use and an easy hardware integration,
        the produced code should ideally be compatible with standard protocols such as AXII and/or Avalon.
\end{enumerate}

\subsection{TVM}

TVM is an open Deep Learning compiler stack for \acrshortpl{cpu}, \acrshortpl{gpu} and specialised accelerators
(\acrshortpl{fpga}). It aims at closing the gap between the productivity-focused Deep Learning frameworks and the
performance- or efficiency-oriented hardware backends \cite{tvm}.

\subsubsection*{License}

The TVM stack and all its components are licensed under the Apache License 2.0 \cite{license-apache2}.

\subsubsection*{Portability}

The code produced by TVM is an \acrfull{ir} internal to itself. Once the \acrfull{ml} module has been compiled, it is
offloaded to the targeted \acrshort{fpga}'s \acrfull{soc} and executed by the VTA \acrfull{isa}. Thus, the produced
code is platform-agnostic.

The runtime performs a JIT compilation of the accelerator binaries and manages execution between the \acrshort{cpu} and
VTA. VTA hardware architecture can be parametrised (shape of the tensor intrinsic, clock frequency, pipelining, data
type width, and on-chip buffer sizes) so it is optimised for a specific \acrshort{ml} model. \\
It also generates and manages micro-kernels on the fly: it controls when to load kernels from DRAM into the accelerator
so that even large models are supported.

However, at the moment, only a driver for offloading \acrshort{ml} models to the Pynq board is available. To support
more devices, we should implement a driver for a specific target, which can be time-demanding.

An example of integrating a new board (Terasic DE10-Nano) to VTA is available on the TVM Github repository, under the
Pull Request \#3394\footnotemark.

\footnotetext{\url{https://github.com/dmlc/tvm/pull/3394}.}

\subsubsection*{Additional notes}

The documentation is quite good, especially for examples and tutorials, although not always up-to-date with the code. \\
A forum where questions can be asked, and topics be discussed, exists and is really active (multiple posts per day). It
is a good thing if one has questions about features or issues.

An example showing how to deploy a pre-trained ResNet model on VTA is available at appendix
\ref{appendix:tvm-vta-resnet-example}.

\subsubsection*{Interface compatibility}

If our assumptions above are correct, then TVM don't offer AXII nor Avalon bus interfaces.

\subsection{nGraph}

nGraph is an open-source graph compiler for artificial \acrlongpl{nn}. The nGraph compiler stack provides an inherently
efficient graph-based compilation infrastructure designed to be compatible with many upcoming integrated circuits while
also unlocking a massive performance boost on any existing hardware targets \cite{ngraph}.

\subsubsection*{License}

The nGraph compiler stack and all its components are licensed under the Apache License 2.0 \cite{license-apache2}.

\subsubsection*{Portability}

Since nGraph doesn't support \acrshortpl{fpga} yet, we can't establish a statement. The only thing we can conclude is
that nGraph is not a suitable candidate at the moment.

\subsubsection*{Interface compatibility}

N/A.

\subsection{hls4ml}

hls4ml is a package for \acrlong{ml} inference in \acrshortpl{fpga}. It translates traditional open-source \acrlong{ml}
models into \acrfull{hls} language that can be configured according to the output platform \cite{hls4ml}.

\subsubsection*{License}

The hls4ml framework and all its components are licensed under the Apache License 2.0 \cite{license-apache2}.

\subsubsection*{Portability}

hls4ml produces an IP which can be imported into a design. At the moment, the package targets Xilinx IP only.

\subsubsection*{Interface compatibility}

Since hls4ml produces an IP, we should be able to implement an AXI or Avalon bus interfaces as it pleases us.

\subsubsection*{Additional notes}

hls4ml accepts Keras/Tensorflow and PyTorch as input. However, our model is described with ONNX. A tool to import an
ONNX model in Tensorflow exists but is currently in beta.

An example of converting a Keras/Tensorflow model to \acrshort{hls} is available at appendix
\ref{appendix:hls4ml-example}.

According to a member of the development team, they are currently testing their tool with Vivado 2018.2. 

\subsection{HeteroCL}

HeteroCL is a programming infrastructure composed of a Python-based \acrfull{dsl} (extended from TVM) and a compilation
flow. The HeteroCL \acrshort{dsl} provides a clean abstraction that decouples algorithm specification from three
important types of hardware customization in compute, data types and memory architectures \cite{heterocl}.

\subsubsection*{License}

HeteroCL and all its components are licensed under the Apache License 2.0 \cite{license-apache2}.

\subsubsection*{Portability}

As outputs, HeteroCL can generate Vivado \acrshort{hls} C++ code, Intel \acrshort{hls} C++ code and Merlin C code.

\subsubsection*{Interface compatibility}

Since HeteroCL generates code for a particular entity, it should be possible to integrate AXI or Avalon bus protocols as
it pleases us.

\subsubsection*{Additional notes}

HeteroCL doesn't accept ONNX models as input but it accepts PyTorch, MXNet and Keras/Tensorflow models. A tool to import
ONNX models in MXNet exist and is fully-compatible while the tool to import ONNX models in Tensorflow is still in beta
version.

An example on how to do inference with LeNet model is available at appendix \ref{appendix:hcl-lenet-example}.

According to one of the maintainer, the development team uses Vivado 2017.2.

\newpage
\input{sections/02-table.tex}

\subsection{Conclusion}

The most interesting candidate is HeteroCL, because it offers three different outputs and importing ONNX models is
possible and reliable.

Because HeteroCL is based on TVM, it might be recurrent to choose it as fallback candidate. Thus, we prefer to choose
hls4ml not only because it's the \textit{last} choice, but also because it outputs IP that can easily be integrated
in a Xilinx design and bus protocols should be easy to implement as interfaces.