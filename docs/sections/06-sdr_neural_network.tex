\section{SDR Neural Network}

Now that we defined a workflow with a simple \acrshort{nn} such as MNIST, we can focus on deploying the \acrlong{nn}
developed by the REDS institute on an FPGA.


\subsection{Network architecture}

The figure \ref{fig:reds-model} shows an overview of the \acrlong{nn} developed
by the REDS institute.

\begin{minipage}{\linewidth}
    \centering
    \includegraphics[scale=0.4]{figures/reds_model.png}
    \captionof{figure}{Architecture of the \acrlong{nn} developed at the REDS institute.}
    \label{fig:reds-model}
\end{minipage}

This model is quite light: layers perform simple operations. Defining it using the HeteroCL should be straightforward.

\textbf{Note:} To view more details about the model, we recommend using the website
\url{https://lutzroeder.github.io/netron/} and load the \texttt{.h5} file located in the \texttt{src/hcl/sdr/model}
directory.


\subsection{Dataset}

To test that our network performs well, we will test it on the DeepSig Inc. Dataset called RadioML 2016.10A
\cite{deepsig}.

This dataset is a synthetic dataset, generated with GNU Radio, consisting of 11 modulations (8 digital and 3 analog) at
varying signal-to-noise ratios. It contains 200'000 samples, each composed of 2x128 floating point data.

To ease the dataset download, a script has been provided. Execute the following commands in a terminal:

\begin{minted}{bash}
cd src/hcl/sdr/data
./fetch_data.sh
\end{minted}

Once the script is done executing, the \texttt{data} folder should contain the file \texttt{RML2016.10a\_dict.pkl}.

Now that the dataset is ready to use, we must extract data from it. The following Python code is used to extract the
200'000 samples:

\begin{minted}{python}
# Load the dataset
Xd = cPickle.load(open("data/RML2016.10a_dict.pkl",'rb'), encoding='latin1')
snrs,mods = map(lambda j: sorted(list(set(map(lambda x: x[j], Xd.keys())))), [1,0])
X = []  
lbl = []
for mod in mods:
    for snr in snrs:
        X.append(Xd[(mod,snr)])
        for i in range(Xd[(mod,snr)].shape[0]):  lbl.append((mod,snr))
X = np.vstack(X)

# X.shape: (220000,2,128)
\end{minted}


\subsection{Development}

All the code discussed in this section is available at the file \texttt{src/hcl/sdr/sdr.py}.

As explained previously, we need to define the network as an HeteroCL specification.
Since only the network definition differs from the section \ref{sec:development}, we will only explain those differences
to avoid unnecessary repetitions.

The first layer to implement is the first \texttt{Conv1D} layer shown in figure \ref{fig:reds-model}. HeteroCL only
offers the \texttt{Conv2D\_NCHW} operator and we want to re-use the provided operators as far as possible. Thus, we
will need to tweak a bit the layers' inputs as well as the layers' weights.

Input for a convolutional layer can be of different formats: \texttt{NCHW} and \texttt{NHWC}, where:

\begin{itemize}
\item   \texttt{N}: batch size
\item   \texttt{C}: channel
\item   \texttt{H}: height
\item   \texttt{W}: width
\end{itemize}

The network architecture shown in figure \ref{fig:reds-model} is extracted from a Keras model, which uses the
\texttt{NHWC} format for convolutional layers. However, the current implementation of HeteroCL offers the NCHW format
only. Thus, we have to transpose the weights of the layer as well as the input data.

\begin{minted}[label=sdr.py]{python}
w_conv1d_1_np = np.transpose(np.load('weights/w_conv1d_1.npy'))
b_conv1d_1_np = np.load('weights/b_conv1d_1.npy')
w_conv1d_2_np = np.transpose(np.load('weights/w_conv1d_2.npy'))
b_conv1d_2_np = np.load('weights/b_conv1d_2.npy')
\end{minted}

Also, since HeteroCL only offers a \texttt{Conv2D} operator, we have to reshape the input data and the layer's weights
so that the parameter \texttt{H} (height) is equal to \texttt{1}.

\begin{minted}[label=sdr.py]{python}
w_conv1d_1_np = w_conv1d_1_np.reshape((w_conv1d_1_np.shape[0], w_conv1d_1_np.shape[1], 1, w_conv1d_1_np.shape[2]))
w_conv1d_2_np = w_conv1d_2_np.reshape((w_conv1d_2_np.shape[0], w_conv1d_2_np.shape[1], 1, w_conv1d_2_np.shape[2]))
\end{minted}

As said before, we also have to transpose the input data. This will be done thanks to a \texttt{Tranpose} operator
written especially for the network:

\begin{minted}[label=sdr.py]{python}
# Transpose layer with perms=[0,3,2,1]
def transpose(data, name='transpose'):
    s = data.shape
    return hcl.compute((s[0],s[3],s[2],s[1]), lambda i, j, k, l: data[i, l, k, j], name=name,
                       attrs=OrderedDict([('app_name', tvm.make.StringImm('transpose'))]))
\end{minted}

This operator can be enhanced by adding a \texttt{perms} parameter which defines the permutations to be performed on the
layer passed with parameter \texttt{data}. But for this network we only needed to do a single permutation so it seemed
overkill to implement something more complicated than this.

Now that we have everything we need, we can start to define the REDS network:

\begin{minted}[label=sdr.py]{python}
def build_network(input_data,
                  w_conv1d_1, b_conv1d_1,
                  w_conv1d_2, b_conv1d_2,
                  w_dense_1, b_dense_1,
                  w_dense_2, b_dense_2,
                  w_dense_3, b_dense_3,
                  w_dense_4, b_dense_4):
    # Transpose data
    transpose_1 = transpose(input_data, 'transpose_1')
\end{minted}

The first convolutional layer is composed of two operators: the \texttt{Conv1D} and the \texttt{MaxPooling1D}. The
latter performs a \acrshort{relu} activation on the data so we also must add this operator to the network definition:

\begin{minted}[label=sdr.py]{python}
    # First 1D-Convolutional layer
    conv1d_1 = hlib.nn.conv2d_nchw(transpose_1, w_conv1d_1, bias=b_conv1d_1, name='conv1d_1')
    relu_1 = hlib.nn.relu(conv1d_1, name='relu_1')
    maxpool1d_1 = hlib.nn.max_pool(relu_1, kernel=(1,2), stride=(1,2), name='maxpool1d_1')
\end{minted}

The second convolutional layer works the same way as the first:

\begin{minted}[label=sdr.py]{python}
    # Second 1D-Convolutional layer
    conv1d_2 = hlib.nn.conv2d_nchw(maxpool1d_1, w_conv1d_2, bias=b_conv1d_2, name='conv1d_2')
    relu_2 = hlib.nn.relu(conv1d_2, name='relu_2')
    maxpool1d_2 = hlib.nn.max_pool(relu_2, kernel=(1,2), stride=(1,2), name='maxpool1d_2')
\end{minted}

We now have to flatten the output of the second convolutional layer. However, since the input data has previously been
transposed, we have to transpose it again to keep the data coherent with the original Keras model:

\begin{minted}[label=sdr.py]{python}
    # Flatten layer
    transpose_2 = transpose(maxpool1d_2, 'transpose_2')
    flatten_1 = hlib.nn.flatten(transpose_2, name='flatten_1')
\end{minted}

The next three layers are composed of a \texttt{Dense} operator as well as a \acrshort{relu} activation.

\begin{minted}[label=sdr.py]{python}
    # First Dense layer
    dense_1 = hlib.nn.dense(flatten_1, w_dense_1, bias=b_dense_1, name='dense_1')
    relu_3 = hlib.nn.relu(dense_1, name='relu_3')

    # Second Dense layer
    dense_2 = hlib.nn.dense(relu_3, w_dense_2, bias=b_dense_2, name='dense_2')
    relu_4 = hlib.nn.relu(dense_2, name='relu_4')

    # Third Dense layer
    dense_3 = hlib.nn.dense(relu_4, w_dense_3, bias=b_dense_3, name='dense_3')
    relu_5 = hlib.nn.relu(dense_3, name='relu_5')
\end{minted}

Finally, the last layer is composed of a \texttt{Dense} layer and a \texttt{Softmax} activation.

\begin{minted}[label=sdr.py]{python}
    # Fourth Dense layer
    dense_4 = hlib.nn.dense(relu_5, w_dense_4, bias=b_dense_4, name='dense_4')
    softmax_1 = hlib.nn.softmax(dense_4, name='softmax_1')

    return softmax_1
\end{minted}

We are now finished with defining the REDS model. The next and final step is to validate it by verifying that it outputs
the same results as the original Keras model:

\begin{minted}[label=sdr.py]{python}
rom keras.models import load_model

model = load_model('model/VGG_RML2016.10b_20190426084923.h5', compile=False)
total = 0
iters = 1000

for i in range(iters):
    input_data = np.transpose(X[0]).reshape(1,128,2)
    input_data_hcl = hcl.asarray(input_data.reshape(1,128,1,2), dtype=qtype1)
    output_hcl = hcl.asarray(np.zeros(shape=(1,10)), dtype=qtype1)

    f(input_data_hcl,
        w_conv1d_1_hcl, b_conv1d_1_hcl,
        w_conv1d_2_hcl, b_conv1d_2_hcl,
        w_dense_1_hcl, b_dense_1_hcl,
        w_dense_2_hcl, b_dense_2_hcl,
        w_dense_3_hcl, b_dense_3_hcl,
        w_dense_4_hcl, b_dense_4_hcl,
        output_hcl
    )

    hcl_pred = np.argmax(output_hcl.asnumpy(), axis=1)
    model_pred = np.argmax(model.predict(input_data, batch_size=1), axis=1)

    if (hcl_pred == model_pred):
        total += 1

print('Accuracy: {}'.format(total / iters))
\end{minted}

When executing the program, we get the following output:

\begin{minted}{text}
Accuracy: 1.00000
\end{minted}

Thus, we can consider that the REDS model is well defined.


\newpage
\subsubsection{Deployment}

The VHLS code produced by HeteroCL is available at appendix \ref{appendix:reds-vhls}.

As performed before, we modified the function arguments so that weights are included in the design rather than passed
during execution. The problem is that the weights take more memory than what is available on the \acrshort{fpga} board,
as the following resources report shows:

\begin{minipage}{\linewidth}
    \centering
    \includegraphics[width=0.6\linewidth]{figures/report_weights.png}
    \captionof{figure}{Vivado HLS resources report.}
\end{minipage}

The current design uses 348\% of the available \texttt{BRAM\_18K} resources, which is not synthesizable.

To tackle this problem, we should use the DDRAM3 integrated into the \acrshort{fpga} board to store the data of
\texttt{w\_conv1d\_2} and \texttt{w\_dense\_1}, which respectively require 512 and 704 KiB of memory space.

To do so, the idea is to load the weights data into the DDR3 thanks to Linux (with \texttt{tftp} command or an
equivalent) to fixed addresses. Then, we pass these addresses to the design so it knows where to find the data for
the arrays.

Inference should run afterwards.