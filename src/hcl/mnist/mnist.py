# pylint: disable=no-member

from collections import OrderedDict

import heterocl as hcl
import hlib
import heterocl.tvm as tvm

import numpy as np

import mxnet as mx
import mxnet.contrib.onnx as onnx_mxnet

hcl.init(hcl.Float())

def bias_add_4x3(data, bias, name='bias_add_4x3'):
    return hcl.compute(data.shape, lambda i, j, k, l: data[i, j, k, l] + bias[j, i, i],
                       name=name, attrs=OrderedDict([('app_name', tvm.make.StringImm('bias_add_4x3'))]))

def bias_add(data, bias, name='add'):
    assert len(bias.shape) == 1
    k = hcl.reduce_axis(0, bias.shape[0], 'k')
    return hcl.compute(data.shape, lambda *i: data[i] + bias[k],
                       name=name, attrs=OrderedDict([('app_name', tvm.make.StringImm('bias_add'))]))

def bias_add_2x2(data, bias, name='bias_add_2x2'):
    return hcl.compute(data.shape, lambda i, j: data[i, j] + bias[i, j],
                       name=name, attrs=OrderedDict([('app_name', tvm.make.StringImm('bias_add_2x2'))]))

# ReLU activation function.
def relu(data, name='relu'):
    return hcl.compute(data.shape, lambda *i: hcl.select(data[i] < 0, 0.0, data[i]),
                       name, attrs=OrderedDict([('app_name', tvm.make.StringImm('relu'))]))

# Matrix multiplication layer.
def matmul(data, weight, bias=None, name='matmul'):
    assert len(data.shape) == 2 and len(weight.shape) == 2, "only support 2-dim matmul"
    if bias is not None:
        assert len(bias.shape) == 1, "only support 1-dim bias"
    batch, in_dim = data.shape
    _, out_dim = weight.shape
    k = hcl.reduce_axis(0, in_dim)
    attrs = OrderedDict([
        ('k', in_dim),
        ('j', out_dim),
        ('i', batch),
        ('app_name', tvm.make.StringImm('matmul'))
    ])
    matmul = hcl.compute((batch, out_dim), lambda i, j: hlib.nn.sum(data[i, k] * weight[k, j], axis=k), name, attrs=attrs)
    if bias is not None:
        matmul = hcl.compute((batch, out_dim), lambda i, j: matmul[i, j] + bias[i], name=name, attrs=attrs)
    return matmul

def build_mlp(input_image, w_conv1, w_add1, w_conv2, w_add2, w_matmul1, w_add3):
    # First convolution
    conv1 = hlib.nn.conv2d_nchw(input_image, w_conv1, stride=(1,1), padding='SAME', dilation=(1,1), name='conv1')
    add1 = bias_add_4x3(conv1, w_add1, name='add1')
    # add1 = bias_add(conv1, w_add1, name='add1')
    relu1 = relu(add1, 'relu1')
    pool1 = hlib.nn.max_pool(relu1, kernel=(2,2), stride=(2,2), name='pool1')

    # Second convolution
    conv2 = hlib.nn.conv2d_nchw(pool1, w_conv2, stride=(1,1), padding='SAME', dilation=(1,1), name='conv2')
    add2 = bias_add_4x3(conv2, w_add2, name='add2')
    relu2 = relu(add2, 'relu2')
    pool2 = hlib.nn.max_pool(relu2, kernel=(3,3), stride=(3,3), name='pool2')

    # Dense
    flat1 = hlib.nn.flatten(pool2, name='flat1')
    matmul1 = matmul(flat1, w_matmul1, bias=w_add3, name='matmul1')

    return matmul1

# Path to model.
MODEL_PATH = './model/model.onnx'

# Import ONNX model and convert to MXNet.
sym, arg, aux = onnx_mxnet.import_model(MODEL_PATH)

w_conv1_np   = arg['Parameter5'].asnumpy()
w_add1_np    = arg['Parameter6'].asnumpy()
w_conv2_np   = arg['Parameter87'].asnumpy()
w_add2_np    = arg['Parameter88'].asnumpy()
w_matmul1_np = arg['Parameter193'].asnumpy().reshape((256, 10))
w_add3_np    = arg['Parameter194'].asnumpy().reshape((10,))

qtype1 = hcl.Float()
batch_size = 1
target = 'llvm'
mnist = mx.test_utils.get_mnist()

correct_sum = 0

def build_mlp_inf(batch_size=batch_size, target=target):
    # Setup input/output placeholders
    input_image = hcl.placeholder((batch_size, 1, 28, 28), 'input_image', qtype1)
    w_conv1 = hcl.placeholder(w_conv1_np.shape, 'w_conv1', qtype1)
    w_add1 = hcl.placeholder(w_add1_np.shape, 'w_add1', qtype1)
    w_conv2 = hcl.placeholder(w_conv2_np.shape, 'w_conv2', qtype1)
    w_add2 = hcl.placeholder(w_add2_np.shape, 'w_add2', qtype1)
    w_matmul1 = hcl.placeholder(w_matmul1_np.shape, 'w_matmul1', qtype1)
    w_add3 = hcl.placeholder(w_add3_np.shape, 'w_add3', qtype1)

    # Create quantization scheme
    scheme = hcl.create_scheme(
        [input_image, w_conv1, w_add1, w_conv2, w_add2, w_matmul1, w_add3],
        build_mlp
    )

    s = hcl.create_schedule_from_scheme(scheme)

    return hcl.build(s, target=target)

f = build_mlp_inf()

w_conv1_hcl = hcl.asarray(w_conv1_np, dtype=qtype1)
w_add1_hcl = hcl.asarray(w_add1_np, dtype=qtype1)
w_conv2_hcl = hcl.asarray(w_conv2_np, dtype=qtype1)
w_add2_hcl = hcl.asarray(w_add2_np, dtype=qtype1)
w_matmul1_hcl = hcl.asarray(w_matmul1_np, dtype=qtype1)
w_add3_hcl = hcl.asarray(w_add3_np, dtype=qtype1)

for i in range(10000 // batch_size):
    label = mnist['test_label'][i*batch_size:(i+1)*batch_size]
    input_image_np = mnist['test_data'][i*batch_size:(i+1)*batch_size]
    input_image_hcl = hcl.asarray(input_image_np, dtype=qtype1)

    output_hcl = hcl.asarray(np.zeros(shape=(batch_size, 10)), dtype=qtype1)
    f(input_image_hcl, w_conv1_hcl, w_add1_hcl, w_conv2_hcl, w_add2_hcl, w_matmul1_hcl, w_add3_hcl, output_hcl)

    prediction = np.argmax(output_hcl.asnumpy(), axis=1)
    correct_sum += np.sum(np.equal(prediction, label))

print("Testing accuracy: {}".format(correct_sum / 10000.))

f = build_mlp_inf(target='vhls')
print(f)