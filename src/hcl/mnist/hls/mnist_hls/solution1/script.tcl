############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2017 Xilinx, Inc. All Rights Reserved.
############################################################
open_project mnist_hls
set_top run_mnist_inference
add_files mnist_hls/src/w_reshape1.h
add_files mnist_hls/src/w_conv2.h
add_files mnist_hls/src/w_conv1.h
add_files mnist_hls/src/w_add3.h
add_files mnist_hls/src/w_add2.h
add_files mnist_hls/src/w_add1.h
add_files mnist_hls/src/mnist.cpp -cflags "-I../tb"
add_files -tb mnist_hls/tb/testbench.cpp
add_files -tb mnist_hls/tb/t10k-labels-idx1-ubyte
add_files -tb mnist_hls/tb/t10k-images-idx3-ubyte
add_files -tb mnist_hls/tb/mnist.h
add_files -tb mnist_hls/tb/input_image.h
open_solution "solution1"
set_part {xc7z020clg484-1} -tool vivado
create_clock -period 10 -name default
source "./mnist_hls/solution1/directives.tcl"
csim_design -compiler gcc
csynth_design
cosim_design
export_design -rtl verilog -format ip_catalog
