// ==============================================================
// File generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2017.2
// Copyright (C) 1986-2017 Xilinx, Inc. All Rights Reserved.
// 
// ==============================================================

#ifndef XRUN_MNIST_INFERENCE_H
#define XRUN_MNIST_INFERENCE_H

#ifdef __cplusplus
extern "C" {
#endif

/***************************** Include Files *********************************/
#ifndef __linux__
#include "xil_types.h"
#include "xil_assert.h"
#include "xstatus.h"
#include "xil_io.h"
#else
#include <stdint.h>
#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stddef.h>
#endif
#include "xrun_mnist_inference_hw.h"

/**************************** Type Definitions ******************************/
#ifdef __linux__
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
#else
typedef struct {
    u16 DeviceId;
    u32 Axilites_BaseAddress;
} XRun_mnist_inference_Config;
#endif

typedef struct {
    u32 Axilites_BaseAddress;
    u32 IsReady;
} XRun_mnist_inference;

/***************** Macros (Inline Functions) Definitions *********************/
#ifndef __linux__
#define XRun_mnist_inference_WriteReg(BaseAddress, RegOffset, Data) \
    Xil_Out32((BaseAddress) + (RegOffset), (u32)(Data))
#define XRun_mnist_inference_ReadReg(BaseAddress, RegOffset) \
    Xil_In32((BaseAddress) + (RegOffset))
#else
#define XRun_mnist_inference_WriteReg(BaseAddress, RegOffset, Data) \
    *(volatile u32*)((BaseAddress) + (RegOffset)) = (u32)(Data)
#define XRun_mnist_inference_ReadReg(BaseAddress, RegOffset) \
    *(volatile u32*)((BaseAddress) + (RegOffset))

#define Xil_AssertVoid(expr)    assert(expr)
#define Xil_AssertNonvoid(expr) assert(expr)

#define XST_SUCCESS             0
#define XST_DEVICE_NOT_FOUND    2
#define XST_OPEN_DEVICE_FAILED  3
#define XIL_COMPONENT_IS_READY  1
#endif

/************************** Function Prototypes *****************************/
#ifndef __linux__
int XRun_mnist_inference_Initialize(XRun_mnist_inference *InstancePtr, u16 DeviceId);
XRun_mnist_inference_Config* XRun_mnist_inference_LookupConfig(u16 DeviceId);
int XRun_mnist_inference_CfgInitialize(XRun_mnist_inference *InstancePtr, XRun_mnist_inference_Config *ConfigPtr);
#else
int XRun_mnist_inference_Initialize(XRun_mnist_inference *InstancePtr, const char* InstanceName);
int XRun_mnist_inference_Release(XRun_mnist_inference *InstancePtr);
#endif


u32 XRun_mnist_inference_Get_input_image_BaseAddress(XRun_mnist_inference *InstancePtr);
u32 XRun_mnist_inference_Get_input_image_HighAddress(XRun_mnist_inference *InstancePtr);
u32 XRun_mnist_inference_Get_input_image_TotalBytes(XRun_mnist_inference *InstancePtr);
u32 XRun_mnist_inference_Get_input_image_BitWidth(XRun_mnist_inference *InstancePtr);
u32 XRun_mnist_inference_Get_input_image_Depth(XRun_mnist_inference *InstancePtr);
u32 XRun_mnist_inference_Write_input_image_Words(XRun_mnist_inference *InstancePtr, int offset, int *data, int length);
u32 XRun_mnist_inference_Read_input_image_Words(XRun_mnist_inference *InstancePtr, int offset, int *data, int length);
u32 XRun_mnist_inference_Write_input_image_Bytes(XRun_mnist_inference *InstancePtr, int offset, char *data, int length);
u32 XRun_mnist_inference_Read_input_image_Bytes(XRun_mnist_inference *InstancePtr, int offset, char *data, int length);
u32 XRun_mnist_inference_Get_output_r_BaseAddress(XRun_mnist_inference *InstancePtr);
u32 XRun_mnist_inference_Get_output_r_HighAddress(XRun_mnist_inference *InstancePtr);
u32 XRun_mnist_inference_Get_output_r_TotalBytes(XRun_mnist_inference *InstancePtr);
u32 XRun_mnist_inference_Get_output_r_BitWidth(XRun_mnist_inference *InstancePtr);
u32 XRun_mnist_inference_Get_output_r_Depth(XRun_mnist_inference *InstancePtr);
u32 XRun_mnist_inference_Write_output_r_Words(XRun_mnist_inference *InstancePtr, int offset, int *data, int length);
u32 XRun_mnist_inference_Read_output_r_Words(XRun_mnist_inference *InstancePtr, int offset, int *data, int length);
u32 XRun_mnist_inference_Write_output_r_Bytes(XRun_mnist_inference *InstancePtr, int offset, char *data, int length);
u32 XRun_mnist_inference_Read_output_r_Bytes(XRun_mnist_inference *InstancePtr, int offset, char *data, int length);

#ifdef __cplusplus
}
#endif

#endif
