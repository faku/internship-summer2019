#include <ap_int.h>
#include <ap_fixed.h>
#include <math.h>

#include "w_add1.h"
#include "w_add2.h"
#include "w_add3.h"
#include "w_conv1.h"
#include "w_conv2.h"
#include "w_reshape1.h"

void run_mnist_inference(float input_image[1][1][28][28], float output[1][10]) {
    float pad_temp[1][1][32][32];
    for (ap_int<32> index_tuple = 0; index_tuple < 32; ++index_tuple) {
        for (ap_int<32> i = 0; i < 32; ++i) {
            pad_temp[0][0][index_tuple][i] = (((((2 <= index_tuple) && (index_tuple < 30)) && (2 <= i)) && (i < 30)) ? input_image[((((i - ((i + -2) % 28)) + (index_tuple * 28)) + -58) / 784)][0][(((((i - ((i + -2) % 28)) + (index_tuple * 28)) + -58) / 28) % 28)][((i + -2) % 28)] : 0.000000e+00f);
        }
    }
    float conv1[1][8][28][28];
    for (ap_int<32> ff = 0; ff < 8; ++ff) {
        for (ap_int<32> yy = 0; yy < 28; ++yy) {
            for (ap_int<32> xx = 0; xx < 28; ++xx) {
                float reducer5;
                reducer5 = 0.000000e+00f;
                for (ap_int<32> ry = 0; ry < 5; ++ry) {
                    for (ap_int<32> rx = 0; rx < 5; ++rx) {
                        reducer5 = ((pad_temp[0][0][(yy + ry)][(xx + rx)] * w_conv1[ff][0][ry][rx]) + reducer5);
                    }
                }
                conv1[0][ff][yy][xx] = reducer5;
            }
        }
    }
    float add1[1][8][28][28];
    for (ap_int<32> i1 = 0; i1 < 1; ++i1) {
        for (ap_int<32> j = 0; j < 8; ++j) {
            for (ap_int<32> k = 0; k < 28; ++k) {
                for (ap_int<32> l = 0; l < 28; ++l) {
                    add1[i1][j][k][l] = (conv1[i1][j][k][l] + w_add1[((i1 * 2) + j)][0][0]);
                }
            }
        }
    }
    float relu1[1][8][28][28];
    for (ap_int<32> i2 = 0; i2 < 1; ++i2) {
        for (ap_int<32> args0 = 0; args0 < 8; ++args0) {
            for (ap_int<32> args1 = 0; args1 < 28; ++args1) {
                for (ap_int<32> args2 = 0; args2 < 28; ++args2) {
                    relu1[i2][args0][args1][args2] = ((add1[i2][args0][args1][args2] < 0.000000e+00f) ? 0.000000e+00f : add1[i2][args0][args1][args2]);
                }
            }
        }
    }
    float pool1[1][8][14][14];
    for (ap_int<32> i3 = 0; i3 < 1; ++i3) {
        for (ap_int<32> c = 0; c < 8; ++c) {
            for (ap_int<32> h = 0; h < 14; ++h) {
                for (ap_int<32> w = 0; w < 14; ++w) {
                    float reducer6;
                    reducer6 = -1.000000e+00f;
                    for (ap_int<32> ra5 = 0; ra5 < 2; ++ra5) {
                        for (ap_int<32> ra6 = 0; ra6 < 2; ++ra6) {
                            reducer6 = std::max(relu1[i3][c][((h * 2) + ra5)][((w * 2) + ra6)], reducer6);
                        }
                    }
                    pool1[i3][c][h][w] = reducer6;
                }
            }
        }
    }
    float pad_temp1[1][8][18][18];
    for (ap_int<32> not_zero = 0; not_zero < 8; ++not_zero) {
        for (ap_int<32> index_tuple1 = 0; index_tuple1 < 18; ++index_tuple1) {
            for (ap_int<32> i4 = 0; i4 < 18; ++i4) {
                pad_temp1[0][not_zero][index_tuple1][i4] = (((((2 <= index_tuple1) && (index_tuple1 < 16)) && (2 <= i4)) && (i4 < 16)) ? pool1[(((((i4 - ((i4 + -2) % 14)) + (index_tuple1 * 14)) + (not_zero * 196)) + -30) / 1568)][((((((i4 - ((i4 + -2) % 14)) + (index_tuple1 * 14)) + (not_zero * 196)) + -30) / 196) % 8)][((((((i4 - ((i4 + -2) % 14)) + (index_tuple1 * 14)) + (not_zero * 196)) + -30) / 14) % 14)][((i4 + -2) % 14)] : 0.000000e+00f);
            }
        }
    }
    float conv2[1][16][14][14];
    for (ap_int<32> ff1 = 0; ff1 < 16; ++ff1) {
        for (ap_int<32> yy1 = 0; yy1 < 14; ++yy1) {
            for (ap_int<32> xx1 = 0; xx1 < 14; ++xx1) {
                float reducer7;
                reducer7 = 0.000000e+00f;
                for (ap_int<32> rc = 0; rc < 8; ++rc) {
                    for (ap_int<32> ry1 = 0; ry1 < 5; ++ry1) {
                        for (ap_int<32> rx1 = 0; rx1 < 5; ++rx1) {
                            reducer7 = ((pad_temp1[0][rc][(yy1 + ry1)][(xx1 + rx1)] * w_conv2[ff1][rc][ry1][rx1]) + reducer7);
                        }
                    }
                }
                conv2[0][ff1][yy1][xx1] = reducer7;
            }
        }
    }
    float add2[1][16][14][14];
    for (ap_int<32> i5 = 0; i5 < 1; ++i5) {
        for (ap_int<32> j1 = 0; j1 < 16; ++j1) {
            for (ap_int<32> k1 = 0; k1 < 14; ++k1) {
                for (ap_int<32> l1 = 0; l1 < 14; ++l1) {
                    add2[i5][j1][k1][l1] = (conv2[i5][j1][k1][l1] + w_add2[((i5 * 2) + j1)][0][0]);
                }
            }
        }
    }
    float relu2[1][16][14][14];
    for (ap_int<32> i6 = 0; i6 < 1; ++i6) {
        for (ap_int<32> args01 = 0; args01 < 16; ++args01) {
            for (ap_int<32> args11 = 0; args11 < 14; ++args11) {
                for (ap_int<32> args21 = 0; args21 < 14; ++args21) {
                    relu2[i6][args01][args11][args21] = ((add2[i6][args01][args11][args21] < 0.000000e+00f) ? 0.000000e+00f : add2[i6][args01][args11][args21]);
                }
            }
        }
    }
    float pool2[1][16][4][4];
    for (ap_int<32> i7 = 0; i7 < 1; ++i7) {
        for (ap_int<32> c1 = 0; c1 < 16; ++c1) {
            for (ap_int<32> h1 = 0; h1 < 4; ++h1) {
                for (ap_int<32> w1 = 0; w1 < 4; ++w1) {
                    float reducer8;
                    reducer8 = -1.000000e+00f;
                    for (ap_int<32> ra7 = 0; ra7 < 3; ++ra7) {
                        for (ap_int<32> ra8 = 0; ra8 < 3; ++ra8) {
                            reducer8 = std::max(relu2[i7][c1][((h1 * 3) + ra7)][((w1 * 3) + ra8)], reducer8);
                        }
                    }
                    pool2[i7][c1][h1][w1] = reducer8;
                }
            }
        }
    }
    float flat1[1][256];
    for (ap_int<32> i8 = 0; i8 < 1; ++i8) {
        for (ap_int<32> j2 = 0; j2 < 256; ++j2) {
            flat1[i8][j2] = pool2[i8][(j2 / 16)][((j2 / 4) % 4)][(j2 % 4)];
        }
    }
    float matmul11[1][10];
    for (ap_int<32> i9 = 0; i9 < 1; ++i9) {
        for (ap_int<32> j3 = 0; j3 < 10; ++j3) {
            float reducer9;
            reducer9 = 0.000000e+00f;
            for (ap_int<32> ra9 = 0; ra9 < 256; ++ra9) {
                reducer9 = ((flat1[i9][ra9] * w_reshape1[ra9][j3]) + reducer9);
            }
            matmul11[i9][j3] = reducer9;
        }
    }
    for (ap_int<32> i10 = 0; i10 < 1; ++i10) {
        for (ap_int<32> j4 = 0; j4 < 10; ++j4) {
            output[i10][j4] = (matmul11[i10][j4] + w_add3[i10][j4]);
        }
    }
}
