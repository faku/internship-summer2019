#include "mnist.h"

#include <float.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

// #define RUN_ALL

int main(void) {
    float output [1][10] = { 0 };
    float input_image [1][1][28][28] = { 0 };
    uint8_t label, prediction;
    FILE *fp_images, *fp_labels;
    int i, j, k;
    float max;
    size_t correct_sum;

    fp_images = fopen("t10k-images-idx3-ubyte", "rb");
    if (fp_images == NULL) {
        perror("fopen():");
        return EXIT_FAILURE;
    }

    fp_labels = fopen("t10k-labels-idx1-ubyte", "rb");
    if (fp_labels == NULL) {
        perror("fopen():");
        return EXIT_FAILURE;
    }

    fseek(fp_images, 4 * sizeof(uint32_t), SEEK_CUR);
    fseek(fp_labels, 2 * sizeof(uint32_t), SEEK_CUR);
    correct_sum = 0;

#ifdef RUN_ALL
    for (i = 0; i < 10000; ++i) {
#else
    for (i = 0; i < 1; ++i) {
#endif
        fprintf(stdout, "Inferring image %u...\n", i);

        for (j = 0; j < 28; ++j) {
            for (k = 0; k < 28; ++k) {
                input_image[0][0][j][k] = (float)(fgetc(fp_images)/255.0f);
            }
        }

        label = (uint8_t)fgetc(fp_labels);

        run_mnist_inference(input_image, output);

        max = FLT_MIN;
        prediction = 0;

        for (k = 0; k < 10; ++k) {
            if (output[0][k] > max) {
                max = output[0][k];
                prediction = k;
            }
        }

        if (prediction == label) {
            ++correct_sum;
        }
    }

#ifdef RUN_ALL
    fprintf(stdout, "Accuracy: %f", (correct_sum / 10000.0f));
#else
    fprintf(stdout, "Actual: %u, expected: %u\n", prediction, label);
#endif

    return 0;
}
