// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.2 (lin64) Build 1909853 Thu Jun 15 18:39:10 MDT 2017
// Date        : Mon Aug  5 14:16:41 2019
// Host        : ubuntu running 64-bit Ubuntu 16.04.6 LTS
// Command     : write_verilog -force -mode synth_stub
//               /home/faku/internship-summer2019/src/hcl/mnist/vivado/mnist.srcs/sources_1/bd/design_1/ip/design_1_run_mnist_inference_0_0/design_1_run_mnist_inference_0_0_stub.v
// Design      : design_1_run_mnist_inference_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "run_mnist_inference,Vivado 2017.2" *)
module design_1_run_mnist_inference_0_0(s_axi_MNIST_AWADDR, s_axi_MNIST_AWVALID, 
  s_axi_MNIST_AWREADY, s_axi_MNIST_WDATA, s_axi_MNIST_WSTRB, s_axi_MNIST_WVALID, 
  s_axi_MNIST_WREADY, s_axi_MNIST_BRESP, s_axi_MNIST_BVALID, s_axi_MNIST_BREADY, 
  s_axi_MNIST_ARADDR, s_axi_MNIST_ARVALID, s_axi_MNIST_ARREADY, s_axi_MNIST_RDATA, 
  s_axi_MNIST_RRESP, s_axi_MNIST_RVALID, s_axi_MNIST_RREADY, ap_clk, ap_rst_n, interrupt)
/* synthesis syn_black_box black_box_pad_pin="s_axi_MNIST_AWADDR[13:0],s_axi_MNIST_AWVALID,s_axi_MNIST_AWREADY,s_axi_MNIST_WDATA[31:0],s_axi_MNIST_WSTRB[3:0],s_axi_MNIST_WVALID,s_axi_MNIST_WREADY,s_axi_MNIST_BRESP[1:0],s_axi_MNIST_BVALID,s_axi_MNIST_BREADY,s_axi_MNIST_ARADDR[13:0],s_axi_MNIST_ARVALID,s_axi_MNIST_ARREADY,s_axi_MNIST_RDATA[31:0],s_axi_MNIST_RRESP[1:0],s_axi_MNIST_RVALID,s_axi_MNIST_RREADY,ap_clk,ap_rst_n,interrupt" */;
  input [13:0]s_axi_MNIST_AWADDR;
  input s_axi_MNIST_AWVALID;
  output s_axi_MNIST_AWREADY;
  input [31:0]s_axi_MNIST_WDATA;
  input [3:0]s_axi_MNIST_WSTRB;
  input s_axi_MNIST_WVALID;
  output s_axi_MNIST_WREADY;
  output [1:0]s_axi_MNIST_BRESP;
  output s_axi_MNIST_BVALID;
  input s_axi_MNIST_BREADY;
  input [13:0]s_axi_MNIST_ARADDR;
  input s_axi_MNIST_ARVALID;
  output s_axi_MNIST_ARREADY;
  output [31:0]s_axi_MNIST_RDATA;
  output [1:0]s_axi_MNIST_RRESP;
  output s_axi_MNIST_RVALID;
  input s_axi_MNIST_RREADY;
  input ap_clk;
  input ap_rst_n;
  output interrupt;
endmodule
