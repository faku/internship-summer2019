-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.2 (lin64) Build 1909853 Thu Jun 15 18:39:10 MDT 2017
-- Date        : Mon Aug  5 14:16:39 2019
-- Host        : ubuntu running 64-bit Ubuntu 16.04.6 LTS
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_run_mnist_inference_0_0_stub.vhdl
-- Design      : design_1_run_mnist_inference_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    s_axi_MNIST_AWADDR : in STD_LOGIC_VECTOR ( 13 downto 0 );
    s_axi_MNIST_AWVALID : in STD_LOGIC;
    s_axi_MNIST_AWREADY : out STD_LOGIC;
    s_axi_MNIST_WDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_MNIST_WSTRB : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_MNIST_WVALID : in STD_LOGIC;
    s_axi_MNIST_WREADY : out STD_LOGIC;
    s_axi_MNIST_BRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_MNIST_BVALID : out STD_LOGIC;
    s_axi_MNIST_BREADY : in STD_LOGIC;
    s_axi_MNIST_ARADDR : in STD_LOGIC_VECTOR ( 13 downto 0 );
    s_axi_MNIST_ARVALID : in STD_LOGIC;
    s_axi_MNIST_ARREADY : out STD_LOGIC;
    s_axi_MNIST_RDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_MNIST_RRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_MNIST_RVALID : out STD_LOGIC;
    s_axi_MNIST_RREADY : in STD_LOGIC;
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    interrupt : out STD_LOGIC
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "s_axi_MNIST_AWADDR[13:0],s_axi_MNIST_AWVALID,s_axi_MNIST_AWREADY,s_axi_MNIST_WDATA[31:0],s_axi_MNIST_WSTRB[3:0],s_axi_MNIST_WVALID,s_axi_MNIST_WREADY,s_axi_MNIST_BRESP[1:0],s_axi_MNIST_BVALID,s_axi_MNIST_BREADY,s_axi_MNIST_ARADDR[13:0],s_axi_MNIST_ARVALID,s_axi_MNIST_ARREADY,s_axi_MNIST_RDATA[31:0],s_axi_MNIST_RRESP[1:0],s_axi_MNIST_RVALID,s_axi_MNIST_RREADY,ap_clk,ap_rst_n,interrupt";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "run_mnist_inference,Vivado 2017.2";
begin
end;
