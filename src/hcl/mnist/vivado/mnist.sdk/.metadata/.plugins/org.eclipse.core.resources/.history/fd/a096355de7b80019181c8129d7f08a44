#include <stdio.h>
#include <stdlib.h>

#include "platform.h"
#include "xil_printf.h"
#include "xrun_mnist_inference.h"
#include "xscugic.h"

// Hardware instance.
static XRun_mnist_inference mnist;

// Interrupt Controller instance.
static XScuGic scu_gic;

// Keep track of status.
static u8 done;

/*
 * Wrapper for hardware initialization.
 */
int mnist_init(XRun_mnist_inference *instance) {
	XRun_mnist_inference_Config *config;
	int status;

	config = XRun_mnist_inference_LookupConfig(XPAR_RUN_MNIST_INFERENCE_0_DEVICE_ID);
	if (config == NULL) {
		print("ERROR: Configuration lookup failed!\n");

		return XST_FAILURE;
	}

	status = XRun_mnist_inference_CfgInitialize(instance, config);
	if (status != XST_SUCCESS) {
		print("ERROR: Initialization failed!\n");

		return XST_FAILURE;
	}

	return status;
}

/*
 * Start hardware accelerator.
 */
void mnist_start(XRun_mnist_inference *instance) {
	XRun_mnist_inference_InterruptEnable(instance, 1);
	XRun_mnist_inference_InterruptGlobalEnable(instance);
	XRun_mnist_inference_Start(instance);
}

/*
 * Interrupt routine.
 */
void mnist_isr(void *cookie) {
	XRun_mnist_inference *instance = (XRun_mnist_inference *)cookie;

	// Disable global interrupt.
	XRun_mnist_inference_InterruptGlobalDisable(instance);
	// Disable local interrupt.
	XRun_mnist_inference_InterruptDisable(instance, 0xFFFFFFFF);
	// Clear local interrupt.
	XRun_mnist_inference_InterruptClear(instance, 1);

	print("GOT INTERRUPT\n");

	done = 1;
}

/*
 * ISR setup.
 */
int irq_setup(void) {
	XScuGic_Config *config;
	int status;

	config = XScuGic_LookupConfig(XPAR_SCUGIC_SINGLE_DEVICE_ID);
	if (config == NULL) {
		print("ERROR: Interrupt configuration lookup failed!\n");

		return XST_FAILURE;
	}

	status = XScuGic_CfgInitialize(&scu_gic, config, config->CpuBaseAddress);
	if (status != XST_SUCCESS) {
		return status;
	}

	// Self test.
	status = XScuGic_SelfTest(&scu_gic);
	if (status != XST_SUCCESS) {
		return status;
	}

	// Initialize exception handler.
	Xil_ExceptionInit();
	// Register exception handler.
	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT, (Xil_ExceptionHandler)XScuGic_InterruptHandler, &scu_gic);
	// Enable exception handler.
	Xil_ExceptionEnable();

	// Connect the ISR to the exception table.
	status = XScuGic_Connect(&scu_gic, XPAR_FABRIC_RUN_MNIST_INFERENCE_0_INTERRUPT_INTR, (Xil_InterruptHandler)mnist_isr, &mnist);
	if (status != XST_SUCCESS) {
		return status;
	}

	// Enable ISR.
	XScuGic_Enable(&scu_gic, XPAR_FABRIC_RUN_MNIST_INFERENCE_0_INTERRUPT_INTR);

	return XST_SUCCESS;
}

/*
 * Main program.
 */
int main() {
	int i, status;
	float *input_image, *output;

    init_platform();

    done = 0;

    printf("Program started...\n");

    status = mnist_init(&mnist);
    if (status != XST_SUCCESS) {
    	printf("ERROR: Accelerator setup failed\n");

    	return EXIT_FAILURE;
    }

    status = irq_setup();
    if (status != XST_SUCCESS) {
    	printf("ERROR: Interrupt setup failed\n");

    	return EXIT_FAILURE;
    }

    input_image = (float *)XRun_mnist_inference_Get_input_image_BaseAddress(&mnist);
    output = (float *)XRun_mnist_inference_Get_output_r_BaseAddress(&mnist);

    for (i = 0; i < 28 * 28; ++i) {
    	input_image[i] = 0.0f;
    }

    for (i = 0; i < 10; ++i) {
    	output[i] = 0.0f;
    }

    if (XRun_mnist_inference_IsReady(&mnist) != 0) {
    	printf("Accelerator is ready. Starting...\n");
    }
    else {
    	printf("ERROR: Accelerator is not ready. Exiting\n");

    	return EXIT_FAILURE;
    }

    if (0) {
    	mnist_start(&mnist);
    	while (done == 0);
    	done = 0;
    	mnist_start(&mnist);
    }
    else {
    	XRun_mnist_inference_Start(&mnist);
    	while (XRun_mnist_inference_IsReady(&mnist) == 0);
    	printf("Result received\n");
    }

    print("Result: ");
    for (i = 0; i < 10; ++i) {
    	printf("%.8f ", output[i]);
    }


    cleanup_platform();

    return EXIT_SUCCESS;
}
