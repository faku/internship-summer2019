#include <stdio.h>

#define IMAGES_FILENAME	"t10k-images-idx3-ubyte"
#define LABELS_FILENAME "t10k-labels-idx1-ubyte"

#define IMAGES_OFFSET	(4 * sizeof(uint32_t))
#define LABELS_OFFSET 	(2 * sizeof(uint32_t))

#define IMAGE_SIZE		(28 * 28)
#define LABEL_SIZE		sizeof(uint8_t)

static FILE *fp_images;
static FILE *fp_labels;

static uint8_t files_opened = 0;

int open_files(void) {
	fp_images =	fopen(IMAGES_FILENAME, "rb");
	if (fp_images == NULL) {
		printf("ERROR: Could not open images dataset\n");

		return -1;
	}

	fp_labels = fopen(LABELS_FILENAME, "rb");
	if (fp_labels == NULL) {
		printf("ERROR: Could not open labels dataset\n");

		return -1;
	}

	files_opened = 1;

	return 0;
}

void close_files(void) {
	if (files_opened == 0) {
		return;
	}

	fclose(fp_images);
	fclose(fp_labels);

	files_opened = 0;
}

void get_image_at(size_t index, float *image) {
	if (files_opened == 0) {
		printf("WARNING: Images file not opened\n");

		return;
	}

	fseek(fp_images, IMAGES_OFFSET + (IMAGE_SIZE * index), SEEK_CUR);

	fread(image, sizeof(float), IMAGE_SIZE, fp_images);
}

void get_label_at(size_t index, uint8_t *label) {
	if (files_opened == 0) {
		printf("WARNING: Images file not opened\n");

		return;
	}

	fseek(fp_labels, LABELS_OFFSET + (LABEL_SIZE * index), SEEK_CUR);

	fread(label, sizeof(uint8_t), 1, fp_labels);
}
