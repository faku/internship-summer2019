#ifndef __MNIST_UTILS_H__
#define __MNIST_UTILS_H__

int  open_files(void);
void close_files(void);
void get_image_at(size_t index, float *image);
void get_label_at(size_t index, uint8_t* label);
uint8_t get_prediction(float *output);

#endif
