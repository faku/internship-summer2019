# pylint: disable=no-member
import sys
import onnx
import numpy as np

from onnx import numpy_helper
from onnx import ModelProto, TensorProto
import onnx.test as test

import mxnet as mx

np.set_printoptions(threshold=sys.maxsize)

# Original ONNX model.
orig_model = onnx.load('./model/model.onnx')

input_image = np.load('verif/input_image.npy')
w_conv1 = np.load('verif/w_conv1.npy')
w_add1 = np.load('verif/w_add1.npy')
w_conv2 = np.load('verif/w_conv2.npy')
w_add2 = np.load('verif/w_add2.npy')
w_reshape1 = np.load('verif/w_reshape1.npy')
w_add3 = np.load('verif/w_add3.npy')

conv1 = np.load('verif/conv1.npy')
add1 = np.load('verif/add1.npy')
relu1 = np.load('verif/relu1.npy')
pool1 = np.load('verif/pool1.npy')
conv2 = np.load('verif/conv2.npy')
add2 = np.load('verif/add2.npy')
relu2 = np.load('verif/relu2.npy')
pool2 = np.load('verif/pool2.npy')
flat1 = np.load('verif/flat1.npy')
flat2 = np.load('verif/flat2.npy')
matmul1 = np.load('verif/matmul1.npy')
add3 = np.load('verif/add3.npy')

shape1 = np.array([1, 256], dtype=np.int64)
shape2 = np.array([256, 10], dtype=np.int64)

input_layer = onnx.helper.make_tensor_value_info(
    'input_image', TensorProto.FLOAT, input_image.shape)

conv1_p = onnx.helper.make_tensor_value_info(
    'conv1', TensorProto.FLOAT, conv1.shape)
add1_p = onnx.helper.make_tensor_value_info(
    'add1',  TensorProto.FLOAT, add1.shape)
relu1_p = onnx.helper.make_tensor_value_info(
    'relu1', TensorProto.FLOAT, relu1.shape)
pool1_p = onnx.helper.make_tensor_value_info(
    'pool1', TensorProto.FLOAT, pool1.shape)
conv2_p = onnx.helper.make_tensor_value_info(
    'conv2', TensorProto.FLOAT, conv2.shape)
add2_p = onnx.helper.make_tensor_value_info(
    'add2',  TensorProto.FLOAT, add2.shape)
relu2_p = onnx.helper.make_tensor_value_info(
    'relu2', TensorProto.FLOAT, relu2.shape)
pool2_p = onnx.helper.make_tensor_value_info(
    'pool2', TensorProto.FLOAT, pool2.shape)
flat1_p = onnx.helper.make_tensor_value_info(
    'flat1', TensorProto.FLOAT, flat1.shape)
flat2_p = onnx.helper.make_tensor_value_info(
    'flat2', TensorProto.FLOAT, flat2.shape)
matmul1_p = onnx.helper.make_tensor_value_info(
    'matmul1', TensorProto.FLOAT, matmul1.shape)
add3_p = onnx.helper.make_tensor_value_info(
    'add3', TensorProto.FLOAT, add3.shape)

w_conv1_p = onnx.helper.make_tensor_value_info(
    'w_conv1',    TensorProto.FLOAT, w_conv1.shape)
w_add1_p = onnx.helper.make_tensor_value_info(
    'w_add1',     TensorProto.FLOAT, w_add1.shape)
w_conv2_p = onnx.helper.make_tensor_value_info(
    'w_conv2',    TensorProto.FLOAT, w_conv2.shape)
w_add2_p = onnx.helper.make_tensor_value_info(
    'w_add2',     TensorProto.FLOAT, w_add2.shape)
w_reshape1_p = onnx.helper.make_tensor_value_info(
    'w_reshape1', TensorProto.FLOAT, w_reshape1.shape)
w_add3_p = onnx.helper.make_tensor_value_info(
    'w_add3', TensorProto.FLOAT, w_add3.shape)

shape1_p = onnx.helper.make_tensor_value_info(
    'shape1', TensorProto.INT64, shape1.shape)
shape2_p = onnx.helper.make_tensor_value_info(
    'shape2', TensorProto.INT64, shape2.shape)

'''
conv1_node = onnx.helper.make_node(
    'Conv',
    inputs=['input_image', 'w_conv1'],
    outputs=['conv1'],
    kernel_shape=[5, 5],
    auto_pad='SAME_UPPER',
    strides=[1,1]
)
'''

'''
add1_node = onnx.helper.make_node(
    'Add',
    inputs=['conv1', 'w_add1'],
    outputs=['add1']
)
'''

'''
relu1_node = onnx.helper.make_node(
    'Relu',
    inputs=['add1'],
    outputs=['relu1']
)
'''

'''
pool1_node = onnx.helper.make_node(
    'MaxPool',
    inputs=['relu1'],
    outputs=['pool1'],
    kernel_shape=[2,2],
    pads=[0,0,0,0],
    strides=[2,2]
)
'''

'''
conv2_node = onnx.helper.make_node(
    'Conv',
    inputs=['pool1', 'w_conv2'],
    outputs=['conv2'],
    kernel_shape=[5, 5],
    auto_pad='SAME_UPPER',
    strides=[1,1]
)
'''

'''
add2_node = onnx.helper.make_node(
    'Add',
    inputs=['conv2', 'w_add2'],
    outputs=['add2']
)
'''

'''
relu2_node = onnx.helper.make_node(
    'Relu',
    inputs=['add2'],
    outputs=['relu2']
)
'''

'''
pool2_node = onnx.helper.make_node(
    'MaxPool',
    inputs=['relu2'],
    outputs=['pool2'],
    kernel_shape=[3,3],
    pads=[0,0,0,0],
    strides=[3,3]
)
'''

'''
flat1_node = onnx.helper.make_node(
    'Reshape',
    inputs=['pool2', 'shape1'],
    outputs=['flat1']
)
'''

'''
flat2_node = onnx.helper.make_node(
    'Reshape',
    inputs=['w_fc1', 'shape2'],
    outputs=['flat2']
)
'''

'''
matmul1_node = onnx.helper.make_node(
    'MatMul',
    inputs=['flat1', 'w_reshape1'],
    outputs=['matmul1']
)
'''

add3_node = onnx.helper.make_node(
    'Add',
    inputs=['matmul1', 'w_add3'],
    outputs=['add3']
)

graph_def = onnx.helper.make_graph(
    [add3_node],
    'test-model',
    [matmul1_p, w_add3_p],
    [add3_p]
)

model_def = onnx.helper.make_model(graph_def)


def predict(model_def, *inputs):
    import onnxruntime as ort

    sess = ort.InferenceSession(model_def.SerializeToString())
    names = [i.name for i in sess.get_inputs()]
    input = {name: i for name, i in zip(names, inputs)}
    res = sess.run(None, input)
    names = [o.name for o in sess.get_outputs()]

    return {name: output for name, output in zip(names, res)}


# y = predict(model_def, matmul1, w_add3)
y = predict(orig_model, input_image)
result = y['Plus214_Output_0']
original = add3

verification = np.allclose(result, original)

if (verification == True):
    print(result)
else:
    print(original)
    print(w_add3)
    print(np.subtract(result, original))

print('original = result: {}'.format(verification))

print('Expected prediction: {}'.format(
    np.argmax(predict(orig_model, input_image)['Plus214_Output_0'], axis=1)))


batch_size = 1
mnist = mx.test_utils.get_mnist()
correct_sum = 0

for i in range(10000 // batch_size):
    label = mnist['test_label'][i*batch_size:(i+1)*batch_size]
    input_image_np = mnist['test_data'][i*batch_size:(i+1)*batch_size]

    y = predict(orig_model, input_image_np)

    prediction = np.argmax(y['Plus214_Output_0'], axis=1)
    correct_sum += np.sum(np.equal(prediction, label))

print("Testing accuracy: {} ({})".format(correct_sum / 10000., correct_sum))