import heterocl as hcl
import hlib
import heterocl.tvm as tvm

import numpy as np

import _pickle as cPickle

from collections import OrderedDict

DATASET_PATH = 'data/RML2016.10a_dict.pkl'

# Transpose layer with perms=[0,3,2,1]
def transpose(data, name='transpose'):
    s = data.shape
    return hcl.compute((s[0],s[3],s[2],s[1]), lambda i, j, k, l: data[i, l, k, j], name=name,
                       attrs=OrderedDict([('app_name', tvm.make.StringImm('transpose'))]))

# Load weights.
w_conv1d_1_np = np.transpose(np.load('weights/w_conv1d_1.npy'))
b_conv1d_1_np = np.load('weights/b_conv1d_1.npy')
w_conv1d_2_np = np.transpose(np.load('weights/w_conv1d_2.npy'))
b_conv1d_2_np = np.load('weights/b_conv1d_2.npy')
w_dense_1_np = np.load('weights/w_dense_1.npy')
b_dense_1_np = np.load('weights/b_dense_1.npy')
w_dense_2_np = np.load('weights/w_dense_2.npy')
b_dense_2_np = np.load('weights/b_dense_2.npy')
w_dense_3_np = np.load('weights/w_dense_3.npy')
b_dense_3_np = np.load('weights/b_dense_3.npy')
w_dense_4_np = np.load('weights/w_dense_4.npy')
b_dense_4_np = np.load('weights/b_dense_4.npy')

w_conv1d_1_np = w_conv1d_1_np.reshape((w_conv1d_1_np.shape[0], w_conv1d_1_np.shape[1], 1, w_conv1d_1_np.shape[2]))
w_conv1d_2_np = w_conv1d_2_np.reshape((w_conv1d_2_np.shape[0], w_conv1d_2_np.shape[1], 1, w_conv1d_2_np.shape[2]))

hcl.init(hcl.Float())

qtype1 = hcl.Float()
batch_size = 1
target = 'llvm'

input_data = np.load('verif/input_data.npy').reshape(1,128,1,2)

def build_network(input_data,
                  w_conv1d_1, b_conv1d_1,
                  w_conv1d_2, b_conv1d_2,
                  w_dense_1, b_dense_1,
                  w_dense_2, b_dense_2,
                  w_dense_3, b_dense_3,
                  w_dense_4, b_dense_4):
    # Transpose data
    transpose_1 = transpose(input_data, 'transpose_1')
    print(transpose_1)

    # First 1D-Convolutional layer
    conv1d_1 = hlib.nn.conv2d_nchw(transpose_1, w_conv1d_1, bias=b_conv1d_1, name='conv1d_1')
    print(conv1d_1)
    relu_1 = hlib.nn.relu(conv1d_1, name='relu_1')
    print(relu_1)
    maxpool1d_1 = hlib.nn.max_pool(relu_1, kernel=(1,2), stride=(1,2), name='maxpool1d_1')
    print(maxpool1d_1)

    # Second 1D-Convolutional layer
    conv1d_2 = hlib.nn.conv2d_nchw(maxpool1d_1, w_conv1d_2, bias=b_conv1d_2, name='conv1d_2')
    print(conv1d_2)
    relu_2 = hlib.nn.relu(conv1d_2, name='relu_2')
    print(relu_2)
    maxpool1d_2 = hlib.nn.max_pool(relu_2, kernel=(1,2), stride=(1,2), name='maxpool1d_2')
    print(maxpool1d_2)

    # Flatten layer
    transpose_2 = transpose(maxpool1d_2, 'transpose_2')
    print(transpose_2)
    flatten_1 = hlib.nn.flatten(transpose_2, name='flatten_1')
    print(flatten_1)

    # First Dense layer
    dense_1 = hlib.nn.dense(flatten_1, w_dense_1, bias=b_dense_1, name='dense_1')
    print(dense_1)
    relu_3 = hlib.nn.relu(dense_1, name='relu_3')
    print(relu_3)

    # Second Dense layer
    dense_2 = hlib.nn.dense(relu_3, w_dense_2, bias=b_dense_2, name='dense_2')
    print(dense_2)
    relu_4 = hlib.nn.relu(dense_2, name='relu_4')
    print(relu_4)

    # Third Dense layer
    dense_3 = hlib.nn.dense(relu_4, w_dense_3, bias=b_dense_3, name='dense_3')
    print(dense_3)
    relu_5 = hlib.nn.relu(dense_3, name='relu_5')
    print(relu_5)

    # Fourth Dense layer
    dense_4 = hlib.nn.dense(relu_5, w_dense_4, bias=b_dense_4, name='dense_4')
    print(dense_4)
    softmax_1 = hlib.nn.softmax(dense_4, name='softmax_1')
    print(softmax_1)

    return softmax_1

def build_inference(batch_size=batch_size, target=target):
    input = hcl.placeholder(input_data.shape, 'input_image', qtype1)
    # First 1D-Conv layer weights and biases
    w_conv1d_1 = hcl.placeholder(w_conv1d_1_np.shape, 'w_conv1d_1', qtype1)
    b_conv1d_1 = hcl.placeholder(b_conv1d_1_np.shape, 'b_conv1d_1', qtype1)
    # Second 1D-Conv layer weights and biases
    w_conv1d_2 = hcl.placeholder(w_conv1d_2_np.shape, 'w_conv1d_2', qtype1)
    b_conv1d_2 = hcl.placeholder(b_conv1d_2_np.shape, 'b_conv1d_2', qtype1)
    # First Dense layer weights and biases
    w_dense_1 = hcl.placeholder(w_dense_1_np.shape, 'w_dense_1', qtype1)
    b_dense_1 = hcl.placeholder(b_dense_1_np.shape, 'b_dense_1', qtype1)
    # Second Dense layer weights and biases
    w_dense_2 = hcl.placeholder(w_dense_2_np.shape, 'w_dense_2', qtype1)
    b_dense_2 = hcl.placeholder(b_dense_2_np.shape, 'b_dense_2', qtype1)
    # Third Dense layer weights and biases
    w_dense_3 = hcl.placeholder(w_dense_3_np.shape, 'w_dense_3', qtype1)
    b_dense_3 = hcl.placeholder(b_dense_3_np.shape, 'b_dense_3', qtype1)
    # Fourth Dense layer weights and biases
    w_dense_4 = hcl.placeholder(w_dense_4_np.shape, 'w_dense_4', qtype1)
    b_dense_4 = hcl.placeholder(b_dense_4_np.shape, 'b_dense_4', qtype1)

    scheme = hcl.create_scheme(
        [
            input, w_conv1d_1, b_conv1d_1, w_conv1d_2, b_conv1d_2, w_dense_1, b_dense_1,
            w_dense_2, b_dense_2, w_dense_3, b_dense_3, w_dense_4, b_dense_4
        ],
        build_network
    )

    s = hcl.create_schedule_from_scheme(scheme)

    return hcl.build(s, target=target)

f = build_inference()

input_data_hcl = hcl.asarray(input_data, dtype=qtype1)
w_conv1d_1_hcl = hcl.asarray(w_conv1d_1_np, dtype=qtype1)
b_conv1d_1_hcl = hcl.asarray(b_conv1d_1_np, dtype=qtype1)
w_conv1d_2_hcl = hcl.asarray(w_conv1d_2_np, dtype=qtype1)
b_conv1d_2_hcl = hcl.asarray(b_conv1d_2_np, dtype=qtype1)
w_dense_1_hcl = hcl.asarray(w_dense_1_np, dtype=qtype1)
b_dense_1_hcl = hcl.asarray(b_dense_1_np, dtype=qtype1)
w_dense_2_hcl = hcl.asarray(w_dense_2_np, dtype=qtype1)
b_dense_2_hcl = hcl.asarray(b_dense_2_np, dtype=qtype1)
w_dense_3_hcl = hcl.asarray(w_dense_3_np, dtype=qtype1)
b_dense_3_hcl = hcl.asarray(b_dense_3_np, dtype=qtype1)
w_dense_4_hcl = hcl.asarray(w_dense_4_np, dtype=qtype1)
b_dense_4_hcl = hcl.asarray(b_dense_4_np, dtype=qtype1)
output_hcl = hcl.asarray(np.zeros(shape=(1,10)), dtype=qtype1)

f(input_data_hcl,
  w_conv1d_1_hcl, b_conv1d_1_hcl,
  w_conv1d_2_hcl, b_conv1d_2_hcl,
  w_dense_1_hcl, b_dense_1_hcl,
  w_dense_2_hcl, b_dense_2_hcl,
  w_dense_3_hcl, b_dense_3_hcl,
  w_dense_4_hcl, b_dense_4_hcl,
  output_hcl)

output_np = output_hcl.asnumpy()
print(output_np)

# Test accuracy
# Load the dataset
Xd = cPickle.load(open("data/RML2016.10a_dict.pkl",'rb'), encoding='latin1')
snrs,mods = map(lambda j: sorted(list(set(map(lambda x: x[j], Xd.keys())))), [1,0])
X = []  
lbl = []
for mod in mods:
    for snr in snrs:
        X.append(Xd[(mod,snr)])
        for i in range(Xd[(mod,snr)].shape[0]):  lbl.append((mod,snr))
X = np.vstack(X)


if False:
    from keras.models import load_model

    model = load_model('model/VGG_RML2016.10b_20190426084923.h5', compile=False)
    total = 0
    iters = 1000

    for i in range(iters):
        input_data = np.transpose(X[0]).reshape(1,128,2)
        input_data_hcl = hcl.asarray(input_data.reshape(1,128,1,2), dtype=qtype1)
        output_hcl = hcl.asarray(np.zeros(shape=(1,10)), dtype=qtype1)

        f(input_data_hcl,
            w_conv1d_1_hcl, b_conv1d_1_hcl,
            w_conv1d_2_hcl, b_conv1d_2_hcl,
            w_dense_1_hcl, b_dense_1_hcl,
            w_dense_2_hcl, b_dense_2_hcl,
            w_dense_3_hcl, b_dense_3_hcl,
            w_dense_4_hcl, b_dense_4_hcl,
            output_hcl
        )

        hcl_pred = np.argmax(output_hcl.asnumpy(), axis=1)
        model_pred = np.argmax(model.predict(input_data, batch_size=1), axis=1)

        if (hcl_pred == model_pred):
            total += 1

    print('Accuracy: {}'.format(total / iters))

f = build_inference(target='vhls')
print(f)