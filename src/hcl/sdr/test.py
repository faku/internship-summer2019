 # pylint: disable=no-member

import numpy as np

import onnx
from onnx import TensorProto

input_data = np.load('verif/input_data.npy')
w_conv1d_1 = np.load('weights/w_conv1d_1.npy').reshape((128,2,8))
b_conv1d_1 = np.load('weights/b_conv1d_1.npy')
w_conv1d_2 = np.load('weights/w_conv1d_2.npy').reshape((64,128,16))
b_conv1d_2 = np.load('weights/b_conv1d_2.npy')
w_dense_1 = np.load('weights/w_dense_1.npy')
b_dense_1 = np.load('weights/b_dense_1.npy')
w_dense_2 = np.load('weights/w_dense_2.npy')
b_dense_2 = np.load('weights/b_dense_2.npy')
w_dense_3 = np.load('weights/w_dense_3.npy')
b_dense_3 = np.load('weights/b_dense_3.npy')
w_dense_4 = np.load('weights/w_dense_4.npy')
b_dense_4 = np.load('weights/b_dense_4.npy')

input_layer = onnx.helper.make_tensor_value_info(
    'input_data', TensorProto.FLOAT, input_data.shape
)
conv1d_1_p = onnx.helper.make_tensor_value_info(
    'conv1d_1', TensorProto.FLOAT, (1,128,121)
)
w_conv1d_1_p = onnx.helper.make_tensor_value_info(
    'w_conv1d_1', TensorProto.FLOAT, w_conv1d_1.shape
)
b_conv1d_1_p = onnx.helper.make_tensor_value_info(
    'b_conv1d_1', TensorProto.FLOAT, b_conv1d_1.shape
)
relu_1_p = onnx.helper.make_tensor_value_info(
    'relu_1', TensorProto.FLOAT, (1,128,121)
)
maxpool1d_1_p = onnx.helper.make_tensor_value_info(
    'maxpool1d_1', TensorProto.FLOAT, (1,128,60)
)
conv1d_2_p = onnx.helper.make_tensor_value_info(
    'conv1d_2', TensorProto.FLOAT, (1,64,45)
)
w_conv1d_2_p = onnx.helper.make_tensor_value_info(
    'w_conv1d_2', TensorProto.FLOAT, w_conv1d_2.shape
)
b_conv1d_2_p = onnx.helper.make_tensor_value_info(
    'b_conv1d_2', TensorProto.FLOAT, b_conv1d_2.shape
)
relu_2_p = onnx.helper.make_tensor_value_info(
    'relu_2', TensorProto.FLOAT, (1,64,45)
)
maxpool1d_2_p = onnx.helper.make_tensor_value_info(
    'maxpool1d_2', TensorProto.FLOAT, (1,64,22)
)
transpose_1_p = onnx.helper.make_tensor_value_info(
    'transpose_1', TensorProto.FLOAT, (1,22,64)
)
reshape_1_p = onnx.helper.make_tensor_value_info(
    'reshape_1', TensorProto.FLOAT, (1,1408)
)
shape_1_p = onnx.helper.make_tensor_value_info(
    'shape_1', TensorProto.INT64, (2,)
)
w_dense_1_p = onnx.helper.make_tensor_value_info(
    'w_dense_1', TensorProto.FLOAT, (1408,128)
)
b_dense_1_p = onnx.helper.make_tensor_value_info(
    'b_dense_1', TensorProto.FLOAT, (128,)
)
matmul_1_p = onnx.helper.make_tensor_value_info(
    'matmul_1', TensorProto.FLOAT, (1,128)
)
add_1_p = onnx.helper.make_tensor_value_info(
    'add_1', TensorProto.FLOAT, (1,128)
)
relu_3_p = onnx.helper.make_tensor_value_info(
    'relu_3', TensorProto.FLOAT, (1,128)
)
w_dense_2_p = onnx.helper.make_tensor_value_info(
    'w_dense_2', TensorProto.FLOAT, (128,64)
)
b_dense_2_p = onnx.helper.make_tensor_value_info(
    'b_dense_2', TensorProto.FLOAT, (64,)
)
matmul_2_p = onnx.helper.make_tensor_value_info(
    'matmul_2', TensorProto.FLOAT, (1,64)
)
add_2_p = onnx.helper.make_tensor_value_info(
    'add_2', TensorProto.FLOAT, (1,64)
)
relu_4_p = onnx.helper.make_tensor_value_info(
    'relu_4', TensorProto.FLOAT, (1,64)
)
w_dense_3_p = onnx.helper.make_tensor_value_info(
    'w_dense_3', TensorProto.FLOAT, (64,32)
)
b_dense_3_p = onnx.helper.make_tensor_value_info(
    'b_dense_3', TensorProto.FLOAT, (32,)
)
matmul_3_p = onnx.helper.make_tensor_value_info(
    'matmul_3', TensorProto.FLOAT, (1,32)
)
add_3_p = onnx.helper.make_tensor_value_info(
    'add_3', TensorProto.FLOAT, (1,32)
)
relu_5_p = onnx.helper.make_tensor_value_info(
    'relu_5', TensorProto.FLOAT, (1,32)
)
w_dense_4_p = onnx.helper.make_tensor_value_info(
    'w_dense_4', TensorProto.FLOAT, (32,10)
)
b_dense_4_p = onnx.helper.make_tensor_value_info(
    'b_dense_4', TensorProto.FLOAT, (10,)
)
matmul_4_p = onnx.helper.make_tensor_value_info(
    'matmul_4', TensorProto.FLOAT, (1,10)
)
add_4_p = onnx.helper.make_tensor_value_info(
    'add_4', TensorProto.FLOAT, (1,10)
)
softmax_p = onnx.helper.make_tensor_value_info(
    'softmax', TensorProto.FLOAT, (1,10)
)

conv1d_1_node = onnx.helper.make_node(
    'Conv',
    inputs=['input_data', 'w_conv1d_1', 'b_conv1d_1'],
    outputs=['conv1d_1'],
    kernel_shape=[8],
    auto_pad='VALID',
    strides=[1]
)
relu_1_node = onnx.helper.make_node(
    'Relu',
    inputs=['conv1d_1'],
    outputs=['relu_1']
)
maxpool1d_1_node = onnx.helper.make_node(
    'MaxPool',
    inputs=['relu_1'],
    outputs=['maxpool1d_1'],
    kernel_shape=[2],
    strides=[2]
)
conv1d_2_node = onnx.helper.make_node(
    'Conv',
    inputs=['maxpool1d_1', 'w_conv1d_2', 'b_conv1d_2'],
    outputs=['conv1d_2'],
    kernel_shape=[16],
    auto_pad='VALID',
    strides=[1]
)
relu_2_node = onnx.helper.make_node(
    'Relu',
    inputs=['conv1d_2'],
    outputs=['relu_2']
)
maxpool1d_2_node = onnx.helper.make_node(
    'MaxPool',
    inputs=['relu_2'],
    outputs=['maxpool1d_2'],
    kernel_shape=[2],
    strides=[2]
)
transpose_1_node = onnx.helper.make_node(
    'Transpose',
    inputs=['maxpool1d_2'],
    outputs=['transpose_1'],
    perm=[0,2,1]
)
reshape_1_node = onnx.helper.make_node(
    'Reshape',
    inputs=['transpose_1', 'shape_1'],
    outputs=['reshape_1']
)
matmul_1_node = onnx.helper.make_node(
    'MatMul',
    inputs=['reshape_1', 'w_dense_1'],
    outputs=['matmul_1']
)
add_1_node = onnx.helper.make_node(
    'Add',
    inputs=['matmul_1', 'b_dense_1'],
    outputs=['add_1']
)
relu_3_node = onnx.helper.make_node(
    'Relu',
    inputs=['add_1'],
    outputs=['relu_3']
)
matmul_2_node = onnx.helper.make_node(
    'MatMul',
    inputs=['relu_3', 'w_dense_2'],
    outputs=['matmul_2']
)
add_2_node = onnx.helper.make_node(
    'Add',
    inputs=['matmul_2', 'b_dense_2'],
    outputs=['add_2']
)
relu_4_node = onnx.helper.make_node(
    'Relu',
    inputs=['add_2'],
    outputs=['relu_4']
)
matmul_3_node = onnx.helper.make_node(
    'MatMul',
    inputs=['relu_4', 'w_dense_3'],
    outputs=['matmul_3']
)
add_3_node = onnx.helper.make_node(
    'Add',
    inputs=['matmul_3', 'b_dense_3'],
    outputs=['add_3']
)
relu_5_node = onnx.helper.make_node(
    'Relu',
    inputs=['add_3'],
    outputs=['relu_5']
)
matmul_4_node = onnx.helper.make_node(
    'MatMul',
    inputs=['relu_5', 'w_dense_4'],
    outputs=['matmul_4']
)
add_4_node = onnx.helper.make_node(
    'Add',
    inputs=['matmul_4', 'b_dense_4'],
    outputs=['add_4']
)
softmax_node = onnx.helper.make_node(
    'Softmax',
    inputs=['add_4'],
    outputs=['softmax']
)


graph_def = onnx.helper.make_graph(
    [
        conv1d_1_node, relu_1_node, maxpool1d_1_node, conv1d_2_node, relu_2_node,
        maxpool1d_2_node, transpose_1_node, reshape_1_node, matmul_1_node, add_1_node,
        relu_3_node, matmul_2_node, add_2_node, relu_4_node, matmul_3_node, add_3_node,
        relu_5_node, matmul_4_node, add_4_node, softmax_node
    ],
    'verification-model',
    [
        input_layer, w_conv1d_1_p, b_conv1d_1_p, w_conv1d_2_p, b_conv1d_2_p, shape_1_p,
        w_dense_1_p, b_dense_1_p, w_dense_2_p, b_dense_2_p, w_dense_3_p, b_dense_3_p,
        w_dense_4_p, b_dense_4_p
    ],
    [softmax_p]
)

model_def = onnx.helper.make_model(graph_def)

def predict(model_def, *inputs):
    import onnxruntime as ort

    sess = ort.InferenceSession(model_def.SerializeToString())
    names = [i.name for i in sess.get_inputs()]
    input = {name: i for name, i in zip(names, inputs)}
    res = sess.run(None, input)
    names = [o.name for o in sess.get_outputs()]

    return {name: output for name, output in zip(names, res)}

y = predict(model_def, input_data, w_conv1d_1, b_conv1d_1, w_conv1d_2, b_conv1d_2,
    np.array([-1,1408]), w_dense_1, b_dense_1, w_dense_2, b_dense_2, w_dense_3, b_dense_3,
    w_dense_4, b_dense_4)
result = y['softmax']
output = np.load('verif/output.npy')

print(result.shape)
print(output.shape)

verification = np.allclose(result, output, rtol=1.e-4)

print(np.subtract(result, output))

print(verification)

print(np.load('verif/input_data.npy').shape)