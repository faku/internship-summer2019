#include <ap_int.h>
#include <ap_fixed.h>
#include <math.h>

void default_function(float input_image[1][128][1][2], float w_conv1d_1[128][2][1][8], float b_conv1d_1[128], float w_conv1d_2[64][128][1][16], float b_conv1d_2[64], float w_dense_1[1408][128], float b_dense_1[128], float w_dense_2[128][64], float b_dense_2[64], float w_dense_3[64][32], float b_dense_3[32], float w_dense_4[32][10], float b_dense_4[10], float softmax_1[1][10]) {
  float transpose_1[1][2][1][128];
  for (ap_int<32> i = 0; i < 1; ++i) {
    for (ap_int<32> j = 0; j < 2; ++j) {
      for (ap_int<32> l = 0; l < 128; ++l) {
        transpose_1[i][j][0][l] = input_image[i][l][0][j];
      }
    }
  }
  float pad_temp[1][2][1][128];
  for (ap_int<32> not_zero = 0; not_zero < 2; ++not_zero) {
    for (ap_int<32> i1 = 0; i1 < 128; ++i1) {
      pad_temp[0][not_zero][0][i1] = transpose_1[0][not_zero][0][i1];
    }
  }
  float conv1d_1[1][128][1][121];
  for (ap_int<32> ff = 0; ff < 128; ++ff) {
    for (ap_int<32> xx = 0; xx < 121; ++xx) {
      float reducer30;
      reducer30 = 0.000000e+00f;
      for (ap_int<32> rc = 0; rc < 2; ++rc) {
        for (ap_int<32> rx = 0; rx < 8; ++rx) {
          reducer30 = ((pad_temp[0][rc][0][(xx + rx)] * w_conv1d_1[ff][rc][0][rx]) + reducer30);
        }
      }
      conv1d_1[0][ff][0][xx] = reducer30;
    }
  }
  float conv1d_11[1][128][1][121];
  for (ap_int<32> j1 = 0; j1 < 128; ++j1) {
    for (ap_int<32> l1 = 0; l1 < 121; ++l1) {
      conv1d_11[0][j1][0][l1] = (conv1d_1[0][j1][0][l1] + b_conv1d_1[j1]);
    }
  }
  float relu_1[1][128][1][121];
  for (ap_int<32> args = 0; args < 1; ++args) {
    for (ap_int<32> args0 = 0; args0 < 128; ++args0) {
      for (ap_int<32> args2 = 0; args2 < 121; ++args2) {
        relu_1[args][args0][0][args2] = ((conv1d_11[args][args0][0][args2] < 0.000000e+00f) ? 0.000000e+00f : conv1d_11[args][args0][0][args2]);
      }
    }
  }
  float maxpool1d_1[1][128][1][60];
  for (ap_int<32> i2 = 0; i2 < 1; ++i2) {
    for (ap_int<32> c = 0; c < 128; ++c) {
      for (ap_int<32> w = 0; w < 60; ++w) {
        float reducer31;
        reducer31 = -1.000000e+00f;
        for (ap_int<32> ra31 = 0; ra31 < 2; ++ra31) {
          reducer31 = std::max(relu_1[i2][c][0][((w * 2) + ra31)], reducer31);
        }
        maxpool1d_1[i2][c][0][w] = reducer31;
      }
    }
  }
  float pad_temp1[1][128][1][60];
  for (ap_int<32> not_zero1 = 0; not_zero1 < 128; ++not_zero1) {
    for (ap_int<32> i3 = 0; i3 < 60; ++i3) {
      pad_temp1[0][not_zero1][0][i3] = maxpool1d_1[0][not_zero1][0][i3];
    }
  }
  float conv1d_2[1][64][1][45];
  for (ap_int<32> ff1 = 0; ff1 < 64; ++ff1) {
    for (ap_int<32> xx1 = 0; xx1 < 45; ++xx1) {
      float reducer32;
      reducer32 = 0.000000e+00f;
      for (ap_int<32> rc1 = 0; rc1 < 128; ++rc1) {
        for (ap_int<32> rx1 = 0; rx1 < 16; ++rx1) {
          reducer32 = ((pad_temp1[0][rc1][0][(xx1 + rx1)] * w_conv1d_2[ff1][rc1][0][rx1]) + reducer32);
        }
      }
      conv1d_2[0][ff1][0][xx1] = reducer32;
    }
  }
  float conv1d_21[1][64][1][45];
  for (ap_int<32> j2 = 0; j2 < 64; ++j2) {
    for (ap_int<32> l2 = 0; l2 < 45; ++l2) {
      conv1d_21[0][j2][0][l2] = (conv1d_2[0][j2][0][l2] + b_conv1d_2[j2]);
    }
  }
  float relu_2[1][64][1][45];
  for (ap_int<32> args1 = 0; args1 < 1; ++args1) {
    for (ap_int<32> args01 = 0; args01 < 64; ++args01) {
      for (ap_int<32> args21 = 0; args21 < 45; ++args21) {
        relu_2[args1][args01][0][args21] = ((conv1d_21[args1][args01][0][args21] < 0.000000e+00f) ? 0.000000e+00f : conv1d_21[args1][args01][0][args21]);
      }
    }
  }
  float maxpool1d_2[1][64][1][22];
  for (ap_int<32> i4 = 0; i4 < 1; ++i4) {
    for (ap_int<32> c1 = 0; c1 < 64; ++c1) {
      for (ap_int<32> w1 = 0; w1 < 22; ++w1) {
        float reducer33;
        reducer33 = -1.000000e+00f;
        for (ap_int<32> ra33 = 0; ra33 < 2; ++ra33) {
          reducer33 = std::max(relu_2[i4][c1][0][((w1 * 2) + ra33)], reducer33);
        }
        maxpool1d_2[i4][c1][0][w1] = reducer33;
      }
    }
  }
  float transpose_2[1][22][1][64];
  for (ap_int<32> i5 = 0; i5 < 1; ++i5) {
    for (ap_int<32> j3 = 0; j3 < 22; ++j3) {
      for (ap_int<32> l3 = 0; l3 < 64; ++l3) {
        transpose_2[i5][j3][0][l3] = maxpool1d_2[i5][l3][0][j3];
      }
    }
  }
  float flatten_1[1][1408];
  for (ap_int<32> i6 = 0; i6 < 1; ++i6) {
    for (ap_int<32> j4 = 0; j4 < 1408; ++j4) {
      flatten_1[i6][j4] = transpose_2[i6][(j4 / 64)][0][(j4 % 64)];
    }
  }
  float dense_1[1][128];
  for (ap_int<32> i7 = 0; i7 < 1; ++i7) {
    for (ap_int<32> j5 = 0; j5 < 128; ++j5) {
      float reducer34;
      reducer34 = 0.000000e+00f;
      for (ap_int<32> ra34 = 0; ra34 < 1408; ++ra34) {
        reducer34 = ((flatten_1[i7][ra34] * w_dense_1[ra34][j5]) + reducer34);
      }
      dense_1[i7][j5] = reducer34;
    }
  }
  float dense_11[1][128];
  for (ap_int<32> i8 = 0; i8 < 1; ++i8) {
    for (ap_int<32> j6 = 0; j6 < 128; ++j6) {
      dense_11[i8][j6] = (dense_1[i8][j6] + b_dense_1[j6]);
    }
  }
  float relu_3[1][128];
  for (ap_int<32> args3 = 0; args3 < 1; ++args3) {
    for (ap_int<32> args02 = 0; args02 < 128; ++args02) {
      relu_3[args3][args02] = ((dense_11[args3][args02] < 0.000000e+00f) ? 0.000000e+00f : dense_11[args3][args02]);
    }
  }
  float dense_2[1][64];
  for (ap_int<32> i9 = 0; i9 < 1; ++i9) {
    for (ap_int<32> j7 = 0; j7 < 64; ++j7) {
      float reducer35;
      reducer35 = 0.000000e+00f;
      for (ap_int<32> ra35 = 0; ra35 < 128; ++ra35) {
        reducer35 = ((relu_3[i9][ra35] * w_dense_2[ra35][j7]) + reducer35);
      }
      dense_2[i9][j7] = reducer35;
    }
  }
  float dense_21[1][64];
  for (ap_int<32> i10 = 0; i10 < 1; ++i10) {
    for (ap_int<32> j8 = 0; j8 < 64; ++j8) {
      dense_21[i10][j8] = (dense_2[i10][j8] + b_dense_2[j8]);
    }
  }
  float relu_4[1][64];
  for (ap_int<32> args4 = 0; args4 < 1; ++args4) {
    for (ap_int<32> args03 = 0; args03 < 64; ++args03) {
      relu_4[args4][args03] = ((dense_21[args4][args03] < 0.000000e+00f) ? 0.000000e+00f : dense_21[args4][args03]);
    }
  }
  float dense_3[1][32];
  for (ap_int<32> i11 = 0; i11 < 1; ++i11) {
    for (ap_int<32> j9 = 0; j9 < 32; ++j9) {
      float reducer36;
      reducer36 = 0.000000e+00f;
      for (ap_int<32> ra36 = 0; ra36 < 64; ++ra36) {
        reducer36 = ((relu_4[i11][ra36] * w_dense_3[ra36][j9]) + reducer36);
      }
      dense_3[i11][j9] = reducer36;
    }
  }
  float dense_31[1][32];
  for (ap_int<32> i12 = 0; i12 < 1; ++i12) {
    for (ap_int<32> j10 = 0; j10 < 32; ++j10) {
      dense_31[i12][j10] = (dense_3[i12][j10] + b_dense_3[j10]);
    }
  }
  float relu_5[1][32];
  for (ap_int<32> args5 = 0; args5 < 1; ++args5) {
    for (ap_int<32> args04 = 0; args04 < 32; ++args04) {
      relu_5[args5][args04] = ((dense_31[args5][args04] < 0.000000e+00f) ? 0.000000e+00f : dense_31[args5][args04]);
    }
  }
  float dense_4[1][10];
  for (ap_int<32> i13 = 0; i13 < 1; ++i13) {
    for (ap_int<32> j11 = 0; j11 < 10; ++j11) {
      float reducer37;
      reducer37 = 0.000000e+00f;
      for (ap_int<32> ra37 = 0; ra37 < 32; ++ra37) {
        reducer37 = ((relu_5[i13][ra37] * w_dense_4[ra37][j11]) + reducer37);
      }
      dense_4[i13][j11] = reducer37;
    }
  }
  float dense_41[1][10];
  for (ap_int<32> i14 = 0; i14 < 1; ++i14) {
    for (ap_int<32> j12 = 0; j12 < 10; ++j12) {
      dense_41[i14][j12] = (dense_4[i14][j12] + b_dense_4[j12]);
    }
  }
  float compute6;
  float reducer38;
  reducer38 = -1.000000e+00f;
  for (ap_int<32> ra38 = 0; ra38 < 10; ++ra38) {
    reducer38 = std::max(dense_41[0][ra38], reducer38);
  }
  compute6 = reducer38;
  float compute7;
  float reducer39;
  reducer39 = 0.000000e+00f;
  for (ap_int<32> ra39 = 0; ra39 < 10; ++ra39) {
    reducer39 = ((float)(exp(((double)(dense_41[0][ra39] - compute6))) + ((double)reducer39)));
  }
  compute7 = reducer39;
  for (ap_int<32> i15 = 0; i15 < 1; ++i15) {
    for (ap_int<32> j13 = 0; j13 < 10; ++j13) {
      softmax_1[i15][j13] = ((float)(exp(((double)(dense_41[i15][j13] - compute6))) / ((double)compute7)));
    }
  }
}