-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.2 (lin64) Build 1909853 Thu Jun 15 18:39:10 MDT 2017
-- Date        : Tue Sep 10 10:33:34 2019
-- Host        : ubuntu running 64-bit Ubuntu 16.04.6 LTS
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ Zynq_SDR_Inference_BD_SDR_inference_0_1_stub.vhdl
-- Design      : Zynq_SDR_Inference_BD_SDR_inference_0_1
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    s_axi_AXILiteS_AWADDR : in STD_LOGIC_VECTOR ( 11 downto 0 );
    s_axi_AXILiteS_AWVALID : in STD_LOGIC;
    s_axi_AXILiteS_AWREADY : out STD_LOGIC;
    s_axi_AXILiteS_WDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_AXILiteS_WSTRB : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_AXILiteS_WVALID : in STD_LOGIC;
    s_axi_AXILiteS_WREADY : out STD_LOGIC;
    s_axi_AXILiteS_BRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_AXILiteS_BVALID : out STD_LOGIC;
    s_axi_AXILiteS_BREADY : in STD_LOGIC;
    s_axi_AXILiteS_ARADDR : in STD_LOGIC_VECTOR ( 11 downto 0 );
    s_axi_AXILiteS_ARVALID : in STD_LOGIC;
    s_axi_AXILiteS_ARREADY : out STD_LOGIC;
    s_axi_AXILiteS_RDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_AXILiteS_RRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_AXILiteS_RVALID : out STD_LOGIC;
    s_axi_AXILiteS_RREADY : in STD_LOGIC;
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    ap_start : in STD_LOGIC;
    ap_done : out STD_LOGIC;
    ap_idle : out STD_LOGIC;
    ap_ready : out STD_LOGIC;
    m_axi_w_conv1d_2_AWADDR : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_w_conv1d_2_AWLEN : out STD_LOGIC_VECTOR ( 7 downto 0 );
    m_axi_w_conv1d_2_AWSIZE : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_w_conv1d_2_AWBURST : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_w_conv1d_2_AWLOCK : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_w_conv1d_2_AWREGION : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_w_conv1d_2_AWCACHE : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_w_conv1d_2_AWPROT : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_w_conv1d_2_AWQOS : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_w_conv1d_2_AWVALID : out STD_LOGIC;
    m_axi_w_conv1d_2_AWREADY : in STD_LOGIC;
    m_axi_w_conv1d_2_WDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_w_conv1d_2_WSTRB : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_w_conv1d_2_WLAST : out STD_LOGIC;
    m_axi_w_conv1d_2_WVALID : out STD_LOGIC;
    m_axi_w_conv1d_2_WREADY : in STD_LOGIC;
    m_axi_w_conv1d_2_BRESP : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_w_conv1d_2_BVALID : in STD_LOGIC;
    m_axi_w_conv1d_2_BREADY : out STD_LOGIC;
    m_axi_w_conv1d_2_ARADDR : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_w_conv1d_2_ARLEN : out STD_LOGIC_VECTOR ( 7 downto 0 );
    m_axi_w_conv1d_2_ARSIZE : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_w_conv1d_2_ARBURST : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_w_conv1d_2_ARLOCK : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_w_conv1d_2_ARREGION : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_w_conv1d_2_ARCACHE : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_w_conv1d_2_ARPROT : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_w_conv1d_2_ARQOS : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_w_conv1d_2_ARVALID : out STD_LOGIC;
    m_axi_w_conv1d_2_ARREADY : in STD_LOGIC;
    m_axi_w_conv1d_2_RDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_w_conv1d_2_RRESP : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_w_conv1d_2_RLAST : in STD_LOGIC;
    m_axi_w_conv1d_2_RVALID : in STD_LOGIC;
    m_axi_w_conv1d_2_RREADY : out STD_LOGIC;
    m_axi_w_dense_1_AWADDR : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_w_dense_1_AWLEN : out STD_LOGIC_VECTOR ( 7 downto 0 );
    m_axi_w_dense_1_AWSIZE : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_w_dense_1_AWBURST : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_w_dense_1_AWLOCK : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_w_dense_1_AWREGION : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_w_dense_1_AWCACHE : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_w_dense_1_AWPROT : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_w_dense_1_AWQOS : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_w_dense_1_AWVALID : out STD_LOGIC;
    m_axi_w_dense_1_AWREADY : in STD_LOGIC;
    m_axi_w_dense_1_WDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_w_dense_1_WSTRB : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_w_dense_1_WLAST : out STD_LOGIC;
    m_axi_w_dense_1_WVALID : out STD_LOGIC;
    m_axi_w_dense_1_WREADY : in STD_LOGIC;
    m_axi_w_dense_1_BRESP : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_w_dense_1_BVALID : in STD_LOGIC;
    m_axi_w_dense_1_BREADY : out STD_LOGIC;
    m_axi_w_dense_1_ARADDR : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_w_dense_1_ARLEN : out STD_LOGIC_VECTOR ( 7 downto 0 );
    m_axi_w_dense_1_ARSIZE : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_w_dense_1_ARBURST : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_w_dense_1_ARLOCK : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_w_dense_1_ARREGION : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_w_dense_1_ARCACHE : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_w_dense_1_ARPROT : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_w_dense_1_ARQOS : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_w_dense_1_ARVALID : out STD_LOGIC;
    m_axi_w_dense_1_ARREADY : in STD_LOGIC;
    m_axi_w_dense_1_RDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_w_dense_1_RRESP : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_w_dense_1_RLAST : in STD_LOGIC;
    m_axi_w_dense_1_RVALID : in STD_LOGIC;
    m_axi_w_dense_1_RREADY : out STD_LOGIC
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "s_axi_AXILiteS_AWADDR[11:0],s_axi_AXILiteS_AWVALID,s_axi_AXILiteS_AWREADY,s_axi_AXILiteS_WDATA[31:0],s_axi_AXILiteS_WSTRB[3:0],s_axi_AXILiteS_WVALID,s_axi_AXILiteS_WREADY,s_axi_AXILiteS_BRESP[1:0],s_axi_AXILiteS_BVALID,s_axi_AXILiteS_BREADY,s_axi_AXILiteS_ARADDR[11:0],s_axi_AXILiteS_ARVALID,s_axi_AXILiteS_ARREADY,s_axi_AXILiteS_RDATA[31:0],s_axi_AXILiteS_RRESP[1:0],s_axi_AXILiteS_RVALID,s_axi_AXILiteS_RREADY,ap_clk,ap_rst_n,ap_start,ap_done,ap_idle,ap_ready,m_axi_w_conv1d_2_AWADDR[31:0],m_axi_w_conv1d_2_AWLEN[7:0],m_axi_w_conv1d_2_AWSIZE[2:0],m_axi_w_conv1d_2_AWBURST[1:0],m_axi_w_conv1d_2_AWLOCK[1:0],m_axi_w_conv1d_2_AWREGION[3:0],m_axi_w_conv1d_2_AWCACHE[3:0],m_axi_w_conv1d_2_AWPROT[2:0],m_axi_w_conv1d_2_AWQOS[3:0],m_axi_w_conv1d_2_AWVALID,m_axi_w_conv1d_2_AWREADY,m_axi_w_conv1d_2_WDATA[31:0],m_axi_w_conv1d_2_WSTRB[3:0],m_axi_w_conv1d_2_WLAST,m_axi_w_conv1d_2_WVALID,m_axi_w_conv1d_2_WREADY,m_axi_w_conv1d_2_BRESP[1:0],m_axi_w_conv1d_2_BVALID,m_axi_w_conv1d_2_BREADY,m_axi_w_conv1d_2_ARADDR[31:0],m_axi_w_conv1d_2_ARLEN[7:0],m_axi_w_conv1d_2_ARSIZE[2:0],m_axi_w_conv1d_2_ARBURST[1:0],m_axi_w_conv1d_2_ARLOCK[1:0],m_axi_w_conv1d_2_ARREGION[3:0],m_axi_w_conv1d_2_ARCACHE[3:0],m_axi_w_conv1d_2_ARPROT[2:0],m_axi_w_conv1d_2_ARQOS[3:0],m_axi_w_conv1d_2_ARVALID,m_axi_w_conv1d_2_ARREADY,m_axi_w_conv1d_2_RDATA[31:0],m_axi_w_conv1d_2_RRESP[1:0],m_axi_w_conv1d_2_RLAST,m_axi_w_conv1d_2_RVALID,m_axi_w_conv1d_2_RREADY,m_axi_w_dense_1_AWADDR[31:0],m_axi_w_dense_1_AWLEN[7:0],m_axi_w_dense_1_AWSIZE[2:0],m_axi_w_dense_1_AWBURST[1:0],m_axi_w_dense_1_AWLOCK[1:0],m_axi_w_dense_1_AWREGION[3:0],m_axi_w_dense_1_AWCACHE[3:0],m_axi_w_dense_1_AWPROT[2:0],m_axi_w_dense_1_AWQOS[3:0],m_axi_w_dense_1_AWVALID,m_axi_w_dense_1_AWREADY,m_axi_w_dense_1_WDATA[31:0],m_axi_w_dense_1_WSTRB[3:0],m_axi_w_dense_1_WLAST,m_axi_w_dense_1_WVALID,m_axi_w_dense_1_WREADY,m_axi_w_dense_1_BRESP[1:0],m_axi_w_dense_1_BVALID,m_axi_w_dense_1_BREADY,m_axi_w_dense_1_ARADDR[31:0],m_axi_w_dense_1_ARLEN[7:0],m_axi_w_dense_1_ARSIZE[2:0],m_axi_w_dense_1_ARBURST[1:0],m_axi_w_dense_1_ARLOCK[1:0],m_axi_w_dense_1_ARREGION[3:0],m_axi_w_dense_1_ARCACHE[3:0],m_axi_w_dense_1_ARPROT[2:0],m_axi_w_dense_1_ARQOS[3:0],m_axi_w_dense_1_ARVALID,m_axi_w_dense_1_ARREADY,m_axi_w_dense_1_RDATA[31:0],m_axi_w_dense_1_RRESP[1:0],m_axi_w_dense_1_RLAST,m_axi_w_dense_1_RVALID,m_axi_w_dense_1_RREADY";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "SDR_inference,Vivado 2017.2";
begin
end;
