-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.2 (lin64) Build 1909853 Thu Jun 15 18:39:10 MDT 2017
-- Date        : Thu Sep  5 16:19:17 2019
-- Host        : ubuntu running 64-bit Ubuntu 16.04.6 LTS
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ Zynq_SDR_Inference_BD_SDR_inference_0_0_stub.vhdl
-- Design      : Zynq_SDR_Inference_BD_SDR_inference_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    w_conv1d_1_ce0 : out STD_LOGIC;
    b_conv1d_1_ce0 : out STD_LOGIC;
    w_conv1d_2_ce0 : out STD_LOGIC;
    b_conv1d_2_ce0 : out STD_LOGIC;
    w_dense_1_ce0 : out STD_LOGIC;
    b_dense_1_ce0 : out STD_LOGIC;
    w_dense_2_ce0 : out STD_LOGIC;
    b_dense_2_ce0 : out STD_LOGIC;
    w_dense_3_ce0 : out STD_LOGIC;
    b_dense_3_ce0 : out STD_LOGIC;
    w_dense_4_ce0 : out STD_LOGIC;
    b_dense_4_ce0 : out STD_LOGIC;
    s_axi_AXILiteS_AWADDR : in STD_LOGIC_VECTOR ( 11 downto 0 );
    s_axi_AXILiteS_AWVALID : in STD_LOGIC;
    s_axi_AXILiteS_AWREADY : out STD_LOGIC;
    s_axi_AXILiteS_WDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_AXILiteS_WSTRB : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_AXILiteS_WVALID : in STD_LOGIC;
    s_axi_AXILiteS_WREADY : out STD_LOGIC;
    s_axi_AXILiteS_BRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_AXILiteS_BVALID : out STD_LOGIC;
    s_axi_AXILiteS_BREADY : in STD_LOGIC;
    s_axi_AXILiteS_ARADDR : in STD_LOGIC_VECTOR ( 11 downto 0 );
    s_axi_AXILiteS_ARVALID : in STD_LOGIC;
    s_axi_AXILiteS_ARREADY : out STD_LOGIC;
    s_axi_AXILiteS_RDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_AXILiteS_RRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_AXILiteS_RVALID : out STD_LOGIC;
    s_axi_AXILiteS_RREADY : in STD_LOGIC;
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    ap_start : in STD_LOGIC;
    ap_done : out STD_LOGIC;
    ap_idle : out STD_LOGIC;
    ap_ready : out STD_LOGIC;
    w_conv1d_1_address0 : out STD_LOGIC_VECTOR ( 10 downto 0 );
    w_conv1d_1_q0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    b_conv1d_1_address0 : out STD_LOGIC_VECTOR ( 6 downto 0 );
    b_conv1d_1_q0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    w_conv1d_2_address0 : out STD_LOGIC_VECTOR ( 16 downto 0 );
    w_conv1d_2_q0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    b_conv1d_2_address0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
    b_conv1d_2_q0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    w_dense_1_address0 : out STD_LOGIC_VECTOR ( 17 downto 0 );
    w_dense_1_q0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    b_dense_1_address0 : out STD_LOGIC_VECTOR ( 6 downto 0 );
    b_dense_1_q0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    w_dense_2_address0 : out STD_LOGIC_VECTOR ( 12 downto 0 );
    w_dense_2_q0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    b_dense_2_address0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
    b_dense_2_q0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    w_dense_3_address0 : out STD_LOGIC_VECTOR ( 10 downto 0 );
    w_dense_3_q0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    b_dense_3_address0 : out STD_LOGIC_VECTOR ( 4 downto 0 );
    b_dense_3_q0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    w_dense_4_address0 : out STD_LOGIC_VECTOR ( 8 downto 0 );
    w_dense_4_q0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    b_dense_4_address0 : out STD_LOGIC_VECTOR ( 3 downto 0 );
    b_dense_4_q0 : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "w_conv1d_1_ce0,b_conv1d_1_ce0,w_conv1d_2_ce0,b_conv1d_2_ce0,w_dense_1_ce0,b_dense_1_ce0,w_dense_2_ce0,b_dense_2_ce0,w_dense_3_ce0,b_dense_3_ce0,w_dense_4_ce0,b_dense_4_ce0,s_axi_AXILiteS_AWADDR[11:0],s_axi_AXILiteS_AWVALID,s_axi_AXILiteS_AWREADY,s_axi_AXILiteS_WDATA[31:0],s_axi_AXILiteS_WSTRB[3:0],s_axi_AXILiteS_WVALID,s_axi_AXILiteS_WREADY,s_axi_AXILiteS_BRESP[1:0],s_axi_AXILiteS_BVALID,s_axi_AXILiteS_BREADY,s_axi_AXILiteS_ARADDR[11:0],s_axi_AXILiteS_ARVALID,s_axi_AXILiteS_ARREADY,s_axi_AXILiteS_RDATA[31:0],s_axi_AXILiteS_RRESP[1:0],s_axi_AXILiteS_RVALID,s_axi_AXILiteS_RREADY,ap_clk,ap_rst_n,ap_start,ap_done,ap_idle,ap_ready,w_conv1d_1_address0[10:0],w_conv1d_1_q0[31:0],b_conv1d_1_address0[6:0],b_conv1d_1_q0[31:0],w_conv1d_2_address0[16:0],w_conv1d_2_q0[31:0],b_conv1d_2_address0[5:0],b_conv1d_2_q0[31:0],w_dense_1_address0[17:0],w_dense_1_q0[31:0],b_dense_1_address0[6:0],b_dense_1_q0[31:0],w_dense_2_address0[12:0],w_dense_2_q0[31:0],b_dense_2_address0[5:0],b_dense_2_q0[31:0],w_dense_3_address0[10:0],w_dense_3_q0[31:0],b_dense_3_address0[4:0],b_dense_3_q0[31:0],w_dense_4_address0[8:0],w_dense_4_q0[31:0],b_dense_4_address0[3:0],b_dense_4_q0[31:0]";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "SDR_inference,Vivado 2017.2";
begin
end;
