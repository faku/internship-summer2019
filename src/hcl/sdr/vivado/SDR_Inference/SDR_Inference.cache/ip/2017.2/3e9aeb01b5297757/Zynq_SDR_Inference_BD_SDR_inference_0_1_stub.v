// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.2 (lin64) Build 1909853 Thu Jun 15 18:39:10 MDT 2017
// Date        : Tue Sep 10 10:33:33 2019
// Host        : ubuntu running 64-bit Ubuntu 16.04.6 LTS
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ Zynq_SDR_Inference_BD_SDR_inference_0_1_stub.v
// Design      : Zynq_SDR_Inference_BD_SDR_inference_0_1
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "SDR_inference,Vivado 2017.2" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(s_axi_AXILiteS_AWADDR, 
  s_axi_AXILiteS_AWVALID, s_axi_AXILiteS_AWREADY, s_axi_AXILiteS_WDATA, 
  s_axi_AXILiteS_WSTRB, s_axi_AXILiteS_WVALID, s_axi_AXILiteS_WREADY, 
  s_axi_AXILiteS_BRESP, s_axi_AXILiteS_BVALID, s_axi_AXILiteS_BREADY, 
  s_axi_AXILiteS_ARADDR, s_axi_AXILiteS_ARVALID, s_axi_AXILiteS_ARREADY, 
  s_axi_AXILiteS_RDATA, s_axi_AXILiteS_RRESP, s_axi_AXILiteS_RVALID, 
  s_axi_AXILiteS_RREADY, ap_clk, ap_rst_n, ap_start, ap_done, ap_idle, ap_ready, 
  m_axi_w_conv1d_2_AWADDR, m_axi_w_conv1d_2_AWLEN, m_axi_w_conv1d_2_AWSIZE, 
  m_axi_w_conv1d_2_AWBURST, m_axi_w_conv1d_2_AWLOCK, m_axi_w_conv1d_2_AWREGION, 
  m_axi_w_conv1d_2_AWCACHE, m_axi_w_conv1d_2_AWPROT, m_axi_w_conv1d_2_AWQOS, 
  m_axi_w_conv1d_2_AWVALID, m_axi_w_conv1d_2_AWREADY, m_axi_w_conv1d_2_WDATA, 
  m_axi_w_conv1d_2_WSTRB, m_axi_w_conv1d_2_WLAST, m_axi_w_conv1d_2_WVALID, 
  m_axi_w_conv1d_2_WREADY, m_axi_w_conv1d_2_BRESP, m_axi_w_conv1d_2_BVALID, 
  m_axi_w_conv1d_2_BREADY, m_axi_w_conv1d_2_ARADDR, m_axi_w_conv1d_2_ARLEN, 
  m_axi_w_conv1d_2_ARSIZE, m_axi_w_conv1d_2_ARBURST, m_axi_w_conv1d_2_ARLOCK, 
  m_axi_w_conv1d_2_ARREGION, m_axi_w_conv1d_2_ARCACHE, m_axi_w_conv1d_2_ARPROT, 
  m_axi_w_conv1d_2_ARQOS, m_axi_w_conv1d_2_ARVALID, m_axi_w_conv1d_2_ARREADY, 
  m_axi_w_conv1d_2_RDATA, m_axi_w_conv1d_2_RRESP, m_axi_w_conv1d_2_RLAST, 
  m_axi_w_conv1d_2_RVALID, m_axi_w_conv1d_2_RREADY, m_axi_w_dense_1_AWADDR, 
  m_axi_w_dense_1_AWLEN, m_axi_w_dense_1_AWSIZE, m_axi_w_dense_1_AWBURST, 
  m_axi_w_dense_1_AWLOCK, m_axi_w_dense_1_AWREGION, m_axi_w_dense_1_AWCACHE, 
  m_axi_w_dense_1_AWPROT, m_axi_w_dense_1_AWQOS, m_axi_w_dense_1_AWVALID, 
  m_axi_w_dense_1_AWREADY, m_axi_w_dense_1_WDATA, m_axi_w_dense_1_WSTRB, 
  m_axi_w_dense_1_WLAST, m_axi_w_dense_1_WVALID, m_axi_w_dense_1_WREADY, 
  m_axi_w_dense_1_BRESP, m_axi_w_dense_1_BVALID, m_axi_w_dense_1_BREADY, 
  m_axi_w_dense_1_ARADDR, m_axi_w_dense_1_ARLEN, m_axi_w_dense_1_ARSIZE, 
  m_axi_w_dense_1_ARBURST, m_axi_w_dense_1_ARLOCK, m_axi_w_dense_1_ARREGION, 
  m_axi_w_dense_1_ARCACHE, m_axi_w_dense_1_ARPROT, m_axi_w_dense_1_ARQOS, 
  m_axi_w_dense_1_ARVALID, m_axi_w_dense_1_ARREADY, m_axi_w_dense_1_RDATA, 
  m_axi_w_dense_1_RRESP, m_axi_w_dense_1_RLAST, m_axi_w_dense_1_RVALID, 
  m_axi_w_dense_1_RREADY)
/* synthesis syn_black_box black_box_pad_pin="s_axi_AXILiteS_AWADDR[11:0],s_axi_AXILiteS_AWVALID,s_axi_AXILiteS_AWREADY,s_axi_AXILiteS_WDATA[31:0],s_axi_AXILiteS_WSTRB[3:0],s_axi_AXILiteS_WVALID,s_axi_AXILiteS_WREADY,s_axi_AXILiteS_BRESP[1:0],s_axi_AXILiteS_BVALID,s_axi_AXILiteS_BREADY,s_axi_AXILiteS_ARADDR[11:0],s_axi_AXILiteS_ARVALID,s_axi_AXILiteS_ARREADY,s_axi_AXILiteS_RDATA[31:0],s_axi_AXILiteS_RRESP[1:0],s_axi_AXILiteS_RVALID,s_axi_AXILiteS_RREADY,ap_clk,ap_rst_n,ap_start,ap_done,ap_idle,ap_ready,m_axi_w_conv1d_2_AWADDR[31:0],m_axi_w_conv1d_2_AWLEN[7:0],m_axi_w_conv1d_2_AWSIZE[2:0],m_axi_w_conv1d_2_AWBURST[1:0],m_axi_w_conv1d_2_AWLOCK[1:0],m_axi_w_conv1d_2_AWREGION[3:0],m_axi_w_conv1d_2_AWCACHE[3:0],m_axi_w_conv1d_2_AWPROT[2:0],m_axi_w_conv1d_2_AWQOS[3:0],m_axi_w_conv1d_2_AWVALID,m_axi_w_conv1d_2_AWREADY,m_axi_w_conv1d_2_WDATA[31:0],m_axi_w_conv1d_2_WSTRB[3:0],m_axi_w_conv1d_2_WLAST,m_axi_w_conv1d_2_WVALID,m_axi_w_conv1d_2_WREADY,m_axi_w_conv1d_2_BRESP[1:0],m_axi_w_conv1d_2_BVALID,m_axi_w_conv1d_2_BREADY,m_axi_w_conv1d_2_ARADDR[31:0],m_axi_w_conv1d_2_ARLEN[7:0],m_axi_w_conv1d_2_ARSIZE[2:0],m_axi_w_conv1d_2_ARBURST[1:0],m_axi_w_conv1d_2_ARLOCK[1:0],m_axi_w_conv1d_2_ARREGION[3:0],m_axi_w_conv1d_2_ARCACHE[3:0],m_axi_w_conv1d_2_ARPROT[2:0],m_axi_w_conv1d_2_ARQOS[3:0],m_axi_w_conv1d_2_ARVALID,m_axi_w_conv1d_2_ARREADY,m_axi_w_conv1d_2_RDATA[31:0],m_axi_w_conv1d_2_RRESP[1:0],m_axi_w_conv1d_2_RLAST,m_axi_w_conv1d_2_RVALID,m_axi_w_conv1d_2_RREADY,m_axi_w_dense_1_AWADDR[31:0],m_axi_w_dense_1_AWLEN[7:0],m_axi_w_dense_1_AWSIZE[2:0],m_axi_w_dense_1_AWBURST[1:0],m_axi_w_dense_1_AWLOCK[1:0],m_axi_w_dense_1_AWREGION[3:0],m_axi_w_dense_1_AWCACHE[3:0],m_axi_w_dense_1_AWPROT[2:0],m_axi_w_dense_1_AWQOS[3:0],m_axi_w_dense_1_AWVALID,m_axi_w_dense_1_AWREADY,m_axi_w_dense_1_WDATA[31:0],m_axi_w_dense_1_WSTRB[3:0],m_axi_w_dense_1_WLAST,m_axi_w_dense_1_WVALID,m_axi_w_dense_1_WREADY,m_axi_w_dense_1_BRESP[1:0],m_axi_w_dense_1_BVALID,m_axi_w_dense_1_BREADY,m_axi_w_dense_1_ARADDR[31:0],m_axi_w_dense_1_ARLEN[7:0],m_axi_w_dense_1_ARSIZE[2:0],m_axi_w_dense_1_ARBURST[1:0],m_axi_w_dense_1_ARLOCK[1:0],m_axi_w_dense_1_ARREGION[3:0],m_axi_w_dense_1_ARCACHE[3:0],m_axi_w_dense_1_ARPROT[2:0],m_axi_w_dense_1_ARQOS[3:0],m_axi_w_dense_1_ARVALID,m_axi_w_dense_1_ARREADY,m_axi_w_dense_1_RDATA[31:0],m_axi_w_dense_1_RRESP[1:0],m_axi_w_dense_1_RLAST,m_axi_w_dense_1_RVALID,m_axi_w_dense_1_RREADY" */;
  input [11:0]s_axi_AXILiteS_AWADDR;
  input s_axi_AXILiteS_AWVALID;
  output s_axi_AXILiteS_AWREADY;
  input [31:0]s_axi_AXILiteS_WDATA;
  input [3:0]s_axi_AXILiteS_WSTRB;
  input s_axi_AXILiteS_WVALID;
  output s_axi_AXILiteS_WREADY;
  output [1:0]s_axi_AXILiteS_BRESP;
  output s_axi_AXILiteS_BVALID;
  input s_axi_AXILiteS_BREADY;
  input [11:0]s_axi_AXILiteS_ARADDR;
  input s_axi_AXILiteS_ARVALID;
  output s_axi_AXILiteS_ARREADY;
  output [31:0]s_axi_AXILiteS_RDATA;
  output [1:0]s_axi_AXILiteS_RRESP;
  output s_axi_AXILiteS_RVALID;
  input s_axi_AXILiteS_RREADY;
  input ap_clk;
  input ap_rst_n;
  input ap_start;
  output ap_done;
  output ap_idle;
  output ap_ready;
  output [31:0]m_axi_w_conv1d_2_AWADDR;
  output [7:0]m_axi_w_conv1d_2_AWLEN;
  output [2:0]m_axi_w_conv1d_2_AWSIZE;
  output [1:0]m_axi_w_conv1d_2_AWBURST;
  output [1:0]m_axi_w_conv1d_2_AWLOCK;
  output [3:0]m_axi_w_conv1d_2_AWREGION;
  output [3:0]m_axi_w_conv1d_2_AWCACHE;
  output [2:0]m_axi_w_conv1d_2_AWPROT;
  output [3:0]m_axi_w_conv1d_2_AWQOS;
  output m_axi_w_conv1d_2_AWVALID;
  input m_axi_w_conv1d_2_AWREADY;
  output [31:0]m_axi_w_conv1d_2_WDATA;
  output [3:0]m_axi_w_conv1d_2_WSTRB;
  output m_axi_w_conv1d_2_WLAST;
  output m_axi_w_conv1d_2_WVALID;
  input m_axi_w_conv1d_2_WREADY;
  input [1:0]m_axi_w_conv1d_2_BRESP;
  input m_axi_w_conv1d_2_BVALID;
  output m_axi_w_conv1d_2_BREADY;
  output [31:0]m_axi_w_conv1d_2_ARADDR;
  output [7:0]m_axi_w_conv1d_2_ARLEN;
  output [2:0]m_axi_w_conv1d_2_ARSIZE;
  output [1:0]m_axi_w_conv1d_2_ARBURST;
  output [1:0]m_axi_w_conv1d_2_ARLOCK;
  output [3:0]m_axi_w_conv1d_2_ARREGION;
  output [3:0]m_axi_w_conv1d_2_ARCACHE;
  output [2:0]m_axi_w_conv1d_2_ARPROT;
  output [3:0]m_axi_w_conv1d_2_ARQOS;
  output m_axi_w_conv1d_2_ARVALID;
  input m_axi_w_conv1d_2_ARREADY;
  input [31:0]m_axi_w_conv1d_2_RDATA;
  input [1:0]m_axi_w_conv1d_2_RRESP;
  input m_axi_w_conv1d_2_RLAST;
  input m_axi_w_conv1d_2_RVALID;
  output m_axi_w_conv1d_2_RREADY;
  output [31:0]m_axi_w_dense_1_AWADDR;
  output [7:0]m_axi_w_dense_1_AWLEN;
  output [2:0]m_axi_w_dense_1_AWSIZE;
  output [1:0]m_axi_w_dense_1_AWBURST;
  output [1:0]m_axi_w_dense_1_AWLOCK;
  output [3:0]m_axi_w_dense_1_AWREGION;
  output [3:0]m_axi_w_dense_1_AWCACHE;
  output [2:0]m_axi_w_dense_1_AWPROT;
  output [3:0]m_axi_w_dense_1_AWQOS;
  output m_axi_w_dense_1_AWVALID;
  input m_axi_w_dense_1_AWREADY;
  output [31:0]m_axi_w_dense_1_WDATA;
  output [3:0]m_axi_w_dense_1_WSTRB;
  output m_axi_w_dense_1_WLAST;
  output m_axi_w_dense_1_WVALID;
  input m_axi_w_dense_1_WREADY;
  input [1:0]m_axi_w_dense_1_BRESP;
  input m_axi_w_dense_1_BVALID;
  output m_axi_w_dense_1_BREADY;
  output [31:0]m_axi_w_dense_1_ARADDR;
  output [7:0]m_axi_w_dense_1_ARLEN;
  output [2:0]m_axi_w_dense_1_ARSIZE;
  output [1:0]m_axi_w_dense_1_ARBURST;
  output [1:0]m_axi_w_dense_1_ARLOCK;
  output [3:0]m_axi_w_dense_1_ARREGION;
  output [3:0]m_axi_w_dense_1_ARCACHE;
  output [2:0]m_axi_w_dense_1_ARPROT;
  output [3:0]m_axi_w_dense_1_ARQOS;
  output m_axi_w_dense_1_ARVALID;
  input m_axi_w_dense_1_ARREADY;
  input [31:0]m_axi_w_dense_1_RDATA;
  input [1:0]m_axi_w_dense_1_RRESP;
  input m_axi_w_dense_1_RLAST;
  input m_axi_w_dense_1_RVALID;
  output m_axi_w_dense_1_RREADY;
endmodule
