onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib Zynq_SDR_Inference_BD_opt

do {wave.do}

view wave
view structure
view signals

do {Zynq_SDR_Inference_BD.udo}

run -all

quit -force
