-makelib ies/xil_defaultlib -sv \
  "/home/faku/opt/Xilinx/Vivado/2017.2/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
  "/home/faku/opt/Xilinx/Vivado/2017.2/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \
-endlib
-makelib ies/xpm \
  "/home/faku/opt/Xilinx/Vivado/2017.2/data/ip/xpm/xpm_VCOMP.vhd" \
-endlib
-makelib ies/axi_infrastructure_v1_1_0 \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/7e3a/hdl/axi_infrastructure_v1_1_vl_rfs.v" \
-endlib
-makelib ies/xil_common_vip_v1_0_0 -sv \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/2ad9/hdl/xil_common_vip_v1_0_vl_rfs.sv" \
-endlib
-makelib ies/smartconnect_v1_0 -sv \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/39ca/hdl/sc_util_v1_0_vl_rfs.sv" \
-endlib
-makelib ies/axi_protocol_checker_v1_1_14 -sv \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/a1b2/hdl/axi_protocol_checker_v1_1_vl_rfs.sv" \
-endlib
-makelib ies/axi_vip_v1_0_2 -sv \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/6eb1/hdl/axi_vip_v1_0_vl_rfs.sv" \
-endlib
-makelib ies/axi_vip_v1_0_1 -sv \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/1d61/hdl/processing_system7_vip_v1_0_vl_rfs.sv" \
-endlib
-makelib ies/xil_defaultlib \
  "../../../bd/Zynq_SDR_Inference_BD/ip/Zynq_SDR_Inference_BD_processing_system7_0_0/sim/Zynq_SDR_Inference_BD_processing_system7_0_0.v" \
-endlib
-makelib ies/lib_cdc_v1_0_2 \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/52cb/hdl/lib_cdc_v1_0_rfs.vhd" \
-endlib
-makelib ies/proc_sys_reset_v5_0_11 \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/5db7/hdl/proc_sys_reset_v5_0_vh_rfs.vhd" \
-endlib
-makelib ies/xil_defaultlib \
  "../../../bd/Zynq_SDR_Inference_BD/ip/Zynq_SDR_Inference_BD_rst_ps7_0_50M_0/sim/Zynq_SDR_Inference_BD_rst_ps7_0_50M_0.vhd" \
-endlib
-makelib ies/xlconstant_v1_1_3 \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/45df/hdl/xlconstant_v1_1_vl_rfs.v" \
-endlib
-makelib ies/xil_defaultlib \
  "../../../bd/Zynq_SDR_Inference_BD/ip/Zynq_SDR_Inference_BD_xlconstant_0_0/sim/Zynq_SDR_Inference_BD_xlconstant_0_0.v" \
  "../../../bd/Zynq_SDR_Inference_BD/hdl/Zynq_SDR_Inference_BD.v" \
-endlib
-makelib ies/xbip_utils_v3_0_7 \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/3d01/hdl/xbip_utils_v3_0_vh_rfs.vhd" \
-endlib
-makelib ies/axi_utils_v2_0_3 \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/43f5/hdl/axi_utils_v2_0_vh_rfs.vhd" \
-endlib
-makelib ies/xbip_pipe_v3_0_3 \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/7db8/hdl/xbip_pipe_v3_0_vh_rfs.vhd" \
-endlib
-makelib ies/xbip_dsp48_wrapper_v3_0_4 \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/1e87/hdl/xbip_dsp48_wrapper_v3_0_vh_rfs.vhd" \
-endlib
-makelib ies/xbip_dsp48_addsub_v3_0_3 \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/c9c4/hdl/xbip_dsp48_addsub_v3_0_vh_rfs.vhd" \
-endlib
-makelib ies/xbip_dsp48_multadd_v3_0_3 \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/779d/hdl/xbip_dsp48_multadd_v3_0_vh_rfs.vhd" \
-endlib
-makelib ies/xbip_bram18k_v3_0_3 \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/403d/hdl/xbip_bram18k_v3_0_vh_rfs.vhd" \
-endlib
-makelib ies/mult_gen_v12_0_12 \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/f0ab/hdl/mult_gen_v12_0_vh_rfs.vhd" \
-endlib
-makelib ies/floating_point_v7_1_4 \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/4a02/hdl/floating_point_v7_1_vh_rfs.vhd" \
-endlib
-makelib ies/xil_defaultlib \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/verilog/SDR_inference_w_dense_1_m_axi.v" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/verilog/SDR_inference_b_cdEe.v" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/verilog/SDR_inference_fadJfO.v" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/verilog/SDR_inference_b_dkbM.v" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/verilog/SDR_inference.v" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/verilog/SDR_inference_denHfu.v" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/verilog/SDR_inference_denEe0.v" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/verilog/SDR_inference_b_dg8j.v" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/verilog/SDR_inference_consc4.v" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/verilog/SDR_inference_w_conv1d_2_m_axi.v" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/verilog/SDR_inference_b_deOg.v" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/verilog/SDR_inference_denyd2.v" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/verilog/SDR_inference_fpeMgi.v" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/verilog/SDR_inference_fcmNgs.v" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/verilog/SDR_inference_fmuKfY.v" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/verilog/SDR_inference_denBew.v" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/verilog/SDR_inference_dexQgW.v" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/verilog/SDR_inference_AXILiteS_s_axi.v" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/verilog/SDR_inference_maxvdy.v" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/verilog/SDR_inference_ddiPgM.v" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/verilog/SDR_inference_w_dfYi.v" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/verilog/SDR_inference_b_dibs.v" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/verilog/SDR_inference_b_ccud.v" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/verilog/SDR_inference_fptLf8.v" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/verilog/SDR_inference_w_djbC.v" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/verilog/SDR_inference_tralbW.v" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/verilog/SDR_inference_maxqcK.v" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/verilog/SDR_inference_w_dhbi.v" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/verilog/SDR_inference_conncg.v" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/verilog/SDR_inference_dadOgC.v" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/verilog/SDR_inference_w_cbkb.v" \
-endlib
-makelib ies/xil_defaultlib \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/ip/SDR_inference_ap_fcmp_0_no_dsp_32.vhd" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/ip/SDR_inference_ap_fmul_2_max_dsp_32.vhd" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/ip/SDR_inference_ap_fptrunc_0_no_dsp_64.vhd" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/ip/SDR_inference_ap_fpext_0_no_dsp_32.vhd" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/ip/SDR_inference_ap_faddfsub_3_full_dsp_32.vhd" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/ip/SDR_inference_ap_ddiv_29_no_dsp_64.vhd" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/ip/SDR_inference_ap_dexp_16_full_dsp_64.vhd" \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/21a5/hdl/ip/SDR_inference_ap_dadd_3_full_dsp_64.vhd" \
  "../../../bd/Zynq_SDR_Inference_BD/ip/Zynq_SDR_Inference_BD_SDR_inference_0_1/sim/Zynq_SDR_Inference_BD_SDR_inference_0_1.vhd" \
-endlib
-makelib ies/blk_mem_gen_v8_3_6 \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/4158/simulation/blk_mem_gen_v8_3.v" \
-endlib
-makelib ies/xil_defaultlib \
  "../../../bd/Zynq_SDR_Inference_BD/ip/Zynq_SDR_Inference_BD_blk_mem_gen_1_0/sim/Zynq_SDR_Inference_BD_blk_mem_gen_1_0.v" \
-endlib
-makelib ies/generic_baseblocks_v2_1_0 \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/f9c1/hdl/generic_baseblocks_v2_1_vl_rfs.v" \
-endlib
-makelib ies/axi_register_slice_v2_1_13 \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/55c0/hdl/axi_register_slice_v2_1_vl_rfs.v" \
-endlib
-makelib ies/fifo_generator_v13_1_4 \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/ebc2/simulation/fifo_generator_vlog_beh.v" \
-endlib
-makelib ies/fifo_generator_v13_1_4 \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/ebc2/hdl/fifo_generator_v13_1_rfs.vhd" \
-endlib
-makelib ies/fifo_generator_v13_1_4 \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/ebc2/hdl/fifo_generator_v13_1_rfs.v" \
-endlib
-makelib ies/axi_data_fifo_v2_1_12 \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/95b9/hdl/axi_data_fifo_v2_1_vl_rfs.v" \
-endlib
-makelib ies/axi_crossbar_v2_1_14 \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/f582/hdl/axi_crossbar_v2_1_vl_rfs.v" \
-endlib
-makelib ies/xil_defaultlib \
  "../../../bd/Zynq_SDR_Inference_BD/ip/Zynq_SDR_Inference_BD_xbar_0/sim/Zynq_SDR_Inference_BD_xbar_0.v" \
-endlib
-makelib ies/axi_protocol_converter_v2_1_13 \
  "../../../../SDR_Inference.srcs/sources_1/bd/Zynq_SDR_Inference_BD/ipshared/f0ae/hdl/axi_protocol_converter_v2_1_vl_rfs.v" \
-endlib
-makelib ies/xil_defaultlib \
  "../../../bd/Zynq_SDR_Inference_BD/ip/Zynq_SDR_Inference_BD_auto_pc_0/sim/Zynq_SDR_Inference_BD_auto_pc_0.v" \
  "../../../bd/Zynq_SDR_Inference_BD/ip/Zynq_SDR_Inference_BD_auto_pc_1/sim/Zynq_SDR_Inference_BD_auto_pc_1.v" \
-endlib
-makelib ies/xil_defaultlib \
  glbl.v
-endlib

