; ModuleID = '/home/faku/internship-summer2019/src/hcl/sdr/vivado/SDR_HLS/no_weights/.autopilot/db/a.o.2.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@llvm_global_ctors_1 = appending global [1 x void ()*] [void ()* @_GLOBAL__I_a]
@llvm_global_ctors_0 = appending global [1 x i32] [i32 65535]
@SDR_inference_str = internal unnamed_addr constant [14 x i8] c"SDR_inference\00"
@RAM_1P_str = internal unnamed_addr constant [7 x i8] c"RAM_1P\00"
@p_str2 = private unnamed_addr constant [10 x i8] c"s_axilite\00", align 1
@p_str1 = private unnamed_addr constant [12 x i8] c"ROM_1P_BRAM\00", align 1
@p_str = private unnamed_addr constant [1 x i8] zeroinitializer, align 1

declare i32 @llvm.part.select.i32(i32, i32, i32) nounwind readnone

declare double @llvm.exp.f64(double) nounwind readonly

declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

declare void @llvm.dbg.declare(metadata, metadata) nounwind readnone

define weak void @_ssdm_op_SpecTopModule(...) {
entry:
  ret void
}

define weak void @_ssdm_op_SpecMemCore(...) {
entry:
  ret void
}

define weak i32 @_ssdm_op_SpecLoopTripCount(...) {
entry:
  ret i32 0
}

define weak void @_ssdm_op_SpecInterface(...) nounwind {
entry:
  ret void
}

define weak void @_ssdm_op_SpecBitsMap(...) {
entry:
  ret void
}

define weak i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32, i32, i32) nounwind readnone {
entry:
  %empty = call i32 @llvm.part.select.i32(i32 %0, i32 %1, i32 %2)
  %empty_5 = trunc i32 %empty to i8
  ret i8 %empty_5
}

declare i23 @_ssdm_op_PartSelect.i23.i32.i32.i32(i32, i32, i32) nounwind readnone

define weak i9 @_ssdm_op_BitConcatenate.i9.i8.i1(i8, i1) nounwind readnone {
entry:
  %empty = zext i8 %0 to i9
  %empty_6 = zext i1 %1 to i9
  %empty_7 = shl i9 %empty, 1
  %empty_8 = or i9 %empty_7, %empty_6
  ret i9 %empty_8
}

define weak i9 @_ssdm_op_BitConcatenate.i9.i6.i3(i6, i3) nounwind readnone {
entry:
  %empty = zext i6 %0 to i9
  %empty_9 = zext i3 %1 to i9
  %empty_10 = shl i9 %empty, 3
  %empty_11 = or i9 %empty_10, %empty_9
  ret i9 %empty_11
}

define weak i9 @_ssdm_op_BitConcatenate.i9.i2.i7(i2, i7) nounwind readnone {
entry:
  %empty = zext i2 %0 to i9
  %empty_12 = zext i7 %1 to i9
  %empty_13 = shl i9 %empty, 7
  %empty_14 = or i9 %empty_13, %empty_12
  ret i9 %empty_14
}

define weak i7 @_ssdm_op_BitConcatenate.i7.i6.i1(i6, i1) nounwind readnone {
entry:
  %empty = zext i6 %0 to i7
  %empty_15 = zext i1 %1 to i7
  %empty_16 = shl i7 %empty, 1
  %empty_17 = or i7 %empty_16, %empty_15
  ret i7 %empty_17
}

define weak i6 @_ssdm_op_BitConcatenate.i6.i5.i1(i5, i1) nounwind readnone {
entry:
  %empty = zext i5 %0 to i6
  %empty_18 = zext i1 %1 to i6
  %empty_19 = shl i6 %empty, 1
  %empty_20 = or i6 %empty_19, %empty_18
  ret i6 %empty_20
}

define weak i19 @_ssdm_op_BitConcatenate.i19.i15.i4(i15, i4) nounwind readnone {
entry:
  %empty = zext i15 %0 to i19
  %empty_21 = zext i4 %1 to i19
  %empty_22 = shl i19 %empty, 4
  %empty_23 = or i19 %empty_22, %empty_21
  ret i19 %empty_23
}

define weak i18 @_ssdm_op_BitConcatenate.i18.i11.i7(i11, i7) nounwind readnone {
entry:
  %empty = zext i11 %0 to i18
  %empty_24 = zext i7 %1 to i18
  %empty_25 = shl i18 %empty, 7
  %empty_26 = or i18 %empty_25, %empty_24
  ret i18 %empty_26
}

define weak i14 @_ssdm_op_BitConcatenate.i14.i8.i6(i8, i6) nounwind readnone {
entry:
  %empty = zext i8 %0 to i14
  %empty_27 = zext i6 %1 to i14
  %empty_28 = shl i14 %empty, 6
  %empty_29 = or i14 %empty_28, %empty_27
  ret i14 %empty_29
}

define weak i14 @_ssdm_op_BitConcatenate.i14.i7.i7(i7, i7) nounwind readnone {
entry:
  %empty = zext i7 %0 to i14
  %empty_30 = zext i7 %1 to i14
  %empty_31 = shl i14 %empty, 7
  %empty_32 = or i14 %empty_31, %empty_30
  ret i14 %empty_32
}

define weak i13 @_ssdm_op_BitConcatenate.i13.i10.i3(i10, i3) nounwind readnone {
entry:
  %empty = zext i10 %0 to i13
  %empty_33 = zext i3 %1 to i13
  %empty_34 = shl i13 %empty, 3
  %empty_35 = or i13 %empty_34, %empty_33
  ret i13 %empty_35
}

define weak i12 @_ssdm_op_BitConcatenate.i12.i7.i5(i7, i5) nounwind readnone {
entry:
  %empty = zext i7 %0 to i12
  %empty_36 = zext i5 %1 to i12
  %empty_37 = shl i12 %empty, 5
  %empty_38 = or i12 %empty_37, %empty_36
  ret i12 %empty_38
}

define weak i11 @_ssdm_op_BitConcatenate.i11.i5.i6(i5, i6) nounwind readnone {
entry:
  %empty = zext i5 %0 to i11
  %empty_39 = zext i6 %1 to i11
  %empty_40 = shl i11 %empty, 6
  %empty_41 = or i11 %empty_40, %empty_39
  ret i11 %empty_41
}

define weak i10 @_ssdm_op_BitConcatenate.i10.i8.i2(i8, i2) nounwind readnone {
entry:
  %empty = zext i8 %0 to i10
  %empty_42 = zext i2 %1 to i10
  %empty_43 = shl i10 %empty, 2
  %empty_44 = or i10 %empty_43, %empty_42
  ret i10 %empty_44
}

declare void @_GLOBAL__I_a() nounwind section ".text.startup"

define void @SDR_inference([256 x float]* %input_data, [2048 x float]* %w_conv1d_1, [128 x float]* %b_conv1d_1, [131072 x float]* %w_conv1d_2, [64 x float]* %b_conv1d_2, [180224 x float]* %w_dense_1, [128 x float]* %b_dense_1, [8192 x float]* %w_dense_2, [64 x float]* %b_dense_2, [2048 x float]* %w_dense_3, [32 x float]* %b_dense_3, [320 x float]* %w_dense_4, [10 x float]* %b_dense_4, [10 x float]* %output_r) nounwind uwtable {
.preheader2667.preheader.0:
  call void (...)* @_ssdm_op_SpecBitsMap([256 x float]* %input_data) nounwind, !map !51
  call void (...)* @_ssdm_op_SpecBitsMap([2048 x float]* %w_conv1d_1) nounwind, !map !58
  call void (...)* @_ssdm_op_SpecBitsMap([128 x float]* %b_conv1d_1) nounwind, !map !64
  call void (...)* @_ssdm_op_SpecBitsMap([131072 x float]* %w_conv1d_2) nounwind, !map !69
  call void (...)* @_ssdm_op_SpecBitsMap([64 x float]* %b_conv1d_2) nounwind, !map !76
  call void (...)* @_ssdm_op_SpecBitsMap([180224 x float]* %w_dense_1) nounwind, !map !81
  call void (...)* @_ssdm_op_SpecBitsMap([128 x float]* %b_dense_1) nounwind, !map !87
  call void (...)* @_ssdm_op_SpecBitsMap([8192 x float]* %w_dense_2) nounwind, !map !91
  call void (...)* @_ssdm_op_SpecBitsMap([64 x float]* %b_dense_2) nounwind, !map !96
  call void (...)* @_ssdm_op_SpecBitsMap([2048 x float]* %w_dense_3) nounwind, !map !100
  call void (...)* @_ssdm_op_SpecBitsMap([32 x float]* %b_dense_3) nounwind, !map !106
  call void (...)* @_ssdm_op_SpecBitsMap([320 x float]* %w_dense_4) nounwind, !map !111
  call void (...)* @_ssdm_op_SpecBitsMap([10 x float]* %b_dense_4) nounwind, !map !117
  call void (...)* @_ssdm_op_SpecBitsMap([10 x float]* %output_r) nounwind, !map !122
  call void (...)* @_ssdm_op_SpecTopModule([14 x i8]* @SDR_inference_str) nounwind
  %transpose_1_0_0 = alloca [256 x float], align 4
  %pad_temp_0_0 = alloca [256 x float], align 4
  %conv1d_1_0_0 = alloca [15488 x float], align 4
  %conv1d_11_0_0 = alloca [15488 x float], align 4
  %relu_1_0_0 = alloca [15488 x float], align 4
  %maxpool1d_1_0_0 = alloca [7680 x float], align 4
  %pad_temp1_0_0 = alloca [7680 x float], align 4
  %conv1d_2_0_0 = alloca [2880 x float], align 4
  %conv1d_21_0_0 = alloca [2880 x float], align 4
  %relu_2_0_0 = alloca [2880 x float], align 4
  %maxpool1d_2_0_0 = alloca [1408 x float], align 4
  %transpose_2_0_0 = alloca [1408 x float], align 4
  %flatten_1_0 = alloca [1408 x float], align 16
  %dense_1_0 = alloca [128 x float], align 16
  %dense_11_0 = alloca [128 x float], align 16
  %relu_3_0 = alloca [128 x float], align 16
  %dense_2_0 = alloca [64 x float], align 16
  %dense_21_0 = alloca [64 x float], align 16
  %relu_4_0 = alloca [64 x float], align 16
  %dense_3_0 = alloca [32 x float], align 16
  %dense_31_0 = alloca [32 x float], align 16
  %relu_5_0 = alloca [32 x float], align 16
  %dense_4_0 = alloca [10 x float], align 16
  %dense_41_0 = alloca [10 x float], align 16
  call void (...)* @_ssdm_op_SpecMemCore([10 x float]* %b_dense_4, [1 x i8]* @p_str, [12 x i8]* @p_str1, [1 x i8]* @p_str, i32 -1, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str) nounwind
  call void (...)* @_ssdm_op_SpecMemCore([320 x float]* %w_dense_4, [1 x i8]* @p_str, [12 x i8]* @p_str1, [1 x i8]* @p_str, i32 -1, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str) nounwind
  call void (...)* @_ssdm_op_SpecMemCore([32 x float]* %b_dense_3, [1 x i8]* @p_str, [12 x i8]* @p_str1, [1 x i8]* @p_str, i32 -1, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str) nounwind
  call void (...)* @_ssdm_op_SpecMemCore([2048 x float]* %w_dense_3, [1 x i8]* @p_str, [12 x i8]* @p_str1, [1 x i8]* @p_str, i32 -1, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str) nounwind
  call void (...)* @_ssdm_op_SpecMemCore([64 x float]* %b_dense_2, [1 x i8]* @p_str, [12 x i8]* @p_str1, [1 x i8]* @p_str, i32 -1, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str) nounwind
  call void (...)* @_ssdm_op_SpecMemCore([8192 x float]* %w_dense_2, [1 x i8]* @p_str, [12 x i8]* @p_str1, [1 x i8]* @p_str, i32 -1, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str) nounwind
  call void (...)* @_ssdm_op_SpecMemCore([128 x float]* %b_dense_1, [1 x i8]* @p_str, [12 x i8]* @p_str1, [1 x i8]* @p_str, i32 -1, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str) nounwind
  call void (...)* @_ssdm_op_SpecMemCore([180224 x float]* %w_dense_1, [1 x i8]* @p_str, [12 x i8]* @p_str1, [1 x i8]* @p_str, i32 -1, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str) nounwind
  call void (...)* @_ssdm_op_SpecMemCore([64 x float]* %b_conv1d_2, [1 x i8]* @p_str, [12 x i8]* @p_str1, [1 x i8]* @p_str, i32 -1, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str) nounwind
  call void (...)* @_ssdm_op_SpecMemCore([131072 x float]* %w_conv1d_2, [1 x i8]* @p_str, [12 x i8]* @p_str1, [1 x i8]* @p_str, i32 -1, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str) nounwind
  call void (...)* @_ssdm_op_SpecMemCore([128 x float]* %b_conv1d_1, [1 x i8]* @p_str, [12 x i8]* @p_str1, [1 x i8]* @p_str, i32 -1, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str) nounwind
  call void (...)* @_ssdm_op_SpecMemCore([2048 x float]* %w_conv1d_1, [1 x i8]* @p_str, [12 x i8]* @p_str1, [1 x i8]* @p_str, i32 -1, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str) nounwind
  call void (...)* @_ssdm_op_SpecMemCore([10 x float]* %output_r, [1 x i8]* @p_str, [7 x i8]* @RAM_1P_str, [1 x i8]* @p_str, i32 -1, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str) nounwind
  call void (...)* @_ssdm_op_SpecInterface([10 x float]* %output_r, [10 x i8]* @p_str2, i32 1, i32 1, [1 x i8]* @p_str, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str)
  call void (...)* @_ssdm_op_SpecMemCore([256 x float]* %input_data, [1 x i8]* @p_str, [7 x i8]* @RAM_1P_str, [1 x i8]* @p_str, i32 -1, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str) nounwind
  call void (...)* @_ssdm_op_SpecInterface([256 x float]* %input_data, [10 x i8]* @p_str2, i32 1, i32 1, [1 x i8]* @p_str, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str)
  br label %.preheader2667.0

.preheader2667.0.loopexit:                        ; preds = %.preheader2666.0
  br label %.preheader2667.0

.preheader2667.0:                                 ; preds = %.preheader2667.0.loopexit, %.preheader2667.preheader.0
  %p_2 = phi i2 [ 0, %.preheader2667.preheader.0 ], [ %j_V, %.preheader2667.0.loopexit ]
  %exitcond3 = icmp eq i2 %p_2, -2
  %empty = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 2, i64 2, i64 2) nounwind
  %j_V = add i2 %p_2, 1
  br i1 %exitcond3, label %.preheader2665.preheader, label %.preheader2666.preheader.0

.preheader2665.preheader:                         ; preds = %.preheader2667.0
  br label %.preheader2665

.preheader2666.0:                                 ; preds = %0, %.preheader2666.preheader.0
  %p_5 = phi i8 [ %l_V, %0 ], [ 0, %.preheader2666.preheader.0 ]
  %exitcond6 = icmp eq i8 %p_5, -128
  %empty_45 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 128, i64 128, i64 128) nounwind
  %l_V = add i8 %p_5, 1
  br i1 %exitcond6, label %.preheader2667.0.loopexit, label %0

.preheader2666.preheader.0:                       ; preds = %.preheader2667.0
  %tmp_3_cast = zext i2 %p_2 to i10
  %tmp_1 = call i9 @_ssdm_op_BitConcatenate.i9.i2.i7(i2 %p_2, i7 0)
  %tmp_2_cast = zext i9 %tmp_1 to i10
  br label %.preheader2666.0

; <label>:0                                       ; preds = %.preheader2666.0
  %tmp_9_cast = zext i8 %p_5 to i10
  %tmp_3 = call i9 @_ssdm_op_BitConcatenate.i9.i8.i1(i8 %p_5, i1 false)
  %tmp_132_cast = zext i9 %tmp_3 to i10
  %tmp_4 = add i10 %tmp_3_cast, %tmp_132_cast
  %tmp_133_cast = zext i10 %tmp_4 to i64
  %input_data_addr = getelementptr [256 x float]* %input_data, i64 0, i64 %tmp_133_cast
  %tmp_5 = add i10 %tmp_9_cast, %tmp_2_cast
  %tmp_134_cast = zext i10 %tmp_5 to i64
  %transpose_1_0_0_ad = getelementptr [256 x float]* %transpose_1_0_0, i64 0, i64 %tmp_134_cast
  %input_data_load = load float* %input_data_addr, align 4
  store float %input_data_load, float* %transpose_1_0_0_ad, align 4
  br label %.preheader2666.0

.preheader2665.loopexit:                          ; preds = %.preheader2664
  br label %.preheader2665

.preheader2665:                                   ; preds = %.preheader2665.preheader, %.preheader2665.loopexit
  %p_1 = phi i2 [ %not_zero_V, %.preheader2665.loopexit ], [ 0, %.preheader2665.preheader ]
  %exitcond2 = icmp eq i2 %p_1, -2
  %empty_46 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 2, i64 2, i64 2) nounwind
  %not_zero_V = add i2 %p_1, 1
  br i1 %exitcond2, label %.preheader2663.preheader, label %.preheader2664.preheader

.preheader2663.preheader:                         ; preds = %.preheader2665
  br label %.preheader2663

.preheader2664.preheader:                         ; preds = %.preheader2665
  %tmp_2 = call i9 @_ssdm_op_BitConcatenate.i9.i2.i7(i2 %p_1, i7 0)
  %tmp_47_cast = zext i9 %tmp_2 to i10
  br label %.preheader2664

.preheader2664:                                   ; preds = %1, %.preheader2664.preheader
  %p_4 = phi i8 [ %i1_V, %1 ], [ 0, %.preheader2664.preheader ]
  %exitcond5 = icmp eq i8 %p_4, -128
  %empty_47 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 128, i64 128, i64 128) nounwind
  %i1_V = add i8 %p_4, 1
  br i1 %exitcond5, label %.preheader2665.loopexit, label %1

; <label>:1                                       ; preds = %.preheader2664
  %tmp_6_cast = zext i8 %p_4 to i10
  %tmp_8 = add i10 %tmp_47_cast, %tmp_6_cast
  %tmp_138_cast = zext i10 %tmp_8 to i64
  %transpose_1_0_0_ad_1 = getelementptr [256 x float]* %transpose_1_0_0, i64 0, i64 %tmp_138_cast
  %pad_temp_0_0_addr = getelementptr [256 x float]* %pad_temp_0_0, i64 0, i64 %tmp_138_cast
  %transpose_1_0_0_lo = load float* %transpose_1_0_0_ad_1, align 4
  store float %transpose_1_0_0_lo, float* %pad_temp_0_0_addr, align 4
  br label %.preheader2664

.preheader2663.loopexit:                          ; preds = %.preheader2662
  br label %.preheader2663

.preheader2663:                                   ; preds = %.preheader2663.preheader, %.preheader2663.loopexit
  %p_3 = phi i8 [ %ff_V, %.preheader2663.loopexit ], [ 0, %.preheader2663.preheader ]
  %phi_mul = phi i14 [ %next_mul, %.preheader2663.loopexit ], [ 0, %.preheader2663.preheader ]
  %next_mul = add i14 %phi_mul, 121
  %exitcond4 = icmp eq i8 %p_3, -128
  %empty_48 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 128, i64 128, i64 128) nounwind
  %ff_V = add i8 %p_3, 1
  br i1 %exitcond4, label %.preheader2659.preheader, label %.preheader2662.preheader

.preheader2659.preheader:                         ; preds = %.preheader2663
  br label %.preheader2659

.preheader2662.preheader:                         ; preds = %.preheader2663
  %tmp_6 = call i9 @_ssdm_op_BitConcatenate.i9.i8.i1(i8 %p_3, i1 false)
  %tmp_136_cast = zext i9 %tmp_6 to i10
  br label %.preheader2662

.preheader2662:                                   ; preds = %3, %.preheader2662.preheader
  %p_7 = phi i7 [ %xx_V, %3 ], [ 0, %.preheader2662.preheader ]
  %exitcond8 = icmp eq i7 %p_7, -7
  %empty_49 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 121, i64 121, i64 121) nounwind
  %xx_V = add i7 %p_7, 1
  br i1 %exitcond8, label %.preheader2663.loopexit, label %.preheader2661.preheader

.preheader2661.preheader:                         ; preds = %.preheader2662
  br label %.preheader2661

.preheader2661.loopexit:                          ; preds = %.preheader2660
  br label %.preheader2661

.preheader2661:                                   ; preds = %.preheader2661.preheader, %.preheader2661.loopexit
  %p_8 = phi i2 [ %rc_V, %.preheader2661.loopexit ], [ 0, %.preheader2661.preheader ]
  %reducer = phi float [ %reducer30_1, %.preheader2661.loopexit ], [ 0.000000e+00, %.preheader2661.preheader ]
  %exitcond9 = icmp eq i2 %p_8, -2
  %empty_50 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 2, i64 2, i64 2) nounwind
  %rc_V = add i2 %p_8, 1
  br i1 %exitcond9, label %3, label %.preheader2660.preheader

.preheader2660.preheader:                         ; preds = %.preheader2661
  %tmp_10_cast = zext i2 %p_8 to i10
  %tmp_11 = add i10 %tmp_10_cast, %tmp_136_cast
  %tmp_145_cast = call i13 @_ssdm_op_BitConcatenate.i13.i10.i3(i10 %tmp_11, i3 0)
  br label %.preheader2660

.preheader2660:                                   ; preds = %2, %.preheader2660.preheader
  %p_13 = phi i4 [ %rx_V, %2 ], [ 0, %.preheader2660.preheader ]
  %reducer30_1 = phi float [ %reducer30, %2 ], [ %reducer, %.preheader2660.preheader ]
  %exitcond14 = icmp eq i4 %p_13, -8
  %empty_51 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 8, i64 8, i64 8) nounwind
  %rx_V = add i4 %p_13, 1
  br i1 %exitcond14, label %.preheader2661.loopexit, label %2

; <label>:2                                       ; preds = %.preheader2660
  %rhs_V_cast = zext i4 %p_13 to i7
  %r_V = add i7 %rhs_V_cast, %p_7
  %tmp_25 = call i9 @_ssdm_op_BitConcatenate.i9.i2.i7(i2 %p_8, i7 %r_V)
  %tmp_27 = zext i9 %tmp_25 to i64
  %pad_temp_0_0_addr_1 = getelementptr [256 x float]* %pad_temp_0_0, i64 0, i64 %tmp_27
  %pad_temp_0_0_load = load float* %pad_temp_0_0_addr_1, align 4
  %tmp_14_cast = zext i4 %p_13 to i13
  %tmp_28 = add i13 %tmp_145_cast, %tmp_14_cast
  %tmp_153_cast = zext i13 %tmp_28 to i64
  %w_conv1d_1_addr = getelementptr [2048 x float]* %w_conv1d_1, i64 0, i64 %tmp_153_cast
  %w_conv1d_1_load = load float* %w_conv1d_1_addr, align 4
  %tmp_15 = fmul float %pad_temp_0_0_load, %w_conv1d_1_load
  %reducer30 = fadd float %tmp_15, %reducer30_1
  br label %.preheader2660

; <label>:3                                       ; preds = %.preheader2661
  %tmp_8_cast = zext i7 %p_7 to i14
  %tmp_10 = add i14 %phi_mul, %tmp_8_cast
  %tmp_142_cast = zext i14 %tmp_10 to i64
  %conv1d_1_0_0_addr_1 = getelementptr [15488 x float]* %conv1d_1_0_0, i64 0, i64 %tmp_142_cast
  store float %reducer, float* %conv1d_1_0_0_addr_1, align 4
  br label %.preheader2662

.preheader2659.loopexit:                          ; preds = %.preheader2658
  br label %.preheader2659

.preheader2659:                                   ; preds = %.preheader2659.preheader, %.preheader2659.loopexit
  %p_6 = phi i8 [ %j1_V, %.preheader2659.loopexit ], [ 0, %.preheader2659.preheader ]
  %phi_mul8 = phi i14 [ %next_mul9, %.preheader2659.loopexit ], [ 0, %.preheader2659.preheader ]
  %next_mul9 = add i14 %phi_mul8, 121
  %exitcond7 = icmp eq i8 %p_6, -128
  %empty_52 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 128, i64 128, i64 128) nounwind
  %j1_V = add i8 %p_6, 1
  br i1 %exitcond7, label %.preheader2656.0.preheader, label %.preheader2658.preheader

.preheader2656.0.preheader:                       ; preds = %.preheader2659
  br label %.preheader2656.0

.preheader2658.preheader:                         ; preds = %.preheader2659
  %tmp_s = zext i8 %p_6 to i64
  %b_conv1d_1_addr = getelementptr [128 x float]* %b_conv1d_1, i64 0, i64 %tmp_s
  %b_conv1d_1_load = load float* %b_conv1d_1_addr, align 4
  br label %.preheader2658

.preheader2658:                                   ; preds = %4, %.preheader2658.preheader
  %p_9 = phi i7 [ %l1_V, %4 ], [ 0, %.preheader2658.preheader ]
  %exitcond = icmp eq i7 %p_9, -7
  %empty_53 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 121, i64 121, i64 121) nounwind
  %l1_V = add i7 %p_9, 1
  br i1 %exitcond, label %.preheader2659.loopexit, label %4

; <label>:4                                       ; preds = %.preheader2658
  %tmp_5_cast = zext i7 %p_9 to i14
  %tmp_9 = add i14 %phi_mul8, %tmp_5_cast
  %tmp_141_cast = zext i14 %tmp_9 to i64
  %conv1d_1_0_0_addr = getelementptr [15488 x float]* %conv1d_1_0_0, i64 0, i64 %tmp_141_cast
  %conv1d_11_0_0_addr = getelementptr [15488 x float]* %conv1d_11_0_0, i64 0, i64 %tmp_141_cast
  %conv1d_1_0_0_load = load float* %conv1d_1_0_0_addr, align 4
  %tmp_7 = fadd float %conv1d_1_0_0_load, %b_conv1d_1_load
  store float %tmp_7, float* %conv1d_11_0_0_addr, align 4
  br label %.preheader2658

.preheader2656.0.loopexit:                        ; preds = %.preheader2655.0
  br label %.preheader2656.0

.preheader2656.0:                                 ; preds = %.preheader2656.0.preheader, %.preheader2656.0.loopexit
  %p_12 = phi i8 [ %args0_V, %.preheader2656.0.loopexit ], [ 0, %.preheader2656.0.preheader ]
  %phi_mul1 = phi i14 [ %next_mul1, %.preheader2656.0.loopexit ], [ 0, %.preheader2656.0.preheader ]
  %next_mul1 = add i14 %phi_mul1, 121
  %exitcond13 = icmp eq i8 %p_12, -128
  %empty_54 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 128, i64 128, i64 128) nounwind
  %args0_V = add i8 %p_12, 1
  br i1 %exitcond13, label %.preheader2653.0.preheader, label %.preheader2655.0.preheader

.preheader2655.0.preheader:                       ; preds = %.preheader2656.0
  br label %.preheader2655.0

.preheader2653.0.preheader:                       ; preds = %.preheader2656.0
  br label %.preheader2653.0

.preheader2655.0:                                 ; preds = %.preheader2655.0.preheader, %._crit_edge.0
  %p_16 = phi i7 [ %args2_V, %._crit_edge.0 ], [ 0, %.preheader2655.0.preheader ]
  %exitcond17 = icmp eq i7 %p_16, -7
  %empty_55 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 121, i64 121, i64 121) nounwind
  %args2_V = add i7 %p_16, 1
  br i1 %exitcond17, label %.preheader2656.0.loopexit, label %._crit_edge.0

._crit_edge.0:                                    ; preds = %.preheader2655.0
  %tmp_18_cast = zext i7 %p_16 to i14
  %tmp_17 = add i14 %phi_mul1, %tmp_18_cast
  %tmp_150_cast = zext i14 %tmp_17 to i64
  %relu_1_0_0_addr = getelementptr [15488 x float]* %relu_1_0_0, i64 0, i64 %tmp_150_cast
  %conv1d_11_0_0_addr_1 = getelementptr [15488 x float]* %conv1d_11_0_0, i64 0, i64 %tmp_150_cast
  %conv1d_11_0_0_load = load float* %conv1d_11_0_0_addr_1, align 4
  %conv1d_11_0_0_load_1 = bitcast float %conv1d_11_0_0_load to i32
  %tmp = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %conv1d_11_0_0_load_1, i32 23, i32 30)
  %tmp_18 = trunc i32 %conv1d_11_0_0_load_1 to i23
  %notlhs = icmp ne i8 %tmp, -1
  %notrhs = icmp eq i23 %tmp_18, 0
  %tmp_19 = or i1 %notrhs, %notlhs
  %tmp_20 = fcmp olt float %conv1d_11_0_0_load, 0.000000e+00
  %tmp_21 = and i1 %tmp_19, %tmp_20
  %tmp_22 = select i1 %tmp_21, float 0.000000e+00, float %conv1d_11_0_0_load
  store float %tmp_22, float* %relu_1_0_0_addr, align 4
  br label %.preheader2655.0

.preheader2653.0.loopexit:                        ; preds = %.preheader2652.0
  br label %.preheader2653.0

.preheader2653.0:                                 ; preds = %.preheader2653.0.preheader, %.preheader2653.0.loopexit
  %p_15 = phi i8 [ %c_V, %.preheader2653.0.loopexit ], [ 0, %.preheader2653.0.preheader ]
  %phi_mul2 = phi i14 [ %next_mul2, %.preheader2653.0.loopexit ], [ 0, %.preheader2653.0.preheader ]
  %next_mul2 = add i14 %phi_mul2, 121
  %exitcond16 = icmp eq i8 %p_15, -128
  %empty_56 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 128, i64 128, i64 128) nounwind
  %c_V = add i8 %p_15, 1
  br i1 %exitcond16, label %.preheader2651.preheader, label %.preheader2652.preheader.0

.preheader2651.preheader:                         ; preds = %.preheader2653.0
  br label %.preheader2651

.preheader2652.0:                                 ; preds = %5, %.preheader2652.preheader.0
  %p_19 = phi i6 [ %w_V, %5 ], [ 0, %.preheader2652.preheader.0 ]
  %exitcond20 = icmp eq i6 %p_19, -4
  %empty_57 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 60, i64 60, i64 60) nounwind
  %w_V = add i6 %p_19, 1
  br i1 %exitcond20, label %.preheader2653.0.loopexit, label %7

.preheader2652.preheader.0:                       ; preds = %.preheader2653.0
  %tmp_12 = call i14 @_ssdm_op_BitConcatenate.i14.i8.i6(i8 %p_15, i6 0)
  %tmp_13 = call i10 @_ssdm_op_BitConcatenate.i10.i8.i2(i8 %p_15, i2 0)
  %p_shl1_cast = zext i10 %tmp_13 to i14
  %tmp_14 = sub i14 %tmp_12, %p_shl1_cast
  br label %.preheader2652.0

; <label>:5                                       ; preds = %6
  store float %tmp_23, float* %maxpool1d_1_0_0_ad, align 4
  br label %.preheader2652.0

; <label>:6                                       ; preds = %8, %7
  %tmp_23 = phi float [ -1.000000e+00, %7 ], [ %reducer3, %8 ]
  %p_22 = phi i2 [ 0, %7 ], [ %ra31_V, %8 ]
  %exitcond23 = icmp eq i2 %p_22, -2
  %empty_58 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 2, i64 2, i64 2) nounwind
  %ra31_V = add i2 %p_22, 1
  br i1 %exitcond23, label %5, label %8

; <label>:7                                       ; preds = %.preheader2652.0
  %lhs_V_1_cast = zext i6 %p_19 to i14
  %tmp_34 = add i14 %lhs_V_1_cast, %tmp_14
  %tmp_157_cast = sext i14 %tmp_34 to i64
  %maxpool1d_1_0_0_ad = getelementptr [7680 x float]* %maxpool1d_1_0_0, i64 0, i64 %tmp_157_cast
  %r_V_1 = call i7 @_ssdm_op_BitConcatenate.i7.i6.i1(i6 %p_19, i1 false)
  br label %6

; <label>:8                                       ; preds = %6
  %tmp_25_cast = zext i2 %p_22 to i7
  %tmp_26 = add i7 %tmp_25_cast, %r_V_1
  %tmp_26_cast_cast = zext i7 %tmp_26 to i14
  %tmp_40 = add i14 %phi_mul2, %tmp_26_cast_cast
  %tmp_162_cast = zext i14 %tmp_40 to i64
  %relu_1_0_0_addr_1 = getelementptr [15488 x float]* %relu_1_0_0, i64 0, i64 %tmp_162_cast
  %relu_1_0_0_load = load float* %relu_1_0_0_addr_1, align 4
  %relu_1_0_0_load_to = bitcast float %relu_1_0_0_load to i32
  %tmp_41 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %relu_1_0_0_load_to, i32 23, i32 30)
  %tmp_43 = trunc i32 %relu_1_0_0_load_to to i23
  %tmp_23_to_int = bitcast float %tmp_23 to i32
  %tmp_47 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %tmp_23_to_int, i32 23, i32 30)
  %tmp_49 = trunc i32 %tmp_23_to_int to i23
  %notlhs1 = icmp ne i8 %tmp_41, -1
  %notrhs1 = icmp eq i23 %tmp_43, 0
  %tmp_50 = or i1 %notrhs1, %notlhs1
  %notlhs2 = icmp ne i8 %tmp_47, -1
  %notrhs2 = icmp eq i23 %tmp_49, 0
  %tmp_51 = or i1 %notrhs2, %notlhs2
  %tmp_52 = and i1 %tmp_50, %tmp_51
  %tmp_54 = fcmp olt float %relu_1_0_0_load, %tmp_23
  %tmp_62 = and i1 %tmp_52, %tmp_54
  %reducer3 = select i1 %tmp_62, float %tmp_23, float %relu_1_0_0_load
  br label %6

.preheader2651.loopexit:                          ; preds = %.preheader2650
  br label %.preheader2651

.preheader2651:                                   ; preds = %.preheader2651.preheader, %.preheader2651.loopexit
  %p_s = phi i8 [ %not_zero1_V, %.preheader2651.loopexit ], [ 0, %.preheader2651.preheader ]
  %exitcond1 = icmp eq i8 %p_s, -128
  %empty_59 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 128, i64 128, i64 128) nounwind
  %not_zero1_V = add i8 %p_s, 1
  br i1 %exitcond1, label %.preheader2649.preheader, label %.preheader2650.preheader

.preheader2649.preheader:                         ; preds = %.preheader2651
  br label %.preheader2649

.preheader2650.preheader:                         ; preds = %.preheader2651
  %tmp_29 = call i14 @_ssdm_op_BitConcatenate.i14.i8.i6(i8 %p_s, i6 0)
  %tmp_30 = call i10 @_ssdm_op_BitConcatenate.i10.i8.i2(i8 %p_s, i2 0)
  %p_shl3_cast = zext i10 %tmp_30 to i14
  %tmp_31 = sub i14 %tmp_29, %p_shl3_cast
  br label %.preheader2650

.preheader2650:                                   ; preds = %9, %.preheader2650.preheader
  %p_14 = phi i6 [ %i3_V, %9 ], [ 0, %.preheader2650.preheader ]
  %exitcond12 = icmp eq i6 %p_14, -4
  %empty_60 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 60, i64 60, i64 60) nounwind
  %i3_V = add i6 %p_14, 1
  br i1 %exitcond12, label %.preheader2651.loopexit, label %9

; <label>:9                                       ; preds = %.preheader2650
  %tmp_20_cast = zext i6 %p_14 to i14
  %tmp_39 = add i14 %tmp_31, %tmp_20_cast
  %tmp_161_cast = sext i14 %tmp_39 to i64
  %maxpool1d_1_0_0_ad_1 = getelementptr [7680 x float]* %maxpool1d_1_0_0, i64 0, i64 %tmp_161_cast
  %pad_temp1_0_0_addr = getelementptr [7680 x float]* %pad_temp1_0_0, i64 0, i64 %tmp_161_cast
  %maxpool1d_1_0_0_lo = load float* %maxpool1d_1_0_0_ad_1, align 4
  store float %maxpool1d_1_0_0_lo, float* %pad_temp1_0_0_addr, align 4
  br label %.preheader2650

.preheader2649.loopexit:                          ; preds = %.preheader2648
  br label %.preheader2649

.preheader2649:                                   ; preds = %.preheader2649.preheader, %.preheader2649.loopexit
  %p_10 = phi i7 [ %ff1_V, %.preheader2649.loopexit ], [ 0, %.preheader2649.preheader ]
  %phi_mul3 = phi i12 [ %next_mul3, %.preheader2649.loopexit ], [ 0, %.preheader2649.preheader ]
  %next_mul3 = add i12 %phi_mul3, 45
  %exitcond10 = icmp eq i7 %p_10, -64
  %empty_61 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 64, i64 64, i64 64) nounwind
  %ff1_V = add i7 %p_10, 1
  br i1 %exitcond10, label %.preheader2645.preheader, label %.preheader2648.preheader

.preheader2645.preheader:                         ; preds = %.preheader2649
  br label %.preheader2645

.preheader2648.preheader:                         ; preds = %.preheader2649
  %tmp_35 = call i14 @_ssdm_op_BitConcatenate.i14.i7.i7(i7 %p_10, i7 0)
  %tmp_159_cast = zext i14 %tmp_35 to i15
  br label %.preheader2648

.preheader2648:                                   ; preds = %11, %.preheader2648.preheader
  %p_17 = phi i6 [ %xx1_V, %11 ], [ 0, %.preheader2648.preheader ]
  %exitcond15 = icmp eq i6 %p_17, -19
  %empty_62 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 45, i64 45, i64 45) nounwind
  %xx1_V = add i6 %p_17, 1
  br i1 %exitcond15, label %.preheader2649.loopexit, label %.preheader2647.preheader

.preheader2647.preheader:                         ; preds = %.preheader2648
  br label %.preheader2647

.preheader2647.loopexit:                          ; preds = %.preheader2646
  br label %.preheader2647

.preheader2647:                                   ; preds = %.preheader2647.preheader, %.preheader2647.loopexit
  %p_20 = phi i8 [ %rc1_V, %.preheader2647.loopexit ], [ 0, %.preheader2647.preheader ]
  %reducer2 = phi float [ %reducer32_1, %.preheader2647.loopexit ], [ 0.000000e+00, %.preheader2647.preheader ]
  %exitcond19 = icmp eq i8 %p_20, -128
  %empty_63 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 128, i64 128, i64 128) nounwind
  %rc1_V = add i8 %p_20, 1
  br i1 %exitcond19, label %11, label %.preheader2646.preheader

.preheader2646.preheader:                         ; preds = %.preheader2647
  %tmp_29_cast = zext i8 %p_20 to i15
  %tmp_91 = add i15 %tmp_159_cast, %tmp_29_cast
  %tmp_169_cast = call i19 @_ssdm_op_BitConcatenate.i19.i15.i4(i15 %tmp_91, i4 0)
  %tmp_92 = call i14 @_ssdm_op_BitConcatenate.i14.i8.i6(i8 %p_20, i6 0)
  %tmp_93 = call i10 @_ssdm_op_BitConcatenate.i10.i8.i2(i8 %p_20, i2 0)
  %p_shl5_cast = zext i10 %tmp_93 to i14
  %tmp_95 = sub i14 %tmp_92, %p_shl5_cast
  br label %.preheader2646

.preheader2646:                                   ; preds = %10, %.preheader2646.preheader
  %p_21 = phi i5 [ %rx1_V, %10 ], [ 0, %.preheader2646.preheader ]
  %reducer32_1 = phi float [ %reducer32, %10 ], [ %reducer2, %.preheader2646.preheader ]
  %exitcond21 = icmp eq i5 %p_21, -16
  %empty_64 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 16, i64 16, i64 16) nounwind
  %rx1_V = add i5 %p_21, 1
  br i1 %exitcond21, label %.preheader2647.loopexit, label %10

; <label>:10                                      ; preds = %.preheader2646
  %rhs_V_1_cast = zext i5 %p_21 to i6
  %r_V_2 = add i6 %rhs_V_1_cast, %p_17
  %tmp_30_cast = zext i6 %r_V_2 to i14
  %tmp_109 = add i14 %tmp_95, %tmp_30_cast
  %tmp_176_cast = sext i14 %tmp_109 to i64
  %pad_temp1_0_0_addr_1 = getelementptr [7680 x float]* %pad_temp1_0_0, i64 0, i64 %tmp_176_cast
  %pad_temp1_0_0_load = load float* %pad_temp1_0_0_addr_1, align 4
  %tmp_31_cast = zext i5 %p_21 to i19
  %tmp_114 = add i19 %tmp_169_cast, %tmp_31_cast
  %tmp_177_cast = zext i19 %tmp_114 to i64
  %w_conv1d_2_addr = getelementptr [131072 x float]* %w_conv1d_2, i64 0, i64 %tmp_177_cast
  %w_conv1d_2_load = load float* %w_conv1d_2_addr, align 4
  %tmp_32 = fmul float %pad_temp1_0_0_load, %w_conv1d_2_load
  %reducer32 = fadd float %tmp_32, %reducer32_1
  br label %.preheader2646

; <label>:11                                      ; preds = %.preheader2647
  %tmp_28_cast = zext i6 %p_17 to i12
  %tmp_78 = add i12 %phi_mul3, %tmp_28_cast
  %tmp_166_cast = zext i12 %tmp_78 to i64
  %conv1d_2_0_0_addr_1 = getelementptr [2880 x float]* %conv1d_2_0_0, i64 0, i64 %tmp_166_cast
  store float %reducer2, float* %conv1d_2_0_0_addr_1, align 4
  br label %.preheader2648

.preheader2645.loopexit:                          ; preds = %.preheader2644
  br label %.preheader2645

.preheader2645:                                   ; preds = %.preheader2645.preheader, %.preheader2645.loopexit
  %p_11 = phi i7 [ %j2_V, %.preheader2645.loopexit ], [ 0, %.preheader2645.preheader ]
  %phi_mul4 = phi i12 [ %next_mul4, %.preheader2645.loopexit ], [ 0, %.preheader2645.preheader ]
  %next_mul4 = add i12 %phi_mul4, 45
  %exitcond11 = icmp eq i7 %p_11, -64
  %empty_65 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 64, i64 64, i64 64) nounwind
  %j2_V = add i7 %p_11, 1
  br i1 %exitcond11, label %.preheader2642.0.preheader, label %.preheader2644.preheader

.preheader2642.0.preheader:                       ; preds = %.preheader2645
  br label %.preheader2642.0

.preheader2644.preheader:                         ; preds = %.preheader2645
  %tmp_16 = zext i7 %p_11 to i64
  %b_conv1d_2_addr = getelementptr [64 x float]* %b_conv1d_2, i64 0, i64 %tmp_16
  %b_conv1d_2_load = load float* %b_conv1d_2_addr, align 4
  br label %.preheader2644

.preheader2644:                                   ; preds = %12, %.preheader2644.preheader
  %p_18 = phi i6 [ %l2_V, %12 ], [ 0, %.preheader2644.preheader ]
  %exitcond18 = icmp eq i6 %p_18, -19
  %empty_66 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 45, i64 45, i64 45) nounwind
  %l2_V = add i6 %p_18, 1
  br i1 %exitcond18, label %.preheader2645.loopexit, label %12

; <label>:12                                      ; preds = %.preheader2644
  %tmp_21_cast = zext i6 %p_18 to i12
  %tmp_70 = add i12 %phi_mul4, %tmp_21_cast
  %tmp_165_cast = zext i12 %tmp_70 to i64
  %conv1d_21_0_0_addr = getelementptr [2880 x float]* %conv1d_21_0_0, i64 0, i64 %tmp_165_cast
  %conv1d_2_0_0_addr = getelementptr [2880 x float]* %conv1d_2_0_0, i64 0, i64 %tmp_165_cast
  %conv1d_2_0_0_load = load float* %conv1d_2_0_0_addr, align 4
  %tmp_24 = fadd float %conv1d_2_0_0_load, %b_conv1d_2_load
  store float %tmp_24, float* %conv1d_21_0_0_addr, align 4
  br label %.preheader2644

.preheader2642.0.loopexit:                        ; preds = %.preheader2641.0
  br label %.preheader2642.0

.preheader2642.0:                                 ; preds = %.preheader2642.0.preheader, %.preheader2642.0.loopexit
  %p_25 = phi i7 [ %args01_V, %.preheader2642.0.loopexit ], [ 0, %.preheader2642.0.preheader ]
  %phi_mul5 = phi i12 [ %next_mul5, %.preheader2642.0.loopexit ], [ 0, %.preheader2642.0.preheader ]
  %next_mul5 = add i12 %phi_mul5, 45
  %exitcond26 = icmp eq i7 %p_25, -64
  %empty_67 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 64, i64 64, i64 64) nounwind
  %args01_V = add i7 %p_25, 1
  br i1 %exitcond26, label %.preheader2639.0.preheader, label %.preheader2641.0.preheader

.preheader2641.0.preheader:                       ; preds = %.preheader2642.0
  br label %.preheader2641.0

.preheader2639.0.preheader:                       ; preds = %.preheader2642.0
  br label %.preheader2639.0

.preheader2641.0:                                 ; preds = %.preheader2641.0.preheader, %._crit_edge2669.0
  %p_27 = phi i6 [ %args21_V, %._crit_edge2669.0 ], [ 0, %.preheader2641.0.preheader ]
  %exitcond28 = icmp eq i6 %p_27, -19
  %empty_68 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 45, i64 45, i64 45) nounwind
  %args21_V = add i6 %p_27, 1
  br i1 %exitcond28, label %.preheader2642.0.loopexit, label %._crit_edge2669.0

._crit_edge2669.0:                                ; preds = %.preheader2641.0
  %tmp_40_cast = zext i6 %p_27 to i12
  %tmp_100 = add i12 %phi_mul5, %tmp_40_cast
  %tmp_175_cast = zext i12 %tmp_100 to i64
  %relu_2_0_0_addr = getelementptr [2880 x float]* %relu_2_0_0, i64 0, i64 %tmp_175_cast
  %conv1d_21_0_0_addr_1 = getelementptr [2880 x float]* %conv1d_21_0_0, i64 0, i64 %tmp_175_cast
  %conv1d_21_0_0_load = load float* %conv1d_21_0_0_addr_1, align 4
  %conv1d_21_0_0_load_1 = bitcast float %conv1d_21_0_0_load to i32
  %tmp_94 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %conv1d_21_0_0_load_1, i32 23, i32 30)
  %tmp_102 = trunc i32 %conv1d_21_0_0_load_1 to i23
  %notlhs3 = icmp ne i8 %tmp_94, -1
  %notrhs3 = icmp eq i23 %tmp_102, 0
  %tmp_96 = or i1 %notrhs3, %notlhs3
  %tmp_97 = fcmp olt float %conv1d_21_0_0_load, 0.000000e+00
  %tmp_98 = and i1 %tmp_96, %tmp_97
  %tmp_44 = select i1 %tmp_98, float 0.000000e+00, float %conv1d_21_0_0_load
  store float %tmp_44, float* %relu_2_0_0_addr, align 4
  br label %.preheader2641.0

.preheader2639.0.loopexit:                        ; preds = %.preheader2638.0
  br label %.preheader2639.0

.preheader2639.0:                                 ; preds = %.preheader2639.0.preheader, %.preheader2639.0.loopexit
  %p_26 = phi i7 [ %c1_V, %.preheader2639.0.loopexit ], [ 0, %.preheader2639.0.preheader ]
  %phi_mul6 = phi i12 [ %next_mul7, %.preheader2639.0.loopexit ], [ 0, %.preheader2639.0.preheader ]
  %phi_mul7 = phi i11 [ %next_mul6, %.preheader2639.0.loopexit ], [ 0, %.preheader2639.0.preheader ]
  %next_mul6 = add i11 %phi_mul7, 22
  %next_mul7 = add i12 %phi_mul6, 45
  %exitcond27 = icmp eq i7 %p_26, -64
  %empty_69 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 64, i64 64, i64 64) nounwind
  %c1_V = add i7 %p_26, 1
  br i1 %exitcond27, label %.preheader2636.0.preheader, label %.preheader2638.0.preheader

.preheader2638.0.preheader:                       ; preds = %.preheader2639.0
  br label %.preheader2638.0

.preheader2636.0.preheader:                       ; preds = %.preheader2639.0
  br label %.preheader2636.0

.preheader2638.0:                                 ; preds = %.preheader2638.0.preheader, %13
  %p_29 = phi i5 [ %w1_V, %13 ], [ 0, %.preheader2638.0.preheader ]
  %exitcond30 = icmp eq i5 %p_29, -10
  %empty_70 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 22, i64 22, i64 22) nounwind
  %w1_V = add i5 %p_29, 1
  br i1 %exitcond30, label %.preheader2639.0.loopexit, label %15

; <label>:13                                      ; preds = %14
  store float %tmp_45, float* %maxpool1d_2_0_0_ad, align 4
  br label %.preheader2638.0

; <label>:14                                      ; preds = %16, %15
  %tmp_45 = phi float [ -1.000000e+00, %15 ], [ %reducer9, %16 ]
  %p_32 = phi i2 [ 0, %15 ], [ %ra33_V, %16 ]
  %exitcond33 = icmp eq i2 %p_32, -2
  %empty_71 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 2, i64 2, i64 2) nounwind
  %ra33_V = add i2 %p_32, 1
  br i1 %exitcond33, label %13, label %16

; <label>:15                                      ; preds = %.preheader2638.0
  %lhs_V_3_cast = zext i5 %p_29 to i11
  %tmp_124 = add i11 %lhs_V_3_cast, %phi_mul7
  %tmp_180_cast = zext i11 %tmp_124 to i64
  %maxpool1d_2_0_0_ad = getelementptr [1408 x float]* %maxpool1d_2_0_0, i64 0, i64 %tmp_180_cast
  %r_V_4 = call i6 @_ssdm_op_BitConcatenate.i6.i5.i1(i5 %p_29, i1 false)
  br label %14

; <label>:16                                      ; preds = %14
  %tmp_52_cast = zext i2 %p_32 to i6
  %tmp_53 = add i6 %tmp_52_cast, %r_V_4
  %tmp_53_cast_cast = zext i6 %tmp_53 to i12
  %tmp_134 = add i12 %phi_mul6, %tmp_53_cast_cast
  %tmp_185_cast = zext i12 %tmp_134 to i64
  %relu_2_0_0_addr_1 = getelementptr [2880 x float]* %relu_2_0_0, i64 0, i64 %tmp_185_cast
  %relu_2_0_0_load = load float* %relu_2_0_0_addr_1, align 4
  %relu_2_0_0_load_to = bitcast float %relu_2_0_0_load to i32
  %tmp_99 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %relu_2_0_0_load_to, i32 23, i32 30)
  %tmp_135 = trunc i32 %relu_2_0_0_load_to to i23
  %tmp_45_to_int = bitcast float %tmp_45 to i32
  %tmp_101 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %tmp_45_to_int, i32 23, i32 30)
  %tmp_136 = trunc i32 %tmp_45_to_int to i23
  %notlhs4 = icmp ne i8 %tmp_99, -1
  %notrhs4 = icmp eq i23 %tmp_135, 0
  %tmp_103 = or i1 %notrhs4, %notlhs4
  %notlhs5 = icmp ne i8 %tmp_101, -1
  %notrhs5 = icmp eq i23 %tmp_136, 0
  %tmp_104 = or i1 %notrhs5, %notlhs5
  %tmp_105 = and i1 %tmp_103, %tmp_104
  %tmp_106 = fcmp olt float %relu_2_0_0_load, %tmp_45
  %tmp_107 = and i1 %tmp_105, %tmp_106
  %reducer9 = select i1 %tmp_107, float %tmp_45, float %relu_2_0_0_load
  br label %14

.preheader2636.0.loopexit:                        ; preds = %.preheader2635.0
  br label %.preheader2636.0

.preheader2636.0:                                 ; preds = %.preheader2636.0.preheader, %.preheader2636.0.loopexit
  %p_28 = phi i5 [ %j3_V, %.preheader2636.0.loopexit ], [ 0, %.preheader2636.0.preheader ]
  %exitcond29 = icmp eq i5 %p_28, -10
  %empty_72 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 22, i64 22, i64 22) nounwind
  %j3_V = add i5 %p_28, 1
  br i1 %exitcond29, label %.preheader2633.0.preheader, label %.preheader2635.preheader.0

.preheader2633.0.preheader:                       ; preds = %.preheader2636.0
  br label %.preheader2633.0

.preheader2635.0:                                 ; preds = %17, %.preheader2635.preheader.0
  %p_31 = phi i7 [ %l3_V, %17 ], [ 0, %.preheader2635.preheader.0 ]
  %phi_mul9 = phi i11 [ %next_mul8, %17 ], [ 0, %.preheader2635.preheader.0 ]
  %exitcond32 = icmp eq i7 %p_31, -64
  %empty_73 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 64, i64 64, i64 64) nounwind
  %l3_V = add i7 %p_31, 1
  br i1 %exitcond32, label %.preheader2636.0.loopexit, label %17

.preheader2635.preheader.0:                       ; preds = %.preheader2636.0
  %tmp_43_cast = zext i5 %p_28 to i11
  %tmp_119 = call i11 @_ssdm_op_BitConcatenate.i11.i5.i6(i5 %p_28, i6 0)
  %tmp_179_cast = zext i11 %tmp_119 to i12
  br label %.preheader2635.0

; <label>:17                                      ; preds = %.preheader2635.0
  %tmp_51_cast = zext i7 %p_31 to i12
  %tmp_132 = add i12 %tmp_179_cast, %tmp_51_cast
  %tmp_182_cast = zext i12 %tmp_132 to i64
  %transpose_2_0_0_ad = getelementptr [1408 x float]* %transpose_2_0_0, i64 0, i64 %tmp_182_cast
  %next_mul8 = add i11 %phi_mul9, 22
  %tmp_133 = add i11 %phi_mul9, %tmp_43_cast
  %tmp_184_cast = zext i11 %tmp_133 to i64
  %maxpool1d_2_0_0_ad_1 = getelementptr [1408 x float]* %maxpool1d_2_0_0, i64 0, i64 %tmp_184_cast
  %maxpool1d_2_0_0_lo = load float* %maxpool1d_2_0_0_ad_1, align 4
  store float %maxpool1d_2_0_0_lo, float* %transpose_2_0_0_ad, align 4
  br label %.preheader2635.0

.preheader2633.0:                                 ; preds = %.preheader2633.0.preheader, %18
  %p_30 = phi i11 [ %j4_V, %18 ], [ 0, %.preheader2633.0.preheader ]
  %exitcond31 = icmp eq i11 %p_30, -640
  %empty_74 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 1408, i64 1408, i64 1408) nounwind
  %j4_V = add i11 %p_30, 1
  br i1 %exitcond31, label %.preheader2631.0.preheader, label %18

.preheader2631.0.preheader:                       ; preds = %.preheader2633.0
  br label %.preheader2631.0

; <label>:18                                      ; preds = %.preheader2633.0
  %tmp_126 = zext i11 %p_30 to i64
  %transpose_2_0_0_ad_1 = getelementptr [1408 x float]* %transpose_2_0_0, i64 0, i64 %tmp_126
  %transpose_2_0_0_lo = load float* %transpose_2_0_0_ad_1, align 4
  %flatten_1_0_addr = getelementptr [1408 x float]* %flatten_1_0, i64 0, i64 %tmp_126
  store float %transpose_2_0_0_lo, float* %flatten_1_0_addr, align 4
  br label %.preheader2633.0

.preheader2631.0:                                 ; preds = %.preheader2631.0.preheader, %19
  %p_33 = phi i8 [ %j5_V, %19 ], [ 0, %.preheader2631.0.preheader ]
  %exitcond34 = icmp eq i8 %p_33, -128
  %empty_75 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 128, i64 128, i64 128) nounwind
  %j5_V = add i8 %p_33, 1
  br i1 %exitcond34, label %.preheader2628.0.preheader, label %.preheader2630.preheader.0

.preheader2628.0.preheader:                       ; preds = %.preheader2631.0
  br label %.preheader2628.0

; <label>:19                                      ; preds = %.preheader2630.0
  %dense_1_0_addr_1 = getelementptr [128 x float]* %dense_1_0, i64 0, i64 %tmp_56
  store float %reducer4, float* %dense_1_0_addr_1, align 4
  br label %.preheader2631.0

.preheader2630.0:                                 ; preds = %20, %.preheader2630.preheader.0
  %p_35 = phi i11 [ %ra34_V, %20 ], [ 0, %.preheader2630.preheader.0 ]
  %reducer4 = phi float [ %reducer10, %20 ], [ 0.000000e+00, %.preheader2630.preheader.0 ]
  %exitcond36 = icmp eq i11 %p_35, -640
  %empty_76 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 1408, i64 1408, i64 1408) nounwind
  %ra34_V = add i11 %p_35, 1
  br i1 %exitcond36, label %19, label %20

.preheader2630.preheader.0:                       ; preds = %.preheader2631.0
  %tmp_56 = zext i8 %p_33 to i64
  %tmp_56_cast = zext i8 %p_33 to i19
  br label %.preheader2630.0

; <label>:20                                      ; preds = %.preheader2630.0
  %tmp_59 = zext i11 %p_35 to i64
  %tmp_137 = call i18 @_ssdm_op_BitConcatenate.i18.i11.i7(i11 %p_35, i7 0)
  %tmp_187_cast = zext i18 %tmp_137 to i19
  %tmp_138 = add i19 %tmp_56_cast, %tmp_187_cast
  %tmp_188_cast = zext i19 %tmp_138 to i64
  %w_dense_1_addr = getelementptr [180224 x float]* %w_dense_1, i64 0, i64 %tmp_188_cast
  %flatten_1_0_addr_1 = getelementptr [1408 x float]* %flatten_1_0, i64 0, i64 %tmp_59
  %flatten_1_0_load = load float* %flatten_1_0_addr_1, align 4
  %w_dense_1_load = load float* %w_dense_1_addr, align 4
  %tmp_60 = fmul float %flatten_1_0_load, %w_dense_1_load
  %reducer10 = fadd float %tmp_60, %reducer4
  br label %.preheader2630.0

.preheader2628.0:                                 ; preds = %.preheader2628.0.preheader, %21
  %p_34 = phi i8 [ %j6_V, %21 ], [ 0, %.preheader2628.0.preheader ]
  %exitcond35 = icmp eq i8 %p_34, -128
  %empty_77 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 128, i64 128, i64 128) nounwind
  %j6_V = add i8 %p_34, 1
  br i1 %exitcond35, label %.preheader2626.0.preheader, label %21

.preheader2626.0.preheader:                       ; preds = %.preheader2628.0
  br label %.preheader2626.0

; <label>:21                                      ; preds = %.preheader2628.0
  %tmp_57 = zext i8 %p_34 to i64
  %dense_1_0_addr = getelementptr [128 x float]* %dense_1_0, i64 0, i64 %tmp_57
  %dense_1_0_load = load float* %dense_1_0_addr, align 4
  %b_dense_1_addr = getelementptr [128 x float]* %b_dense_1, i64 0, i64 %tmp_57
  %b_dense_1_load = load float* %b_dense_1_addr, align 4
  %tmp_58 = fadd float %dense_1_0_load, %b_dense_1_load
  %dense_11_0_addr = getelementptr [128 x float]* %dense_11_0, i64 0, i64 %tmp_57
  store float %tmp_58, float* %dense_11_0_addr, align 4
  br label %.preheader2628.0

.preheader2626.0:                                 ; preds = %.preheader2626.0.preheader, %._crit_edge2670.0
  %p_36 = phi i8 [ %args02_V, %._crit_edge2670.0 ], [ 0, %.preheader2626.0.preheader ]
  %exitcond37 = icmp eq i8 %p_36, -128
  %empty_78 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 128, i64 128, i64 128) nounwind
  %args02_V = add i8 %p_36, 1
  br i1 %exitcond37, label %.preheader2624.0.preheader, label %._crit_edge2670.0

.preheader2624.0.preheader:                       ; preds = %.preheader2626.0
  br label %.preheader2624.0

._crit_edge2670.0:                                ; preds = %.preheader2626.0
  %tmp_61 = zext i8 %p_36 to i64
  %dense_11_0_addr_1 = getelementptr [128 x float]* %dense_11_0, i64 0, i64 %tmp_61
  %dense_11_0_load = load float* %dense_11_0_addr_1, align 4
  %dense_11_0_load_to_s = bitcast float %dense_11_0_load to i32
  %tmp_108 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %dense_11_0_load_to_s, i32 23, i32 30)
  %tmp_139 = trunc i32 %dense_11_0_load_to_s to i23
  %notlhs6 = icmp ne i8 %tmp_108, -1
  %notrhs6 = icmp eq i23 %tmp_139, 0
  %tmp_110 = or i1 %notrhs6, %notlhs6
  %tmp_111 = fcmp olt float %dense_11_0_load, 0.000000e+00
  %tmp_112 = and i1 %tmp_110, %tmp_111
  %tmp_63 = select i1 %tmp_112, float 0.000000e+00, float %dense_11_0_load
  %relu_3_0_addr = getelementptr [128 x float]* %relu_3_0, i64 0, i64 %tmp_61
  store float %tmp_63, float* %relu_3_0_addr, align 4
  br label %.preheader2626.0

.preheader2624.0:                                 ; preds = %.preheader2624.0.preheader, %22
  %p_37 = phi i7 [ %j7_V, %22 ], [ 0, %.preheader2624.0.preheader ]
  %exitcond38 = icmp eq i7 %p_37, -64
  %empty_79 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 64, i64 64, i64 64) nounwind
  %j7_V = add i7 %p_37, 1
  br i1 %exitcond38, label %.preheader2621.0.preheader, label %.preheader2623.preheader.0

.preheader2621.0.preheader:                       ; preds = %.preheader2624.0
  br label %.preheader2621.0

; <label>:22                                      ; preds = %.preheader2623.0
  %dense_2_0_addr_1 = getelementptr [64 x float]* %dense_2_0, i64 0, i64 %tmp_64
  store float %reducer5, float* %dense_2_0_addr_1, align 4
  br label %.preheader2624.0

.preheader2623.0:                                 ; preds = %23, %.preheader2623.preheader.0
  %p_39 = phi i8 [ %ra35_V, %23 ], [ 0, %.preheader2623.preheader.0 ]
  %reducer5 = phi float [ %reducer11, %23 ], [ 0.000000e+00, %.preheader2623.preheader.0 ]
  %exitcond40 = icmp eq i8 %p_39, -128
  %empty_80 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 128, i64 128, i64 128) nounwind
  %ra35_V = add i8 %p_39, 1
  br i1 %exitcond40, label %22, label %23

.preheader2623.preheader.0:                       ; preds = %.preheader2624.0
  %tmp_64 = zext i7 %p_37 to i64
  %tmp_64_cast = zext i7 %p_37 to i15
  br label %.preheader2623.0

; <label>:23                                      ; preds = %.preheader2623.0
  %tmp_67 = zext i8 %p_39 to i64
  %tmp_140 = call i14 @_ssdm_op_BitConcatenate.i14.i8.i6(i8 %p_39, i6 0)
  %tmp_190_cast = zext i14 %tmp_140 to i15
  %tmp_141 = add i15 %tmp_64_cast, %tmp_190_cast
  %tmp_191_cast = zext i15 %tmp_141 to i64
  %w_dense_2_addr = getelementptr [8192 x float]* %w_dense_2, i64 0, i64 %tmp_191_cast
  %relu_3_0_addr_1 = getelementptr [128 x float]* %relu_3_0, i64 0, i64 %tmp_67
  %relu_3_0_load = load float* %relu_3_0_addr_1, align 4
  %w_dense_2_load = load float* %w_dense_2_addr, align 4
  %tmp_68 = fmul float %relu_3_0_load, %w_dense_2_load
  %reducer11 = fadd float %tmp_68, %reducer5
  br label %.preheader2623.0

.preheader2621.0:                                 ; preds = %.preheader2621.0.preheader, %24
  %p_38 = phi i7 [ %j8_V, %24 ], [ 0, %.preheader2621.0.preheader ]
  %exitcond39 = icmp eq i7 %p_38, -64
  %empty_81 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 64, i64 64, i64 64) nounwind
  %j8_V = add i7 %p_38, 1
  br i1 %exitcond39, label %.preheader2619.0.preheader, label %24

.preheader2619.0.preheader:                       ; preds = %.preheader2621.0
  br label %.preheader2619.0

; <label>:24                                      ; preds = %.preheader2621.0
  %tmp_65 = zext i7 %p_38 to i64
  %dense_2_0_addr = getelementptr [64 x float]* %dense_2_0, i64 0, i64 %tmp_65
  %dense_2_0_load = load float* %dense_2_0_addr, align 4
  %b_dense_2_addr = getelementptr [64 x float]* %b_dense_2, i64 0, i64 %tmp_65
  %b_dense_2_load = load float* %b_dense_2_addr, align 4
  %tmp_66 = fadd float %dense_2_0_load, %b_dense_2_load
  %dense_21_0_addr = getelementptr [64 x float]* %dense_21_0, i64 0, i64 %tmp_65
  store float %tmp_66, float* %dense_21_0_addr, align 4
  br label %.preheader2621.0

.preheader2619.0:                                 ; preds = %.preheader2619.0.preheader, %._crit_edge2671.0
  %p_40 = phi i7 [ %args03_V, %._crit_edge2671.0 ], [ 0, %.preheader2619.0.preheader ]
  %exitcond41 = icmp eq i7 %p_40, -64
  %empty_82 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 64, i64 64, i64 64) nounwind
  %args03_V = add i7 %p_40, 1
  br i1 %exitcond41, label %.preheader2617.0.preheader, label %._crit_edge2671.0

.preheader2617.0.preheader:                       ; preds = %.preheader2619.0
  br label %.preheader2617.0

._crit_edge2671.0:                                ; preds = %.preheader2619.0
  %tmp_69 = zext i7 %p_40 to i64
  %dense_21_0_addr_1 = getelementptr [64 x float]* %dense_21_0, i64 0, i64 %tmp_69
  %dense_21_0_load = load float* %dense_21_0_addr_1, align 4
  %dense_21_0_load_to_s = bitcast float %dense_21_0_load to i32
  %tmp_113 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %dense_21_0_load_to_s, i32 23, i32 30)
  %tmp_142 = trunc i32 %dense_21_0_load_to_s to i23
  %notlhs7 = icmp ne i8 %tmp_113, -1
  %notrhs7 = icmp eq i23 %tmp_142, 0
  %tmp_115 = or i1 %notrhs7, %notlhs7
  %tmp_116 = fcmp olt float %dense_21_0_load, 0.000000e+00
  %tmp_117 = and i1 %tmp_115, %tmp_116
  %tmp_71 = select i1 %tmp_117, float 0.000000e+00, float %dense_21_0_load
  %relu_4_0_addr = getelementptr [64 x float]* %relu_4_0, i64 0, i64 %tmp_69
  store float %tmp_71, float* %relu_4_0_addr, align 4
  br label %.preheader2619.0

.preheader2617.0:                                 ; preds = %.preheader2617.0.preheader, %25
  %p_41 = phi i6 [ %j9_V, %25 ], [ 0, %.preheader2617.0.preheader ]
  %exitcond42 = icmp eq i6 %p_41, -32
  %empty_83 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 32, i64 32, i64 32) nounwind
  %j9_V = add i6 %p_41, 1
  br i1 %exitcond42, label %.preheader2614.0.preheader, label %.preheader2616.preheader.0

.preheader2614.0.preheader:                       ; preds = %.preheader2617.0
  br label %.preheader2614.0

; <label>:25                                      ; preds = %.preheader2616.0
  %dense_3_0_addr_1 = getelementptr [32 x float]* %dense_3_0, i64 0, i64 %tmp_72
  store float %reducer6, float* %dense_3_0_addr_1, align 4
  br label %.preheader2617.0

.preheader2616.0:                                 ; preds = %26, %.preheader2616.preheader.0
  %p_43 = phi i7 [ %ra36_V, %26 ], [ 0, %.preheader2616.preheader.0 ]
  %reducer6 = phi float [ %reducer12, %26 ], [ 0.000000e+00, %.preheader2616.preheader.0 ]
  %exitcond44 = icmp eq i7 %p_43, -64
  %empty_84 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 64, i64 64, i64 64) nounwind
  %ra36_V = add i7 %p_43, 1
  br i1 %exitcond44, label %25, label %26

.preheader2616.preheader.0:                       ; preds = %.preheader2617.0
  %tmp_72 = zext i6 %p_41 to i64
  %tmp_72_cast = zext i6 %p_41 to i13
  br label %.preheader2616.0

; <label>:26                                      ; preds = %.preheader2616.0
  %tmp_75 = zext i7 %p_43 to i64
  %tmp_143 = call i12 @_ssdm_op_BitConcatenate.i12.i7.i5(i7 %p_43, i5 0)
  %tmp_193_cast = zext i12 %tmp_143 to i13
  %tmp_144 = add i13 %tmp_72_cast, %tmp_193_cast
  %tmp_194_cast = zext i13 %tmp_144 to i64
  %w_dense_3_addr = getelementptr [2048 x float]* %w_dense_3, i64 0, i64 %tmp_194_cast
  %relu_4_0_addr_1 = getelementptr [64 x float]* %relu_4_0, i64 0, i64 %tmp_75
  %relu_4_0_load = load float* %relu_4_0_addr_1, align 4
  %w_dense_3_load = load float* %w_dense_3_addr, align 4
  %tmp_76 = fmul float %relu_4_0_load, %w_dense_3_load
  %reducer12 = fadd float %tmp_76, %reducer6
  br label %.preheader2616.0

.preheader2614.0:                                 ; preds = %.preheader2614.0.preheader, %27
  %p_42 = phi i6 [ %j10_V, %27 ], [ 0, %.preheader2614.0.preheader ]
  %exitcond43 = icmp eq i6 %p_42, -32
  %empty_85 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 32, i64 32, i64 32) nounwind
  %j10_V = add i6 %p_42, 1
  br i1 %exitcond43, label %.preheader2612.0.preheader, label %27

.preheader2612.0.preheader:                       ; preds = %.preheader2614.0
  br label %.preheader2612.0

; <label>:27                                      ; preds = %.preheader2614.0
  %tmp_73 = zext i6 %p_42 to i64
  %dense_3_0_addr = getelementptr [32 x float]* %dense_3_0, i64 0, i64 %tmp_73
  %dense_3_0_load = load float* %dense_3_0_addr, align 4
  %b_dense_3_addr = getelementptr [32 x float]* %b_dense_3, i64 0, i64 %tmp_73
  %b_dense_3_load = load float* %b_dense_3_addr, align 4
  %tmp_74 = fadd float %dense_3_0_load, %b_dense_3_load
  %dense_31_0_addr = getelementptr [32 x float]* %dense_31_0, i64 0, i64 %tmp_73
  store float %tmp_74, float* %dense_31_0_addr, align 4
  br label %.preheader2614.0

.preheader2612.0:                                 ; preds = %.preheader2612.0.preheader, %._crit_edge2672.0
  %p_44 = phi i6 [ %args04_V, %._crit_edge2672.0 ], [ 0, %.preheader2612.0.preheader ]
  %exitcond45 = icmp eq i6 %p_44, -32
  %empty_86 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 32, i64 32, i64 32) nounwind
  %args04_V = add i6 %p_44, 1
  br i1 %exitcond45, label %.preheader2610.0.preheader, label %._crit_edge2672.0

.preheader2610.0.preheader:                       ; preds = %.preheader2612.0
  br label %.preheader2610.0

._crit_edge2672.0:                                ; preds = %.preheader2612.0
  %tmp_77 = zext i6 %p_44 to i64
  %dense_31_0_addr_1 = getelementptr [32 x float]* %dense_31_0, i64 0, i64 %tmp_77
  %dense_31_0_load = load float* %dense_31_0_addr_1, align 4
  %dense_31_0_load_to_s = bitcast float %dense_31_0_load to i32
  %tmp_118 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %dense_31_0_load_to_s, i32 23, i32 30)
  %tmp_145 = trunc i32 %dense_31_0_load_to_s to i23
  %notlhs8 = icmp ne i8 %tmp_118, -1
  %notrhs8 = icmp eq i23 %tmp_145, 0
  %tmp_120 = or i1 %notrhs8, %notlhs8
  %tmp_121 = fcmp olt float %dense_31_0_load, 0.000000e+00
  %tmp_122 = and i1 %tmp_120, %tmp_121
  %tmp_79 = select i1 %tmp_122, float 0.000000e+00, float %dense_31_0_load
  %relu_5_0_addr = getelementptr [32 x float]* %relu_5_0, i64 0, i64 %tmp_77
  store float %tmp_79, float* %relu_5_0_addr, align 4
  br label %.preheader2612.0

.preheader2610.0:                                 ; preds = %.preheader2610.0.preheader, %28
  %p_45 = phi i4 [ %j11_V, %28 ], [ 0, %.preheader2610.0.preheader ]
  %exitcond46 = icmp eq i4 %p_45, -6
  %empty_87 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 10, i64 10, i64 10) nounwind
  %j11_V = add i4 %p_45, 1
  br i1 %exitcond46, label %.preheader2607.0.preheader, label %.preheader2609.preheader.0

.preheader2607.0.preheader:                       ; preds = %.preheader2610.0
  br label %.preheader2607.0

; <label>:28                                      ; preds = %.preheader2609.0
  %dense_4_0_addr_1 = getelementptr [10 x float]* %dense_4_0, i64 0, i64 %tmp_80
  store float %reducer7, float* %dense_4_0_addr_1, align 4
  br label %.preheader2610.0

.preheader2609.0:                                 ; preds = %29, %.preheader2609.preheader.0
  %p_47 = phi i6 [ %ra37_V, %29 ], [ 0, %.preheader2609.preheader.0 ]
  %reducer7 = phi float [ %reducer13, %29 ], [ 0.000000e+00, %.preheader2609.preheader.0 ]
  %exitcond48 = icmp eq i6 %p_47, -32
  %empty_88 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 32, i64 32, i64 32) nounwind
  %ra37_V = add i6 %p_47, 1
  br i1 %exitcond48, label %28, label %29

.preheader2609.preheader.0:                       ; preds = %.preheader2610.0
  %tmp_80 = zext i4 %p_45 to i64
  %tmp_80_cast = zext i4 %p_45 to i10
  br label %.preheader2609.0

; <label>:29                                      ; preds = %.preheader2609.0
  %tmp_83 = zext i6 %p_47 to i64
  %tmp_146 = call i9 @_ssdm_op_BitConcatenate.i9.i6.i3(i6 %p_47, i3 0)
  %p_shl6_cast = zext i9 %tmp_146 to i10
  %tmp_147 = call i7 @_ssdm_op_BitConcatenate.i7.i6.i1(i6 %p_47, i1 false)
  %p_shl7_cast = zext i7 %tmp_147 to i10
  %tmp_148 = add i10 %p_shl7_cast, %p_shl6_cast
  %tmp_149 = add i10 %tmp_148, %tmp_80_cast
  %tmp_198_cast = zext i10 %tmp_149 to i64
  %w_dense_4_addr = getelementptr [320 x float]* %w_dense_4, i64 0, i64 %tmp_198_cast
  %relu_5_0_addr_1 = getelementptr [32 x float]* %relu_5_0, i64 0, i64 %tmp_83
  %relu_5_0_load = load float* %relu_5_0_addr_1, align 4
  %w_dense_4_load = load float* %w_dense_4_addr, align 4
  %tmp_84 = fmul float %relu_5_0_load, %w_dense_4_load
  %reducer13 = fadd float %tmp_84, %reducer7
  br label %.preheader2609.0

.preheader2607.0:                                 ; preds = %.preheader2607.0.preheader, %30
  %p_46 = phi i4 [ %j12_V, %30 ], [ 0, %.preheader2607.0.preheader ]
  %exitcond47 = icmp eq i4 %p_46, -6
  %empty_89 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 10, i64 10, i64 10) nounwind
  %j12_V = add i4 %p_46, 1
  br i1 %exitcond47, label %.preheader2677.preheader, label %30

.preheader2677.preheader:                         ; preds = %.preheader2607.0
  br label %.preheader2677

; <label>:30                                      ; preds = %.preheader2607.0
  %tmp_81 = zext i4 %p_46 to i64
  %dense_4_0_addr = getelementptr [10 x float]* %dense_4_0, i64 0, i64 %tmp_81
  %dense_4_0_load = load float* %dense_4_0_addr, align 4
  %b_dense_4_addr = getelementptr [10 x float]* %b_dense_4, i64 0, i64 %tmp_81
  %b_dense_4_load = load float* %b_dense_4_addr, align 4
  %tmp_82 = fadd float %dense_4_0_load, %b_dense_4_load
  %dense_41_0_addr = getelementptr [10 x float]* %dense_41_0, i64 0, i64 %tmp_81
  store float %tmp_82, float* %dense_41_0_addr, align 4
  br label %.preheader2607.0

.preheader2677:                                   ; preds = %.preheader2677.preheader, %31
  %compute6 = phi float [ %reducer38, %31 ], [ -1.000000e+00, %.preheader2677.preheader ]
  %p_23 = phi i4 [ %ra38_V, %31 ], [ 0, %.preheader2677.preheader ]
  %exitcond22 = icmp eq i4 %p_23, -6
  %empty_90 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 10, i64 10, i64 10) nounwind
  %ra38_V = add i4 %p_23, 1
  br i1 %exitcond22, label %.preheader2673.preheader, label %31

.preheader2673.preheader:                         ; preds = %.preheader2677
  br label %.preheader2673

; <label>:31                                      ; preds = %.preheader2677
  %tmp_33 = zext i4 %p_23 to i64
  %dense_41_0_addr_1 = getelementptr [10 x float]* %dense_41_0, i64 0, i64 %tmp_33
  %dense_41_0_load_2 = load float* %dense_41_0_addr_1, align 4
  %p_a_assign_load_to_i = bitcast float %dense_41_0_load_2 to i32
  %tmp_123 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %p_a_assign_load_to_i, i32 23, i32 30)
  %tmp_150 = trunc i32 %p_a_assign_load_to_i to i23
  %compute6_to_int = bitcast float %compute6 to i32
  %tmp_125 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %compute6_to_int, i32 23, i32 30)
  %tmp_151 = trunc i32 %compute6_to_int to i23
  %notlhs9 = icmp ne i8 %tmp_123, -1
  %notrhs9 = icmp eq i23 %tmp_150, 0
  %tmp_127 = or i1 %notrhs9, %notlhs9
  %notlhs10 = icmp ne i8 %tmp_125, -1
  %notrhs10 = icmp eq i23 %tmp_151, 0
  %tmp_128 = or i1 %notrhs10, %notlhs10
  %tmp_129 = and i1 %tmp_127, %tmp_128
  %tmp_130 = fcmp olt float %dense_41_0_load_2, %compute6
  %tmp_131 = and i1 %tmp_129, %tmp_130
  %reducer38 = select i1 %tmp_131, float %compute6, float %dense_41_0_load_2
  br label %.preheader2677

.preheader2673:                                   ; preds = %.preheader2673.preheader, %32
  %p_24 = phi i4 [ %ra39_V, %32 ], [ 0, %.preheader2673.preheader ]
  %compute7 = phi float [ %reducer39, %32 ], [ 0.000000e+00, %.preheader2673.preheader ]
  %exitcond24 = icmp eq i4 %p_24, -6
  %empty_91 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 10, i64 10, i64 10) nounwind
  %ra39_V = add i4 %p_24, 1
  br i1 %exitcond24, label %.preheader2606.preheader, label %32

.preheader2606.preheader:                         ; preds = %.preheader2673
  %tmp_36 = fpext float %compute7 to double
  br label %.preheader.0

; <label>:32                                      ; preds = %.preheader2673
  %tmp_37 = zext i4 %p_24 to i64
  %dense_41_0_addr_2 = getelementptr [10 x float]* %dense_41_0, i64 0, i64 %tmp_37
  %dense_41_0_load = load float* %dense_41_0_addr_2, align 4
  %tmp_38 = fsub float %dense_41_0_load, %compute6
  %tmp_42 = fpext float %tmp_38 to double
  %tmp_46 = call double @llvm.exp.f64(double %tmp_42)
  %tmp_48 = fpext float %compute7 to double
  %tmp_55 = fadd double %tmp_46, %tmp_48
  %reducer39 = fptrunc double %tmp_55 to float
  br label %.preheader2673

.preheader.0:                                     ; preds = %33, %.preheader2606.preheader
  %p_042 = phi i4 [ %j13_V, %33 ], [ 0, %.preheader2606.preheader ]
  %exitcond25 = icmp eq i4 %p_042, -6
  %empty_92 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 10, i64 10, i64 10) nounwind
  %j13_V = add i4 %p_042, 1
  br i1 %exitcond25, label %.preheader2606.1, label %33

; <label>:33                                      ; preds = %.preheader.0
  %tmp_85 = zext i4 %p_042 to i64
  %output_addr = getelementptr [10 x float]* %output_r, i64 0, i64 %tmp_85
  %dense_41_0_addr_3 = getelementptr [10 x float]* %dense_41_0, i64 0, i64 %tmp_85
  %dense_41_0_load_1 = load float* %dense_41_0_addr_3, align 4
  %tmp_86 = fsub float %dense_41_0_load_1, %compute6
  %tmp_87 = fpext float %tmp_86 to double
  %tmp_88 = call double @llvm.exp.f64(double %tmp_87)
  %tmp_89 = fdiv double %tmp_88, %tmp_36
  %tmp_90 = fptrunc double %tmp_89 to float
  store float %tmp_90, float* %output_addr, align 4
  br label %.preheader.0

.preheader2606.1:                                 ; preds = %.preheader.0
  ret void
}

!opencl.kernels = !{!0, !7, !13, !13, !19, !25, !19, !19, !7, !25, !19, !19, !19, !28, !19, !31, !19, !19, !19, !33, !33, !35, !35, !7, !25, !19, !19, !19, !33, !33, !19, !25, !33, !33, !19, !19, !13, !13, !19, !37, !40, !41, !42, !42}
!hls.encrypted.func = !{}
!llvm.map.gv = !{!44}

!0 = metadata !{null, metadata !1, metadata !2, metadata !3, metadata !4, metadata !5, metadata !6}
!1 = metadata !{metadata !"kernel_arg_addr_space", i32 1, i32 1, i32 1, i32 1, i32 1, i32 1, i32 1, i32 1, i32 1, i32 1, i32 1, i32 1, i32 1, i32 1}
!2 = metadata !{metadata !"kernel_arg_access_qual", metadata !"none", metadata !"none", metadata !"none", metadata !"none", metadata !"none", metadata !"none", metadata !"none", metadata !"none", metadata !"none", metadata !"none", metadata !"none", metadata !"none", metadata !"none", metadata !"none"}
!3 = metadata !{metadata !"kernel_arg_type", metadata !"float [128][1][2]*", metadata !"float [2][1][8]*", metadata !"float*", metadata !"float [128][1][16]*", metadata !"float*", metadata !"float [128]*", metadata !"float*", metadata !"float [64]*", metadata !"float*", metadata !"float [32]*", metadata !"float*", metadata !"float [10]*", metadata !"float*", metadata !"float [10]*"}
!4 = metadata !{metadata !"kernel_arg_type_qual", metadata !"", metadata !"", metadata !"", metadata !"", metadata !"", metadata !"", metadata !"", metadata !"", metadata !"", metadata !"", metadata !"", metadata !"", metadata !"", metadata !""}
!5 = metadata !{metadata !"kernel_arg_name", metadata !"input_data", metadata !"w_conv1d_1", metadata !"b_conv1d_1", metadata !"w_conv1d_2", metadata !"b_conv1d_2", metadata !"w_dense_1", metadata !"b_dense_1", metadata !"w_dense_2", metadata !"b_dense_2", metadata !"w_dense_3", metadata !"b_dense_3", metadata !"w_dense_4", metadata !"b_dense_4", metadata !"output"}
!6 = metadata !{metadata !"reqd_work_group_size", i32 1, i32 1, i32 1}
!7 = metadata !{null, metadata !8, metadata !9, metadata !10, metadata !11, metadata !12, metadata !6}
!8 = metadata !{metadata !"kernel_arg_addr_space", i32 0, i32 0}
!9 = metadata !{metadata !"kernel_arg_access_qual", metadata !"none", metadata !"none"}
!10 = metadata !{metadata !"kernel_arg_type", metadata !"const ap_int_base<32, true> &", metadata !"int"}
!11 = metadata !{metadata !"kernel_arg_type_qual", metadata !"", metadata !""}
!12 = metadata !{metadata !"kernel_arg_name", metadata !"op", metadata !"i_op"}
!13 = metadata !{null, metadata !14, metadata !15, metadata !16, metadata !17, metadata !18, metadata !6}
!14 = metadata !{metadata !"kernel_arg_addr_space", i32 0}
!15 = metadata !{metadata !"kernel_arg_access_qual", metadata !"none"}
!16 = metadata !{metadata !"kernel_arg_type", metadata !"int"}
!17 = metadata !{metadata !"kernel_arg_type_qual", metadata !""}
!18 = metadata !{metadata !"kernel_arg_name", metadata !"op"}
!19 = metadata !{null, metadata !20, metadata !21, metadata !22, metadata !23, metadata !24, metadata !6}
!20 = metadata !{metadata !"kernel_arg_addr_space"}
!21 = metadata !{metadata !"kernel_arg_access_qual"}
!22 = metadata !{metadata !"kernel_arg_type"}
!23 = metadata !{metadata !"kernel_arg_type_qual"}
!24 = metadata !{metadata !"kernel_arg_name"}
!25 = metadata !{null, metadata !8, metadata !9, metadata !26, metadata !11, metadata !27, metadata !6}
!26 = metadata !{metadata !"kernel_arg_type", metadata !"const ap_int_base<32, true> &", metadata !"const ap_int_base<32, true> &"}
!27 = metadata !{metadata !"kernel_arg_name", metadata !"op", metadata !"op2"}
!28 = metadata !{null, metadata !8, metadata !9, metadata !29, metadata !11, metadata !30, metadata !6}
!29 = metadata !{metadata !"kernel_arg_type", metadata !"const float &", metadata !"const float &"}
!30 = metadata !{metadata !"kernel_arg_name", metadata !"__a", metadata !"__b"}
!31 = metadata !{null, metadata !8, metadata !9, metadata !32, metadata !11, metadata !27, metadata !6}
!32 = metadata !{metadata !"kernel_arg_type", metadata !"const ap_int_base<64, true> &", metadata !"const ap_int_base<32, true> &"}
!33 = metadata !{null, metadata !14, metadata !15, metadata !34, metadata !17, metadata !18, metadata !6}
!34 = metadata !{metadata !"kernel_arg_type", metadata !"const ap_int_base<32, true> &"}
!35 = metadata !{null, metadata !14, metadata !15, metadata !36, metadata !17, metadata !18, metadata !6}
!36 = metadata !{metadata !"kernel_arg_type", metadata !"const ap_int_base<64, true> &"}
!37 = metadata !{null, metadata !14, metadata !15, metadata !38, metadata !17, metadata !39, metadata !6}
!38 = metadata !{metadata !"kernel_arg_type", metadata !"const ap_int_base<1, false> &"}
!39 = metadata !{metadata !"kernel_arg_name", metadata !"op2"}
!40 = metadata !{null, metadata !8, metadata !9, metadata !10, metadata !11, metadata !27, metadata !6}
!41 = metadata !{null, metadata !14, metadata !15, metadata !34, metadata !17, metadata !39, metadata !6}
!42 = metadata !{null, metadata !14, metadata !15, metadata !16, metadata !17, metadata !43, metadata !6}
!43 = metadata !{metadata !"kernel_arg_name", metadata !"val"}
!44 = metadata !{metadata !45, [1 x i32]* @llvm_global_ctors_0}
!45 = metadata !{metadata !46}
!46 = metadata !{i32 0, i32 31, metadata !47}
!47 = metadata !{metadata !48}
!48 = metadata !{metadata !"llvm.global_ctors.0", metadata !49, metadata !"", i32 0, i32 31}
!49 = metadata !{metadata !50}
!50 = metadata !{i32 0, i32 0, i32 1}
!51 = metadata !{metadata !52}
!52 = metadata !{i32 0, i32 31, metadata !53}
!53 = metadata !{metadata !54}
!54 = metadata !{metadata !"input_data", metadata !55, metadata !"float", i32 0, i32 31}
!55 = metadata !{metadata !50, metadata !56, metadata !50, metadata !57}
!56 = metadata !{i32 0, i32 127, i32 1}
!57 = metadata !{i32 0, i32 1, i32 1}
!58 = metadata !{metadata !59}
!59 = metadata !{i32 0, i32 31, metadata !60}
!60 = metadata !{metadata !61}
!61 = metadata !{metadata !"w_conv1d_1", metadata !62, metadata !"float", i32 0, i32 31}
!62 = metadata !{metadata !56, metadata !57, metadata !50, metadata !63}
!63 = metadata !{i32 0, i32 7, i32 1}
!64 = metadata !{metadata !65}
!65 = metadata !{i32 0, i32 31, metadata !66}
!66 = metadata !{metadata !67}
!67 = metadata !{metadata !"b_conv1d_1", metadata !68, metadata !"float", i32 0, i32 31}
!68 = metadata !{metadata !56}
!69 = metadata !{metadata !70}
!70 = metadata !{i32 0, i32 31, metadata !71}
!71 = metadata !{metadata !72}
!72 = metadata !{metadata !"w_conv1d_2", metadata !73, metadata !"float", i32 0, i32 31}
!73 = metadata !{metadata !74, metadata !56, metadata !50, metadata !75}
!74 = metadata !{i32 0, i32 63, i32 1}
!75 = metadata !{i32 0, i32 15, i32 1}
!76 = metadata !{metadata !77}
!77 = metadata !{i32 0, i32 31, metadata !78}
!78 = metadata !{metadata !79}
!79 = metadata !{metadata !"b_conv1d_2", metadata !80, metadata !"float", i32 0, i32 31}
!80 = metadata !{metadata !74}
!81 = metadata !{metadata !82}
!82 = metadata !{i32 0, i32 31, metadata !83}
!83 = metadata !{metadata !84}
!84 = metadata !{metadata !"w_dense_1", metadata !85, metadata !"float", i32 0, i32 31}
!85 = metadata !{metadata !86, metadata !56}
!86 = metadata !{i32 0, i32 1407, i32 1}
!87 = metadata !{metadata !88}
!88 = metadata !{i32 0, i32 31, metadata !89}
!89 = metadata !{metadata !90}
!90 = metadata !{metadata !"b_dense_1", metadata !68, metadata !"float", i32 0, i32 31}
!91 = metadata !{metadata !92}
!92 = metadata !{i32 0, i32 31, metadata !93}
!93 = metadata !{metadata !94}
!94 = metadata !{metadata !"w_dense_2", metadata !95, metadata !"float", i32 0, i32 31}
!95 = metadata !{metadata !56, metadata !74}
!96 = metadata !{metadata !97}
!97 = metadata !{i32 0, i32 31, metadata !98}
!98 = metadata !{metadata !99}
!99 = metadata !{metadata !"b_dense_2", metadata !80, metadata !"float", i32 0, i32 31}
!100 = metadata !{metadata !101}
!101 = metadata !{i32 0, i32 31, metadata !102}
!102 = metadata !{metadata !103}
!103 = metadata !{metadata !"w_dense_3", metadata !104, metadata !"float", i32 0, i32 31}
!104 = metadata !{metadata !74, metadata !105}
!105 = metadata !{i32 0, i32 31, i32 1}
!106 = metadata !{metadata !107}
!107 = metadata !{i32 0, i32 31, metadata !108}
!108 = metadata !{metadata !109}
!109 = metadata !{metadata !"b_dense_3", metadata !110, metadata !"float", i32 0, i32 31}
!110 = metadata !{metadata !105}
!111 = metadata !{metadata !112}
!112 = metadata !{i32 0, i32 31, metadata !113}
!113 = metadata !{metadata !114}
!114 = metadata !{metadata !"w_dense_4", metadata !115, metadata !"float", i32 0, i32 31}
!115 = metadata !{metadata !105, metadata !116}
!116 = metadata !{i32 0, i32 9, i32 1}
!117 = metadata !{metadata !118}
!118 = metadata !{i32 0, i32 31, metadata !119}
!119 = metadata !{metadata !120}
!120 = metadata !{metadata !"b_dense_4", metadata !121, metadata !"float", i32 0, i32 31}
!121 = metadata !{metadata !116}
!122 = metadata !{metadata !123}
!123 = metadata !{i32 0, i32 31, metadata !124}
!124 = metadata !{metadata !125}
!125 = metadata !{metadata !"output", metadata !126, metadata !"float", i32 0, i32 31}
!126 = metadata !{metadata !50, metadata !116}
