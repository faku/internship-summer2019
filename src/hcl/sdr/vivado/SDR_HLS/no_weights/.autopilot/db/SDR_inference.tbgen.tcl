set C_TypeInfoList {{ 
"SDR_inference" : [[], { "return": [[], "void"]} , [{"ExternC" : 0}], [ {"input_data": [[], {"array": [ {"array": [ {"scalar": "float"}, [128,1,2]]}, [1]]}] }, {"w_conv1d_1": [[], {"array": [ {"array": [ {"scalar": "float"}, [2,1,8]]}, [128]]}] }, {"b_conv1d_1": [[], {"array": [ {"scalar": "float"}, [128]]}] }, {"w_conv1d_2": [[], {"array": [ {"array": [ {"scalar": "float"}, [128,1,16]]}, [64]]}] }, {"b_conv1d_2": [[], {"array": [ {"scalar": "float"}, [64]]}] }, {"w_dense_1": [[], {"array": [ {"array": [ {"scalar": "float"}, [128]]}, [1408]]}] }, {"b_dense_1": [[], {"array": [ {"scalar": "float"}, [128]]}] }, {"w_dense_2": [[], {"array": [ {"array": [ {"scalar": "float"}, [64]]}, [128]]}] }, {"b_dense_2": [[], {"array": [ {"scalar": "float"}, [64]]}] }, {"w_dense_3": [[], {"array": [ {"array": [ {"scalar": "float"}, [32]]}, [64]]}] }, {"b_dense_3": [[], {"array": [ {"scalar": "float"}, [32]]}] }, {"w_dense_4": [[], {"array": [ {"array": [ {"scalar": "float"}, [10]]}, [32]]}] }, {"b_dense_4": [[], {"array": [ {"scalar": "float"}, [10]]}] }, {"output": [[], {"array": [ {"array": [ {"scalar": "float"}, [10]]}, [1]]}] }],[],""]
}}
set moduleName SDR_inference
set isCombinational 0
set isDatapathOnly 0
set isPipelined 0
set pipeline_type none
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set C_modelName {SDR_inference}
set C_modelType { void 0 }
set C_modelArgList {
	{ input_data float 32 regular {axi_slave 0}  }
	{ w_conv1d_1 float 32 regular {array 2048 { 1 } 1 1 }  }
	{ b_conv1d_1 float 32 regular {array 128 { 1 } 1 1 }  }
	{ w_conv1d_2 float 32 regular {array 131072 { 1 } 1 1 }  }
	{ b_conv1d_2 float 32 regular {array 64 { 1 } 1 1 }  }
	{ w_dense_1 float 32 regular {array 180224 { 1 } 1 1 }  }
	{ b_dense_1 float 32 regular {array 128 { 1 } 1 1 }  }
	{ w_dense_2 float 32 regular {array 8192 { 1 } 1 1 }  }
	{ b_dense_2 float 32 regular {array 64 { 1 } 1 1 }  }
	{ w_dense_3 float 32 regular {array 2048 { 1 } 1 1 }  }
	{ b_dense_3 float 32 regular {array 32 { 1 } 1 1 }  }
	{ w_dense_4 float 32 regular {array 320 { 1 } 1 1 }  }
	{ b_dense_4 float 32 regular {array 10 { 1 } 1 1 }  }
	{ output_r float 32 regular {axi_slave 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "input_data", "interface" : "axi_slave", "bundle":"AXILiteS","type":"ap_memory","bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "input_data","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 1},{"low" : 0,"up" : 127,"step" : 1},{"low" : 0,"up" : 0,"step" : 1},{"low" : 0,"up" : 1,"step" : 1}]}]}], "offset" : {"in":1024}, "offset_end" : {"in":2047}} , 
 	{ "Name" : "w_conv1d_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "w_conv1d_1","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 127,"step" : 1},{"low" : 0,"up" : 1,"step" : 1},{"low" : 0,"up" : 0,"step" : 1},{"low" : 0,"up" : 7,"step" : 1}]}]}]} , 
 	{ "Name" : "b_conv1d_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "b_conv1d_1","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 127,"step" : 1}]}]}]} , 
 	{ "Name" : "w_conv1d_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "w_conv1d_2","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 63,"step" : 1},{"low" : 0,"up" : 127,"step" : 1},{"low" : 0,"up" : 0,"step" : 1},{"low" : 0,"up" : 15,"step" : 1}]}]}]} , 
 	{ "Name" : "b_conv1d_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "b_conv1d_2","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 63,"step" : 1}]}]}]} , 
 	{ "Name" : "w_dense_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "w_dense_1","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 1407,"step" : 1},{"low" : 0,"up" : 127,"step" : 1}]}]}]} , 
 	{ "Name" : "b_dense_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "b_dense_1","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 127,"step" : 1}]}]}]} , 
 	{ "Name" : "w_dense_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "w_dense_2","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 127,"step" : 1},{"low" : 0,"up" : 63,"step" : 1}]}]}]} , 
 	{ "Name" : "b_dense_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "b_dense_2","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 63,"step" : 1}]}]}]} , 
 	{ "Name" : "w_dense_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "w_dense_3","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 63,"step" : 1},{"low" : 0,"up" : 31,"step" : 1}]}]}]} , 
 	{ "Name" : "b_dense_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "b_dense_3","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 31,"step" : 1}]}]}]} , 
 	{ "Name" : "w_dense_4", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "w_dense_4","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 31,"step" : 1},{"low" : 0,"up" : 9,"step" : 1}]}]}]} , 
 	{ "Name" : "b_dense_4", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "b_dense_4","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 9,"step" : 1}]}]}]} , 
 	{ "Name" : "output_r", "interface" : "axi_slave", "bundle":"AXILiteS","type":"ap_memory","bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "output","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 1},{"low" : 0,"up" : 9,"step" : 1}]}]}], "offset" : {"out":2048}, "offset_end" : {"out":2111}} ]}
# RTL Port declarations: 
set portNum 59
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst_n sc_in sc_logic 1 reset -1 active_low_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ w_conv1d_1_address0 sc_out sc_lv 11 signal 1 } 
	{ w_conv1d_1_ce0 sc_out sc_logic 1 signal 1 } 
	{ w_conv1d_1_q0 sc_in sc_lv 32 signal 1 } 
	{ b_conv1d_1_address0 sc_out sc_lv 7 signal 2 } 
	{ b_conv1d_1_ce0 sc_out sc_logic 1 signal 2 } 
	{ b_conv1d_1_q0 sc_in sc_lv 32 signal 2 } 
	{ w_conv1d_2_address0 sc_out sc_lv 17 signal 3 } 
	{ w_conv1d_2_ce0 sc_out sc_logic 1 signal 3 } 
	{ w_conv1d_2_q0 sc_in sc_lv 32 signal 3 } 
	{ b_conv1d_2_address0 sc_out sc_lv 6 signal 4 } 
	{ b_conv1d_2_ce0 sc_out sc_logic 1 signal 4 } 
	{ b_conv1d_2_q0 sc_in sc_lv 32 signal 4 } 
	{ w_dense_1_address0 sc_out sc_lv 18 signal 5 } 
	{ w_dense_1_ce0 sc_out sc_logic 1 signal 5 } 
	{ w_dense_1_q0 sc_in sc_lv 32 signal 5 } 
	{ b_dense_1_address0 sc_out sc_lv 7 signal 6 } 
	{ b_dense_1_ce0 sc_out sc_logic 1 signal 6 } 
	{ b_dense_1_q0 sc_in sc_lv 32 signal 6 } 
	{ w_dense_2_address0 sc_out sc_lv 13 signal 7 } 
	{ w_dense_2_ce0 sc_out sc_logic 1 signal 7 } 
	{ w_dense_2_q0 sc_in sc_lv 32 signal 7 } 
	{ b_dense_2_address0 sc_out sc_lv 6 signal 8 } 
	{ b_dense_2_ce0 sc_out sc_logic 1 signal 8 } 
	{ b_dense_2_q0 sc_in sc_lv 32 signal 8 } 
	{ w_dense_3_address0 sc_out sc_lv 11 signal 9 } 
	{ w_dense_3_ce0 sc_out sc_logic 1 signal 9 } 
	{ w_dense_3_q0 sc_in sc_lv 32 signal 9 } 
	{ b_dense_3_address0 sc_out sc_lv 5 signal 10 } 
	{ b_dense_3_ce0 sc_out sc_logic 1 signal 10 } 
	{ b_dense_3_q0 sc_in sc_lv 32 signal 10 } 
	{ w_dense_4_address0 sc_out sc_lv 9 signal 11 } 
	{ w_dense_4_ce0 sc_out sc_logic 1 signal 11 } 
	{ w_dense_4_q0 sc_in sc_lv 32 signal 11 } 
	{ b_dense_4_address0 sc_out sc_lv 4 signal 12 } 
	{ b_dense_4_ce0 sc_out sc_logic 1 signal 12 } 
	{ b_dense_4_q0 sc_in sc_lv 32 signal 12 } 
	{ s_axi_AXILiteS_AWVALID sc_in sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_AWREADY sc_out sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_AWADDR sc_in sc_lv 12 signal -1 } 
	{ s_axi_AXILiteS_WVALID sc_in sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_WREADY sc_out sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_WDATA sc_in sc_lv 32 signal -1 } 
	{ s_axi_AXILiteS_WSTRB sc_in sc_lv 4 signal -1 } 
	{ s_axi_AXILiteS_ARVALID sc_in sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_ARREADY sc_out sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_ARADDR sc_in sc_lv 12 signal -1 } 
	{ s_axi_AXILiteS_RVALID sc_out sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_RREADY sc_in sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_RDATA sc_out sc_lv 32 signal -1 } 
	{ s_axi_AXILiteS_RRESP sc_out sc_lv 2 signal -1 } 
	{ s_axi_AXILiteS_BVALID sc_out sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_BREADY sc_in sc_logic 1 signal -1 } 
	{ s_axi_AXILiteS_BRESP sc_out sc_lv 2 signal -1 } 
}
set NewPortList {[ 
	{ "name": "s_axi_AXILiteS_AWADDR", "direction": "in", "datatype": "sc_lv", "bitwidth":12, "type": "signal", "bundle":{"name": "AXILiteS", "role": "AWADDR" },"address":[{"name":"input_data","role":"data","value":"1024"}] },
	{ "name": "s_axi_AXILiteS_AWVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "AWVALID" } },
	{ "name": "s_axi_AXILiteS_AWREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "AWREADY" } },
	{ "name": "s_axi_AXILiteS_WVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "WVALID" } },
	{ "name": "s_axi_AXILiteS_WREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "WREADY" } },
	{ "name": "s_axi_AXILiteS_WDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "AXILiteS", "role": "WDATA" } },
	{ "name": "s_axi_AXILiteS_WSTRB", "direction": "in", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "AXILiteS", "role": "WSTRB" } },
	{ "name": "s_axi_AXILiteS_ARADDR", "direction": "in", "datatype": "sc_lv", "bitwidth":12, "type": "signal", "bundle":{"name": "AXILiteS", "role": "ARADDR" },"address":[{"name":"output_r","role":"data","value":"2048"}] },
	{ "name": "s_axi_AXILiteS_ARVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "ARVALID" } },
	{ "name": "s_axi_AXILiteS_ARREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "ARREADY" } },
	{ "name": "s_axi_AXILiteS_RVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "RVALID" } },
	{ "name": "s_axi_AXILiteS_RREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "RREADY" } },
	{ "name": "s_axi_AXILiteS_RDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "AXILiteS", "role": "RDATA" } },
	{ "name": "s_axi_AXILiteS_RRESP", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "AXILiteS", "role": "RRESP" } },
	{ "name": "s_axi_AXILiteS_BVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "BVALID" } },
	{ "name": "s_axi_AXILiteS_BREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "AXILiteS", "role": "BREADY" } },
	{ "name": "s_axi_AXILiteS_BRESP", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "AXILiteS", "role": "BRESP" } }, 
 	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst_n", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "w_conv1d_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":11, "type": "signal", "bundle":{"name": "w_conv1d_1", "role": "address0" }} , 
 	{ "name": "w_conv1d_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "w_conv1d_1", "role": "ce0" }} , 
 	{ "name": "w_conv1d_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "w_conv1d_1", "role": "q0" }} , 
 	{ "name": "b_conv1d_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "b_conv1d_1", "role": "address0" }} , 
 	{ "name": "b_conv1d_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "b_conv1d_1", "role": "ce0" }} , 
 	{ "name": "b_conv1d_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "b_conv1d_1", "role": "q0" }} , 
 	{ "name": "w_conv1d_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":17, "type": "signal", "bundle":{"name": "w_conv1d_2", "role": "address0" }} , 
 	{ "name": "w_conv1d_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "w_conv1d_2", "role": "ce0" }} , 
 	{ "name": "w_conv1d_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "w_conv1d_2", "role": "q0" }} , 
 	{ "name": "b_conv1d_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "b_conv1d_2", "role": "address0" }} , 
 	{ "name": "b_conv1d_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "b_conv1d_2", "role": "ce0" }} , 
 	{ "name": "b_conv1d_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "b_conv1d_2", "role": "q0" }} , 
 	{ "name": "w_dense_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":18, "type": "signal", "bundle":{"name": "w_dense_1", "role": "address0" }} , 
 	{ "name": "w_dense_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "w_dense_1", "role": "ce0" }} , 
 	{ "name": "w_dense_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "w_dense_1", "role": "q0" }} , 
 	{ "name": "b_dense_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "b_dense_1", "role": "address0" }} , 
 	{ "name": "b_dense_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "b_dense_1", "role": "ce0" }} , 
 	{ "name": "b_dense_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "b_dense_1", "role": "q0" }} , 
 	{ "name": "w_dense_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":13, "type": "signal", "bundle":{"name": "w_dense_2", "role": "address0" }} , 
 	{ "name": "w_dense_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "w_dense_2", "role": "ce0" }} , 
 	{ "name": "w_dense_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "w_dense_2", "role": "q0" }} , 
 	{ "name": "b_dense_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "b_dense_2", "role": "address0" }} , 
 	{ "name": "b_dense_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "b_dense_2", "role": "ce0" }} , 
 	{ "name": "b_dense_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "b_dense_2", "role": "q0" }} , 
 	{ "name": "w_dense_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":11, "type": "signal", "bundle":{"name": "w_dense_3", "role": "address0" }} , 
 	{ "name": "w_dense_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "w_dense_3", "role": "ce0" }} , 
 	{ "name": "w_dense_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "w_dense_3", "role": "q0" }} , 
 	{ "name": "b_dense_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "b_dense_3", "role": "address0" }} , 
 	{ "name": "b_dense_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "b_dense_3", "role": "ce0" }} , 
 	{ "name": "b_dense_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "b_dense_3", "role": "q0" }} , 
 	{ "name": "w_dense_4_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":9, "type": "signal", "bundle":{"name": "w_dense_4", "role": "address0" }} , 
 	{ "name": "w_dense_4_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "w_dense_4", "role": "ce0" }} , 
 	{ "name": "w_dense_4_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "w_dense_4", "role": "q0" }} , 
 	{ "name": "b_dense_4_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "b_dense_4", "role": "address0" }} , 
 	{ "name": "b_dense_4_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "b_dense_4", "role": "ce0" }} , 
 	{ "name": "b_dense_4_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "b_dense_4", "role": "q0" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33"],
		"CDFG" : "SDR_inference",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "0", "ap_idle" : "1",
		"Pipeline" : "None", "AlignedPipeline" : "0", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"VariableLatency" : "1",
		"Port" : [
			{"Name" : "input_data", "Type" : "Memory", "Direction" : "I"},
			{"Name" : "w_conv1d_1", "Type" : "Memory", "Direction" : "I"},
			{"Name" : "b_conv1d_1", "Type" : "Memory", "Direction" : "I"},
			{"Name" : "w_conv1d_2", "Type" : "Memory", "Direction" : "I"},
			{"Name" : "b_conv1d_2", "Type" : "Memory", "Direction" : "I"},
			{"Name" : "w_dense_1", "Type" : "Memory", "Direction" : "I"},
			{"Name" : "b_dense_1", "Type" : "Memory", "Direction" : "I"},
			{"Name" : "w_dense_2", "Type" : "Memory", "Direction" : "I"},
			{"Name" : "b_dense_2", "Type" : "Memory", "Direction" : "I"},
			{"Name" : "w_dense_3", "Type" : "Memory", "Direction" : "I"},
			{"Name" : "b_dense_3", "Type" : "Memory", "Direction" : "I"},
			{"Name" : "w_dense_4", "Type" : "Memory", "Direction" : "I"},
			{"Name" : "b_dense_4", "Type" : "Memory", "Direction" : "I"},
			{"Name" : "output_r", "Type" : "Memory", "Direction" : "O"}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.SDR_inference_AXILiteS_s_axi_U", "Parent" : "0"},
	{"ID" : "2", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.transpose_1_0_0_U", "Parent" : "0"},
	{"ID" : "3", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.pad_temp_0_0_U", "Parent" : "0"},
	{"ID" : "4", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.conv1d_1_0_0_U", "Parent" : "0"},
	{"ID" : "5", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.conv1d_11_0_0_U", "Parent" : "0"},
	{"ID" : "6", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.relu_1_0_0_U", "Parent" : "0"},
	{"ID" : "7", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.maxpool1d_1_0_0_U", "Parent" : "0"},
	{"ID" : "8", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.pad_temp1_0_0_U", "Parent" : "0"},
	{"ID" : "9", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.conv1d_2_0_0_U", "Parent" : "0"},
	{"ID" : "10", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.conv1d_21_0_0_U", "Parent" : "0"},
	{"ID" : "11", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.relu_2_0_0_U", "Parent" : "0"},
	{"ID" : "12", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.maxpool1d_2_0_0_U", "Parent" : "0"},
	{"ID" : "13", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.transpose_2_0_0_U", "Parent" : "0"},
	{"ID" : "14", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.flatten_1_0_U", "Parent" : "0"},
	{"ID" : "15", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.dense_1_0_U", "Parent" : "0"},
	{"ID" : "16", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.dense_11_0_U", "Parent" : "0"},
	{"ID" : "17", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.relu_3_0_U", "Parent" : "0"},
	{"ID" : "18", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.dense_2_0_U", "Parent" : "0"},
	{"ID" : "19", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.dense_21_0_U", "Parent" : "0"},
	{"ID" : "20", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.relu_4_0_U", "Parent" : "0"},
	{"ID" : "21", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.dense_3_0_U", "Parent" : "0"},
	{"ID" : "22", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.dense_31_0_U", "Parent" : "0"},
	{"ID" : "23", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.relu_5_0_U", "Parent" : "0"},
	{"ID" : "24", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.dense_4_0_U", "Parent" : "0"},
	{"ID" : "25", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.dense_41_0_U", "Parent" : "0"},
	{"ID" : "26", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.SDR_inference_fadzec_U1", "Parent" : "0"},
	{"ID" : "27", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.SDR_inference_fmuAem_U2", "Parent" : "0"},
	{"ID" : "28", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.SDR_inference_fptBew_U3", "Parent" : "0"},
	{"ID" : "29", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.SDR_inference_fpeCeG_U4", "Parent" : "0"},
	{"ID" : "30", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.SDR_inference_fcmDeQ_U5", "Parent" : "0"},
	{"ID" : "31", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.SDR_inference_dadEe0_U6", "Parent" : "0"},
	{"ID" : "32", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.SDR_inference_ddiFfa_U7", "Parent" : "0"},
	{"ID" : "33", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.SDR_inference_dexGfk_U8", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	SDR_inference {
		input_data {Type I LastRead 2 FirstWrite -1}
		w_conv1d_1 {Type I LastRead 6 FirstWrite -1}
		b_conv1d_1 {Type I LastRead 4 FirstWrite -1}
		w_conv1d_2 {Type I LastRead 11 FirstWrite -1}
		b_conv1d_2 {Type I LastRead 9 FirstWrite -1}
		w_dense_1 {Type I LastRead 15 FirstWrite -1}
		b_dense_1 {Type I LastRead 15 FirstWrite -1}
		w_dense_2 {Type I LastRead 18 FirstWrite -1}
		b_dense_2 {Type I LastRead 18 FirstWrite -1}
		w_dense_3 {Type I LastRead 21 FirstWrite -1}
		b_dense_3 {Type I LastRead 21 FirstWrite -1}
		w_dense_4 {Type I LastRead 24 FirstWrite -1}
		b_dense_4 {Type I LastRead 24 FirstWrite -1}
		output_r {Type O LastRead -1 FirstWrite 85}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "70880681", "Max" : "70880681"}
	, {"Name" : "Interval", "Min" : "70880682", "Max" : "70880682"}
]}

set PipelineEnableSignalInfo {[
]}

set Spec2ImplPortList { 
	w_conv1d_1 { ap_memory {  { w_conv1d_1_address0 mem_address 1 11 }  { w_conv1d_1_ce0 mem_ce 1 1 }  { w_conv1d_1_q0 mem_dout 0 32 } } }
	b_conv1d_1 { ap_memory {  { b_conv1d_1_address0 mem_address 1 7 }  { b_conv1d_1_ce0 mem_ce 1 1 }  { b_conv1d_1_q0 mem_dout 0 32 } } }
	w_conv1d_2 { ap_memory {  { w_conv1d_2_address0 mem_address 1 17 }  { w_conv1d_2_ce0 mem_ce 1 1 }  { w_conv1d_2_q0 mem_dout 0 32 } } }
	b_conv1d_2 { ap_memory {  { b_conv1d_2_address0 mem_address 1 6 }  { b_conv1d_2_ce0 mem_ce 1 1 }  { b_conv1d_2_q0 mem_dout 0 32 } } }
	w_dense_1 { ap_memory {  { w_dense_1_address0 mem_address 1 18 }  { w_dense_1_ce0 mem_ce 1 1 }  { w_dense_1_q0 mem_dout 0 32 } } }
	b_dense_1 { ap_memory {  { b_dense_1_address0 mem_address 1 7 }  { b_dense_1_ce0 mem_ce 1 1 }  { b_dense_1_q0 mem_dout 0 32 } } }
	w_dense_2 { ap_memory {  { w_dense_2_address0 mem_address 1 13 }  { w_dense_2_ce0 mem_ce 1 1 }  { w_dense_2_q0 mem_dout 0 32 } } }
	b_dense_2 { ap_memory {  { b_dense_2_address0 mem_address 1 6 }  { b_dense_2_ce0 mem_ce 1 1 }  { b_dense_2_q0 mem_dout 0 32 } } }
	w_dense_3 { ap_memory {  { w_dense_3_address0 mem_address 1 11 }  { w_dense_3_ce0 mem_ce 1 1 }  { w_dense_3_q0 mem_dout 0 32 } } }
	b_dense_3 { ap_memory {  { b_dense_3_address0 mem_address 1 5 }  { b_dense_3_ce0 mem_ce 1 1 }  { b_dense_3_q0 mem_dout 0 32 } } }
	w_dense_4 { ap_memory {  { w_dense_4_address0 mem_address 1 9 }  { w_dense_4_ce0 mem_ce 1 1 }  { w_dense_4_q0 mem_dout 0 32 } } }
	b_dense_4 { ap_memory {  { b_dense_4_address0 mem_address 1 4 }  { b_dense_4_ce0 mem_ce 1 1 }  { b_dense_4_q0 mem_dout 0 32 } } }
}

set busDeadlockParameterList { 
}

# RTL port scheduling information:
set fifoSchedulingInfoList { 
}

# RTL bus port read request latency information:
set busReadReqLatencyList { 
}

# RTL bus port write response latency information:
set busWriteResLatencyList { 
}

# RTL array port load latency information:
set memoryLoadLatencyList { 
}
