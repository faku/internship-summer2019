############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2017 Xilinx, Inc. All Rights Reserved.
############################################################
set_directive_interface -mode s_axilite -register "SDR_inference" input_data
set_directive_interface -mode s_axilite -register "SDR_inference" output
set_directive_resource -core ROM_1P_BRAM "SDR_inference" w_conv1d_1
set_directive_resource -core ROM_1P_BRAM "SDR_inference" b_conv1d_1
set_directive_resource -core ROM_1P_BRAM "SDR_inference" w_conv1d_2
set_directive_resource -core ROM_1P_BRAM "SDR_inference" b_conv1d_2
set_directive_resource -core ROM_1P_BRAM "SDR_inference" w_dense_1
set_directive_resource -core ROM_1P_BRAM "SDR_inference" b_dense_1
set_directive_resource -core ROM_1P_BRAM "SDR_inference" w_dense_2
set_directive_resource -core ROM_1P_BRAM "SDR_inference" b_dense_2
set_directive_resource -core ROM_1P_BRAM "SDR_inference" w_dense_3
set_directive_resource -core ROM_1P_BRAM "SDR_inference" b_dense_3
set_directive_resource -core ROM_1P_BRAM "SDR_inference" w_dense_4
set_directive_resource -core ROM_1P_BRAM "SDR_inference" b_dense_4
