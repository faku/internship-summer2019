############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2017 Xilinx, Inc. All Rights Reserved.
############################################################
set_directive_interface -mode s_axilite -register "SDR_inference" input_data
set_directive_interface -mode s_axilite -register "SDR_inference" output
set_directive_stream -dim 1 "SDR_inference" w_conv1d_2
set_directive_stream -dim 1 "SDR_inference" w_dense_1
set_directive_interface -mode m_axi -depth 131072 "SDR_inference" w_conv1d_2
set_directive_interface -mode m_axi -depth 180224 "SDR_inference" w_dense_1
