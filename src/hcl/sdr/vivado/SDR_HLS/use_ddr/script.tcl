############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2017 Xilinx, Inc. All Rights Reserved.
############################################################
open_project SDR_HLS
set_top SDR_inference
add_files SDR_HLS/src/b_conv1d_1.h
add_files SDR_HLS/src/b_conv1d_2.h
add_files SDR_HLS/src/b_dense_1.h
add_files SDR_HLS/src/b_dense_2.h
add_files SDR_HLS/src/b_dense_3.h
add_files SDR_HLS/src/b_dense_4.h
add_files SDR_HLS/src/sdr_inference.cpp
add_files SDR_HLS/src/w_conv1d_1.h
add_files SDR_HLS/src/w_conv1d_2.h
add_files SDR_HLS/src/w_dense_1.h
add_files SDR_HLS/src/w_dense_2.h
add_files SDR_HLS/src/w_dense_3.h
add_files SDR_HLS/src/w_dense_4.h
add_files -tb SDR_HLS/include_weights/main.cpp
open_solution "use_ddr"
set_part {xc7z020clg484-1}
create_clock -period 10 -name default
source "./SDR_HLS/use_ddr/directives.tcl"
csim_design -compiler gcc
csynth_design
cosim_design
export_design -rtl verilog -format ip_catalog
